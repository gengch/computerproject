﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ProPanelCon : MonoBehaviour
{
    private string[] J_ImageUrl;//获取到的图片的url
    private string[] J_ProName;//四个按钮显示的内容
    private string[] J_ProId;//项目对应的ID
    private GameObject J_LoadPanel;
    private Slider J_LoadSlider;
    private Text J_LoadText;
    private float J_LoadTime;

    //private Text J_ProBtn_1;
    //private Text J_ProBtn_2;
    //private Text J_ProBtn_3;
    //private Text J_ProBtn_4;
    private Text J_Text;

    private Button X_SetUp;
    private Button X_SignOut;
    private Button X_Refresh;

    private GameObject Project_Res;
    private Transform GridPanel;

    void Start()
    {
        Debug.LogError("Init Prefabs");
        Project_Res = Resources.Load("ProjectGrid") as GameObject;
        J_LoadPanel = transform.parent.Find("LoadPanel").gameObject;
        J_Text = J_LoadPanel.transform.Find("Text").GetComponent<Text>();
        J_LoadSlider = J_LoadPanel.transform.Find("LoadSlider").GetComponent<Slider>();
        J_LoadText = J_LoadPanel.transform.Find("LoadText").GetComponent<Text>();
        GridPanel = transform.Find("X_ProjectPanel/Viewport/Content");

        //J_ProBtn_1 = transform.Find("ProBtn_1/Text").GetComponent<Text>();
        //J_ProBtn_2 = transform.Find("ProBtn_2/Text").GetComponent<Text>();
        //J_ProBtn_3 = transform.Find("ProBtn_3/Text").GetComponent<Text>();
        //J_ProBtn_4 = transform.Find("ProBtn_4/Text").GetComponent<Text>();
        X_SetUp = transform.Find("SetUp").GetComponent<Button>();
        X_SignOut = transform.Find("SignOut").GetComponent<Button>();
        X_SignOut.onClick.AddListener(SignOut_OnClick);
        
        X_Refresh = transform.Find("Refresh").GetComponent<Button>();
        X_Refresh.onClick.AddListener(Refresh_OnClick);

        Button[] J_Buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_Btn in J_Buttons)
        {
            
            Button J_tempbtn = J_Btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
        JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/projects/projectlist?RolesID=" + LoginCon.X_RolesID + "&UserID=" + LoginCon.X_UserID, "fileid", "ProjectName", "ID", "", "", "");
        Invoke("GetAreaImage", 1.5f);

        //J_ProBtn_1.transform.parent.gameObject.SetActive(false);
        //J_ProBtn_2.transform.parent.gameObject.SetActive(false);
        //J_ProBtn_3.transform.parent.gameObject.SetActive(false);
        //J_ProBtn_4.transform.parent.gameObject.SetActive(false);
    }

    void Update()
    {
        J_LoadTime += Time.deltaTime;
        if (J_LoadTime <= 0.5f)
        {
            J_LoadSlider.value = 2 * J_LoadTime;
            string J_LoadNum = (200 * J_LoadTime).ToString();
            if (J_LoadNum.Length > 4)
            {
                J_LoadText.text = J_LoadNum.Substring(0, 4) + "%";
            }
            else
            {
                J_LoadText.text = J_LoadNum + "%";
            }
        }
        else
        {
            J_LoadPanel.SetActive(false);
        }
    }
    //int inde;
    //int J_1;
    //int J_2;
    //int J_3;
    //int J_4;


    void OnclickProject(string index_1,string index_2,string index_3)
    {
        SceneManager.LoadScene("AreaScene");
        AreaTipCon.instance.J_prostr = index_1;
        AreaTipCon.instance.J_ImageUrl = index_2;
        AreaTipCon.instance.J_proNum = index_3;
    }
    void GetAreaImage()
    {
        Debug.LogError("GetAreaImage");
        J_ImageUrl = JsonContent.J_JsonContent.ReturnJsonData_1();//图片url
        J_ProName = JsonContent.J_JsonContent.ReturnJsonData_2();//按钮显示内容
        Debug.LogError(J_ProName);
        J_ProId = JsonContent.J_JsonContent.ReturnJsonData_3();//项目id
        //inde = 0;

        if(J_ImageUrl.Length >0)
        {
            Button[] btn = new Button[J_ProName.Length];

            float height = 0;
            for (int i = 0; i < J_ProName.Length; i++)
            {
                int j = i;//绑定方法时 不能直接传i，i会一直是最后的数值
                GameObject pro_obj = Instantiate(Project_Res);
                pro_obj.name = "Project_" + i;
                btn[i] = pro_obj.GetComponent<Button>();
                btn[i].onClick.AddListener(delegate ()
                {
                    Debug.Log("项目序号：  " + j + "   项目名称：   " + J_ProName[j] + "    项目url：" + J_ImageUrl[j] + "   项目ID：" + J_ProId[j]);
                    OnclickProject(J_ProName[j], J_ImageUrl[j], J_ProId[j]);
                });
                Debug.Log(GridPanel.name);
                pro_obj.transform.SetParent(GridPanel);
                pro_obj.transform.localScale = new Vector3(2, 2, 2);
                Button Project_UI = pro_obj.GetComponent<Button>();
                Project_UI.transform.Find("Text").GetComponent<Text>().text = J_ProName[i];

                if (i > 5)
                {
                    height += 300;
                    GridPanel.GetComponent<RectTransform>().offsetMax = new Vector2(height, 0);

                }
            }
        }
        else
        {
            Button[] btn = new Button[1];
            for (int i = 0; i < 1; i++)
            {
                int j = i;//绑定方法时 不能直接传i，i会一直是最后的数值
                GameObject pro_obj = Instantiate(Project_Res);
                pro_obj.name = "Project_" + i;
                btn[i] = pro_obj.GetComponent<Button>();
                btn[i].onClick.AddListener(delegate ()
                {
                    print("正在点击系统管理按钮");
                    string url = "http://cams.zcwjvr.com:8100/view/data.html?userId=" + LoginCon.X_UserID;
                    Debug.LogError("url : " + url);
                    Application.OpenURL(url);
                });
                pro_obj.transform.SetParent(GridPanel);
                pro_obj.transform.localScale = new Vector3(2, 2, 2);
                Button Project_UI = pro_obj.GetComponent<Button>();
                Project_UI.transform.Find("Text").GetComponent<Text>().text ="新建项目";
            }
        }
       

        


        //for (int i = 0; i < J_ProName.Length; i++)
        //{
        //    //if (J_ImageUrl[i] != "")
        //    //{
        //        if (inde == 0)
        //        {
        //            J_1 = i;
        //            J_ProBtn_1.text = J_ProName[i];
        //            J_ProBtn_1.transform.parent.gameObject.SetActive(true);
        //        }
        //        if (inde == 1)
        //        {
        //            J_2 = i;
        //            J_ProBtn_2.text = J_ProName[i];
        //            J_ProBtn_2.transform.parent.gameObject.SetActive(true);
        //        }
        //        if (inde == 2)
        //        {
        //            J_3 = i;
        //            J_ProBtn_3.text = J_ProName[i];
        //            J_ProBtn_3.transform.parent.gameObject.SetActive(true);
        //        }
        //        if (inde == 3)
        //        {
        //            J_4 = i;
        //            J_ProBtn_4.text = J_ProName[i];
        //            J_ProBtn_4.transform.parent.gameObject.SetActive(true);
        //        }
        //        inde++;
        //    //}
        //}
    }

    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            //case "ProBtn_1":
            //    SceneManager.LoadScene("AreaScene");
            //    AreaTipCon.instance.J_prostr = J_ProBtn_1.text;
            //    //AreaTipCon.instance.J_ImageUrl = J_ImageUrl[0];
            //    //AreaTipCon.instance.J_proNum = J_ProId[0];
            //    AreaTipCon.instance.J_ImageUrl = J_ImageUrl[J_1];
            //    AreaTipCon.instance.J_proNum = J_ProId[J_1];
            //    break;
            //case "ProBtn_2":
            //    SceneManager.LoadScene("AreaScene");
            //    AreaTipCon.instance.J_prostr = J_ProBtn_2.text;
            //    AreaTipCon.instance.J_ImageUrl = J_ImageUrl[J_2];
            //    AreaTipCon.instance.J_proNum = J_ProId[J_2];
            //    break;
            //case "ProBtn_3":
            //    SceneManager.LoadScene("AreaScene");
            //    AreaTipCon.instance.J_prostr = J_ProBtn_3.text;
            //    AreaTipCon.instance.J_ImageUrl = J_ImageUrl[J_3];
            //    AreaTipCon.instance.J_proNum = J_ProId[J_3];
            //    break;
            //case "ProBtn_4":
            //    SceneManager.LoadScene("AreaScene");
            //    AreaTipCon.instance.J_prostr = J_ProBtn_4.text;
            //    AreaTipCon.instance.J_ImageUrl = J_ImageUrl[J_4];
            //    AreaTipCon.instance.J_proNum = J_ProId[J_4];
            //    break;

            case "SetUp":
                if (X_Refresh.IsActive())
                {
                    X_Refresh.gameObject.SetActive(false);
                }
                else
                {
                    X_Refresh.gameObject.SetActive(true);
                }

                if (X_SignOut.IsActive())
                {
                    X_SignOut.gameObject.SetActive(false);
                }
                else
                {
                    X_SignOut.gameObject.SetActive(true);
                }
                break;         
        }
    }
    void Refresh_OnClick()
    {
        //JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/projects/projectlist", "fileid", "ProjectName", "ID", "", "", "");
        //Invoke("GetAreaImage", 1.5f);
        SceneManager.LoadScene("ProjectScene");
    }

    void SignOut_OnClick()
    {
        SceneManager.LoadScene("Login");
    }
}
