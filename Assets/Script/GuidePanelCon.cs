﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuidePanelCon : MonoBehaviour
{
    public Button J_B;

    void Start()
    {
        J_B.onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        this.gameObject.SetActive(false);
    }
}
