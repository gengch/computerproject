﻿using UnityEngine;
using UnityEngine.EventSystems;

public class GetIndex : MonoBehaviour
{

    public int index;


    public void OnSelect(BaseEventData eventData)
    {
        //对LoginPanel身上的InputNavigator脚本中的index赋值
        this.transform.parent.GetComponent<InputNavigator>().index = index;
    }
}
