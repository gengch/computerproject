﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class DataMgr : MonoSingleton<DataMgr>
{
    private JSONNode testNode;
    public JSONNode TestNode
    {
        get
        {
            if (testNode == null)
            {
                TextAsset textAsset = Resources.Load<TextAsset>("Data/TestData");

                testNode = JSONNode.Parse(textAsset.text);
                Debug.LogError("testNode : " + testNode.ToString());
            }

            return testNode;
        }
    }

    

    public void PrintDataFormat(string format, params object[] parameters)
    {
        Debug.LogWarningFormat(format, parameters);
    }

    public void PrintDataArr(string url, string jsonData)
    {
        JSONNode node = JSONNode.Parse(jsonData);

        Debug.LogWarningFormat("1 => url:{0},\nJsonData:{1}", url, node.ToPrettyString());
    }

    public void PrintDataArr(string url, string[] prams)
    {
        Debug.LogWarningFormat("2 => url:{0}, prams:\n{1}", url, string.Join("\n", prams));
    }

    private LayerMask selectedLayerMask;
    public LayerMask SelectedLayerMask
    {
        get
        {
            selectedLayerMask = GetLayerMask();
            return selectedLayerMask;
        }
    }

    private List<string> layerMaskNames = new List<string>()
    {
        "Default",
        "Model",
        "House",
        "Ding",
        "JG",
        "Xian",
        "Door"
    };

    public LayerMask GetLayerMask()
    {
        LayerMask clickLayerMask = 0;
        layerMaskNames.ForEach(item => clickLayerMask += 1 << LayerMask.NameToLayer(item));

        return clickLayerMask;
    }

}
