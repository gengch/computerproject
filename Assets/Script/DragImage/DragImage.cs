﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragImage : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    private string J_CUrl = "http://3dmis.zcwjvr.com/";
    private Image J_image;
    private GameObject J_drag;
    private GameObject J_obj;
    private RaycastHit J_hit;
    private bool J_isCreat;
    private bool J_isshow;
    private GameObject J_Obj;
    private SceneManger J_scenemanger;
    public GameObject J_jifang;
    private Text J_tiptext;
    private GameObject J_TipPanel;
    private GameObject J_uitip;
    private GameObject J_upanel;
    private Button Btn;
    private InputField J_ufield;
    private GameObject J_rayobj;
    private Text J_Tip;
    /****************待连接数据库后，此数据由数据库提供*********************/


    public int J_bojx;
    public int J_bojy;
    public int J_bojz;


    /**********************************************************************/
    public Camera J_camera;
    [Header("模型名称")]
    public string J_name;
    [Header("预制件名称")]
    public string J_nameprefab;

    public searchlogic J_searchlogic;
    public bool J_iswangxian;
    public bool J_isshebei;
    public bool J_isjigui;
    public int J_UNumber;
    public GameObject J_Player;
    public GameObject J_Camera;
    public GameObject J_NearCamera;

    [HideInInspector]
    public bool J_isFreeView = false;
    public static DragImage J_instance;
    TooltipAttribute ToopTip;

    private GameObject J_InfoPanel;

    private string[] J_EquipType;//设备类型
    private string[] J_EquipId;//设备id
    private string J_Equipid;//当前设备id
    private string[] J_Str_1;//可存放的位置

    private GameObject J_SBInfoPanel;
    private Text J_NameText;
    private Text J_TypeText;
    private Text J_BLText;
    private Text J_PosText;
    private InputField J_NameInput;
    private Button J_SaveBtn;
    private bool J_IsOK;
    private GameObject J_ChosePanel;

    private string[] J_EquiID;//设备id
    private string[] J_EquiName;//设备名称

    private string[] J_portid;//端口id
    private string[] J_portstatus;//端口状态
    private string[] J_porttype;//端口类型
    private string J_portname;//选中的端口
    private string J_portId;//选中的端口id
    private string J_endPortType;//目标端口状态
    private string J_cabinetid;//选中的机柜ID
    private string[] J_CabinetName;//机柜名称
    private string[] J_CabinetId;//机柜ID
    private string[] J_cablelength;//线缆长度
    private string[] J_cableid;//线缆ID
    private string J_CableId;//当前线缆ID

    public Transform J_rubbish;//废弃物品放置处
    public static DragImage J_DragImage;
    private GameObject J_Click;
    [HideInInspector]
    public bool J_IsFirst = true;
    [HideInInspector]
    public string J_EquipTypeId;

    private Dictionary<string, string> J_Data;
    private GameObject J_CabinetObj;//连接网线时选中的机柜
    private GameObject J_PortPanel;
    private InputField J_Input;
    private Text J_StartPort;//起始端口
    private Text J_EndPort;//目标端口
    private Text J_LenghtPort;//线缆名称
    //public Button J_BTn;
    private string[] J_ID = new string[30];
    private bool J_IsF;
    private GameObject J_Port;
    private string J_St;
    private string J_ClickEquipName;
    private string J_ClickPortName;

    private string J_CabinetID;
    private string J_CabinetPos;
    private string J_EquipTypeID;
    private string J_TypeName;

    private int J_CableIDIndex;

    public int J_CableIndex;

    private string[] X_CabinetPosition;//机柜位置
    public static string CabinetPosX;//机柜X坐标
    public static string CabinetPosY;//机柜Y坐标

    private Dictionary<string, string> X_Data;
    private Dictionary<string, string> X_Data_1;//不同字典测试，同一个字典偶尔出现解析错乱导致的问题
    private string X_RoomType;//
    private string X_RoomSize;//上传的房间型号
    private string[] X_Serial;

    private void Awake()
    {
        clickedLayerMask = DataMgr.Instance.GetLayerMask();
        J_CUrl = GameConst.ServerUrl;// File.ReadAllText("D://Url.txt");

        for (int i = 0; i < J_ID.Length; i++)
        {
            J_ID[i] = i + "";
        }
        J_upanel = transform.root.Find("UPanel").gameObject;
        J_upanel.transform.Find("Button").GetComponent<Button>().onClick.AddListener(delegate () { this.OnCLickBtn(0); });
        J_ufield = J_upanel.transform.Find("UField").GetComponent<InputField>();

        //if (J_BTn != null)
        //{
        //    J_BTn.onClick.AddListener(delegate () { this.OnCLickBtn(1); });
        //}

        transform.root.Find("PortPanel/Button").GetComponent<Button>().onClick.AddListener(delegate () { this.OnCLickBtn(1); });
        transform.root.Find("PortPanel/CCLLBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnCLickBtn(2); });
        transform.root.Find("ChosePanel/CCLLBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnCLickBtn(2); });
        J_upanel.transform.Find("CCBTN").GetComponent<Button>().onClick.AddListener(delegate () { this.OnCLickBtn(3); });
    }

    private void OnEnable()
    {
        if (AreaTipCon.instance.J_IsStart)
        {
            for (int i = 0; i < AreaTipCon.instance.J_CableInfo.Length; i++)
            {
                Debug.Log(AreaTipCon.instance.J_CableInfo[i] + "---------146---------");
            }
        }
        if (AreaTipCon.instance.J_IsStart)
        {
            if (J_iswangxian)
            {
                J_name = AreaTipCon.instance.J_CableInfo[J_CableIndex];
            }
        }
        J_PortPanel = transform.root.Find("PortPanel").gameObject;
        J_Input = J_PortPanel.transform.Find("InputField").GetComponent<InputField>();
        J_StartPort = J_PortPanel.transform.Find("StartPort").GetComponent<Text>();
        J_EndPort = J_PortPanel.transform.Find("EndPort").GetComponent<Text>();
        J_LenghtPort = J_PortPanel.transform.Find("LenghtPort").GetComponent<Text>();

        J_DragImage = this;
        J_ChosePanel = transform.root.Find("ChosePanel").gameObject;
        J_InfoPanel = transform.root.Find("InfoPanel").gameObject;
        //ToopTip = new TooltipAttribute("6666666666");
        J_SBInfoPanel = transform.root.Find("SBInfoPanel").gameObject;
        J_NameText = J_SBInfoPanel.transform.Find("NameText").GetComponent<Text>();
        J_TypeText = J_SBInfoPanel.transform.Find("TypeText").GetComponent<Text>();
        J_BLText = J_SBInfoPanel.transform.Find("BLText").GetComponent<Text>();
        J_PosText = J_SBInfoPanel.transform.Find("PosText").GetComponent<Text>();
        J_NameInput = J_SBInfoPanel.transform.Find("NameInput").GetComponent<InputField>();
        //J_SaveBtn = J_SBInfoPanel.transform.Find("").GetComponent<Button>();
        //J_SaveBtn.onClick.AddListener(delegate () { this.OnCLickBtn(1); });

        J_instance = this;
        J_Tip = transform.root.Find("Tip").GetComponent<Text>();

        J_uitip = Resources.Load<GameObject>("UITip");
        //J_jifang = GameObject.Find("jifang");
        J_scenemanger = GameObject.Find("SceneManger").GetComponent<SceneManger>();
        //获得按钮上图片信息
        J_image = GetComponent<Button>().image;
        J_TipPanel = transform.root.Find("TipPanel").gameObject;
        J_tiptext = transform.root.Find("TipPanel/TipText").GetComponent<Text>();
        J_tiptext.gameObject.SetActive(false);
        J_TipPanel.SetActive(false);
        J_upanel.SetActive(false);
    }

    private string J_cabid;

    private void OnCLickBtn(int index)
    {
        if (index == 0)
        {
            if (J_ufield.text != "" && J_rayobj != null)
            {
                //WaitSure();

                //待接口完成后开放

                J_EquipType = JsonContent.J_JsonContent.ReturnJsonData_1();
                J_EquipId = JsonContent.J_JsonContent.ReturnJsonData_2();

                for (int i = 0; i < J_EquipType.Length; i++)
                {
                    if (J_EquipType[i] == J_name)
                    {
                        J_Equipid = J_EquipId[i];
                        print(J_Equipid + "----J_Equipid----");
                    }
                }

                for (int i = 0; i < J_CabinetName.Length; i++)
                {
                    print(J_CabinetName[i] + "========" + J_rayobj.name);
                    if (J_rayobj.tag == "dangban")
                    {
                        if (J_CabinetName[i] == J_rayobj.transform.parent.parent.name)
                        {
                            J_cabid = J_CabinetId[i];
                        }
                    }
                    if (J_CabinetName[i] == J_rayobj.name)
                    {
                        J_cabid = J_CabinetId[i];
                    }
                }

                string endurl = J_cabid + "&EquipTypeID=" + J_EquipTypeId + "&CabinetPos=" + J_ufield.text;
                J_CabinetID = J_cabid;
                J_CabinetPos = J_ufield.text;
                J_EquipTypeID = J_EquipTypeId;
                string url = J_CUrl + "/APIS3d/device/api/web/equipments/cabinet-pos?CabinetID=" + endurl;
                print(url + "--------199------");
                StartCoroutine(WaitForJsonData_1(url));
            }
        }

        if (index == 1 && this.gameObject.name == J_St)
        {
            J_Data = new Dictionary<string, string>();
            Debug.Log(PlayerPrefs.GetString("机柜ID") + "------464864684684-------" + AreaTipCon.instance.J_EndPortCabinetId);
            //J_Data.Add("StartPortCabinetId", PlayerPrefs.GetString("机柜ID"));
            //J_Data.Add("EndPortCabinetId", AreaTipCon.instance.J_EndPortCabinetId);
            //J_Data.Add("StartPortCabinetId", J_CabinetID);
            //J_Data.Add("EndPortCabinetId", J_CabinetId[J_CableIndex]);
            J_Data.Add("StartPortID", PlayerPrefs.GetString("端口ID"));
            J_Data.Add("TargetPortID", AreaTipCon.instance.J_EndPortID);
            J_Data.Add("CabelID", AreaTipCon.instance.J_CableId[J_CableIDIndex]);
            J_Data.Add("PORT", AreaTipCon.instance.J_EndProtType);
            //J_Data.Add("CabelLength", J_Input.text);
            J_Data.Add("remark", AreaTipCon.instance.J_EndProtType);
            print(J_Input.text + "------J_Input.text------");
            transform.root.GetComponent<CreatModelCon>().J_IsThree = false;
            if (PlayerPrefs.HasKey("线缆位置X"))
            {
                PlayerPrefs.DeleteKey("线缆位置X");
                PlayerPrefs.DeleteKey("线缆位置Y");
                PlayerPrefs.DeleteKey("线缆位置Z");
                PlayerPrefs.DeleteKey("旋转位置X");
                PlayerPrefs.DeleteKey("旋转位置Y");
                PlayerPrefs.DeleteKey("旋转位置Z");
            }


            StartCoroutine(PostDataToServer(J_CUrl + "/APIS3d/device/api/web/portslink/save-portslink", J_Data));

            J_PortPanel.SetActive(false);
        }

        if (index == 2)
        {
            if (this.gameObject.name == J_St)
            {
                PlayerPrefs.DeleteKey("线缆位置X");
                PlayerPrefs.DeleteKey("线缆位置Y");
                PlayerPrefs.DeleteKey("线缆位置Z");
                PlayerPrefs.DeleteKey("旋转位置X");
                PlayerPrefs.DeleteKey("旋转位置Y");
                PlayerPrefs.DeleteKey("旋转位置Z");
                if (GameObject.Find("LS") != null)
                {
                    Destroy(GameObject.Find("LS"));
                }

                AreaTipCon.instance.J_IsFirst = true;
                PlayerPrefs.DeleteKey("连接端口信息");
                AreaTipCon.instance.J_port = null;
                J_PortPanel.SetActive(false);
                J_ChosePanel.SetActive(false);
                J_Tip.text = "取消连接";
                J_Tip.gameObject.SetActive(true);
                StartCoroutine(WaitCloseTip());
                AreaTipCon.instance.J_Posx = 0;
                AreaTipCon.instance.J_Posy = 0;
                if (J_obj != null && J_obj.tag == "Xian")
                {
                    J_obj.transform.position = new Vector3(10000, 10000, 10000);
                    J_obj.transform.gameObject.SetActive(false);
                    J_obj.transform.SetParent(J_rubbish);
                }
            }
        }

        if (index == 3)
        {
            J_isCreat = false;
            J_obj = null;
            J_upanel.SetActive(false);
        }
    }

    IEnumerator WaitForJsonData_1(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        string J_Str;
        J_Str = jo["code"].ToString();
        if (J_Str == "0")
        {
            //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
            //J_Str_1 = new string[jar.Count];
            //for (int i = 0; i < jar.Count; i++)
            //{
            //    J_Str_1[i] = jar[i]["cabinetpos"].ToString();
            //    if (J_Str_1[i] == J_ufield.text)
            //    {
            //        WaitSure();
            //        J_IsOK = true;
            //    }
            //}
            WaitSure();
        }
        else
        {
            //设备无法放入
            print("位置不可用，请重新选择");
            //J_upanel.SetActive(false);
            J_Tip.gameObject.SetActive(true);
            J_Tip.text = "位置不可用，请重新选择";
            J_ufield.text = "";
            StartCoroutine(WaitCloseTip());
        }
        //if (!J_IsOK)
        //{
        //    print("空间不足，设备无法放入");
        //    J_upanel.SetActive(false);
        //    J_Tip.gameObject.SetActive(true);
        //    J_Tip.text = "空间不足，设备无法放入";
        //    StartCoroutine(WaitCloseTip());
        //}

        print(jo["code"].ToString());
        print(jo.ToString());
        //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["code"].ToString());
        //print(jar.ToString());
    }

    void WaitSure()
    {
        GameObject[] J_dangban = new GameObject[10];
        for (int i = 0; i < J_UNumber; i++)
        {
            print(J_ufield.text + "----J_ufield.text----");
            int t = int.Parse(J_ufield.text) + i;
            if (J_rayobj.tag == "dangban")
            {
                J_dangban[i] = J_rayobj.transform.parent.parent.Find("dangban/dangban" + t).gameObject;
            }
            else
            {
                J_dangban[i] = J_rayobj.transform.Find("dangban/dangban" + t).gameObject;
            }

            //待之后确定是否开放

            //J_Data = new Dictionary<string, string>();
            //J_Data.Add("EquipName", J_obj.name);
            //J_Data.Add("EquipTypeID", J_EquipTypeID);
            //J_Data.Add("SwitchRoom", AreaTipCon.instance.J_floorid);
            //J_Data.Add("CabinetID", J_CabinetID);
            //J_Data.Add("CabinetPos", J_CabinetPos);
            //JsonContent.J_JsonContent.SaveDataToServer("equipment/equipSave", J_Data);
            //Invoke("ShowSavaInfo", 0.5f);
            //Invoke("WaitUpdateDate", 0.5f);

            //if (J_dangban[i].activeInHierarchy && i == 0)
            //{
            //    J_Tip.gameObject.SetActive(true);
            //    J_Tip.text = "此处有模型了，请重新选择";
            //    StartCoroutine(WaitCloseTip());
            //    return;
            //}
            //else if (J_dangban[i].activeInHierarchy && i != 0)
            //{
            //    J_Tip.gameObject.SetActive(true);
            //    J_Tip.text = "空间不足，请重新选择";
            //    StartCoroutine(WaitCloseTip());
            //    return;
            //}
        }


        //transform.root.GetComponent<CreatModelCon>().UpdateData();
        J_upanel.SetActive(false);
        J_InfoPanel.SetActive(false);

        DataMgr.Instance.PrintDataFormat("J_nameprefab : " + J_nameprefab);
        J_Obj = Resources.Load<GameObject>(J_nameprefab);
        J_obj = Instantiate(J_Obj);
        J_obj.AddComponent<CableCon>();
        J_obj.transform.SetParent(J_rayobj.transform);
        print(J_obj.transform.parent.name + "++++++++++++++++");
        J_obj.transform.position = J_dangban[0].transform.position;
        for (int i = 0; i < J_UNumber; i++)
        {
            //J_dangban[i].GetComponent<MeshRenderer>().enabled = true;
            if (i != 0)
            {
                J_dangban[i].GetComponent<MeshRenderer>().enabled = false;
            }
        }

        J_scenemanger.J_creatindex++;
        J_obj.name = "";
        //J_obj.transform.SetParent(J_jifang.transform);
        J_scenemanger.AddObj(J_obj);
        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_IsNew = true;
        J_SBInfoPanel.SetActive(true);
        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_EquipTypeID = J_EquipTypeId;
        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_obj;
        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_TypeText.text = J_name;
        J_NameText.text = J_obj.name;
        J_NameInput.text = J_obj.name;
        J_TypeText.text = J_name;
        J_BLText.text = J_rayobj.name;
        J_PosText.text = J_ufield.text;

        J_ufield.text = "";
        Debug.Log(J_ufield.text + "------443-----" + J_PosText.text);
        J_rayobj = null;
        J_Obj = null;
    }

    void WaitUpdateDate()
    {
        transform.root.GetComponent<CreatModelCon>().UpdateData();
    }

    void ShowSavaInfo()
    {
        string SaveData = JsonContent.J_JsonContent.ReturnSaveData();
        if (SaveData == "")
        {

        }
        else
        {

        }
    }

    //开始拖拽触发
    public void OnBeginDrag(PointerEventData eventData)
    {
        //检测该按钮上是否有图片
        if (J_image.sprite == null)
        {
            DataMgr.Instance.PrintDataFormat("Current component of 'Image' have none 'Sprite'.");
            return;
        }
        //创建一个新物体
        J_drag = new GameObject("Draging");
        //设置父子关系
        J_drag.transform.SetParent(eventData.pointerDrag.transform.parent);
        //设置初始位置
        J_drag.transform.localPosition = Vector3.zero;
        //设置缩放比例
        J_drag.transform.localScale = Vector3.one;
        //给新物体添加组件
        Image J_goImg = J_drag.AddComponent<Image>();
        //使射线穿透该图片
        //J_goImg.GetComponent<Image>().raycastTarget = enabled;
        //给新物体添加对应图片
        J_goImg.sprite = J_image.sprite;
        //减少内存消耗
        J_goImg.raycastTarget = false;
        //待接口完成后开放
        //获取设备类型和id
        if (J_isshebei || J_iswangxian)
        {
            JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/equipmenttype/get-equipmenttype", "EquipTypeName", "ID", "", "", "", "");
            J_Data = new Dictionary<string, string>();

            J_Data.Add("id", AreaTipCon.instance.J_floorid);
            //J_Data.Add("userName", PlayerPrefs.GetString("账号"));
            J_Data.Add("UserID", LoginCon.X_UserID);
            J_Data.Add("RolesID", LoginCon.X_RolesID);
            print(AreaTipCon.instance.J_floorid + "------" + PlayerPrefs.GetString("账号"));
            string url = J_CUrl + "/APIS3d/device/api/web/cabinets/get-cabinets";
            StartCoroutine(WaitForJsonData(url, J_Data, "CabinetName", "ID"));
        }
    }

    IEnumerator WaitForJsonData(string url, Dictionary<string, string> post, string node_1, string node_2)
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);

        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());
        J_CabinetName = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_CabinetName[i] = jar[i][node_1].ToString();
            print(J_CabinetName[i] + node_1 + "---node_1---");
        }

        if (node_2 != "")
        {
            J_CabinetId = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_CabinetId[i] = jar[i][node_2].ToString();
                print(J_CabinetId[i] + node_2 + "---node_2---" + "----url     " + url);
            }
        }

        for (int i = 0; i < J_CabinetName.Length; i++)
        {
            print(J_CabinetName[i] + "----367-----" + J_CabinetId[i]);
        }
    }

    //拖拽过程中触发
    public void OnDrag(PointerEventData eventData)
    {
        //检测新创建物体是否为空
        if (J_drag == null)
            return;
        //新创建物体跟随鼠标移动
        J_drag.transform.position = Input.mousePosition;
        J_St = this.gameObject.name;
        J_TypeName = this.J_name;
    }

    //拖拽结束触发
    public void OnEndDrag(PointerEventData eventData)
    {
        DataMgr.Instance.PrintDataFormat("OnEndDrag ..");
        //同意在场景中创建模型
        J_isCreat = true;

        J_CableIDIndex = J_CableIndex;
        //消除新物体
        Destroy(J_drag);
        //对新物体重新赋值
        J_drag = null;
    }

    LayerMask clickedLayerMask;
    //在指定地点创建模型
    private void Update()
    {
        Ray J_ray = J_camera.ScreenPointToRay(Input.mousePosition);
        if (J_nameprefab == "xian" || J_nameprefab == "xian 1")
        {
            if (Input.GetMouseButton(0))
            {
                if (J_drag == null)
                {
                    return;
                }

                
                if (Physics.Raycast(J_ray, out J_hit, 1000f, clickedLayerMask.value))
                {
                    if (J_hit.collider.gameObject.tag == "jigui")
                    {
                        J_CabinetObj = J_hit.collider.gameObject;
                        //J_CabinetObj.GetComponent<Rigidbody>().useGravity = false;
                        J_CabinetObj.GetComponent<BoxCollider>().enabled = false;
                    }
                    if (J_hit.collider.gameObject.tag == "Port" || J_hit.collider.gameObject.tag == "Area")
                    {
                        if (!transform.root.GetComponent<CreatModelCon>().J_IsUpdate)
                        {
                            transform.root.GetComponent<CreatModelCon>().J_IsUpdate = true;
                            transform.root.GetComponent<CreatModelCon>().UpdateEquipInfo();
                            transform.root.GetComponent<CreatModelCon>().J_ClickModel = J_hit.collider.transform.parent.parent.gameObject;
                        }
                        J_drag.GetComponent<Image>().color = new Color(1, 1, 1, 0);
                        print("图片应该已经消失了");
                        //J_Player.SetActive(false);
                        //J_Camera.GetComponent<Camera>().enabled = false;
                        //J_NearCamera.SetActive(true);
                        //J_NearCamera.GetComponent<Camera>().depth = 1;

                        if (J_hit.collider.gameObject.transform.parent.name == "JX")
                        {
                            J_Camera.GetComponent<FollowPlayer>().enabled = false;
                            J_Camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                            J_Camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                            J_Camera.GetComponent<FollowPlayer>().Target = J_hit.collider.gameObject.transform.parent.gameObject;
                            J_Camera.transform.position = J_hit.collider.gameObject.transform.parent.parent.position + J_hit.collider.gameObject.transform.parent.parent.parent.forward * 0.5f;
                            J_Camera.GetComponent<FollowPlayer>().enabled = true;
                        }
                        else
                        {
                            J_Camera.GetComponent<FollowPlayer>().enabled = false;
                            J_Camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                            J_Camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                            J_Camera.GetComponent<FollowPlayer>().Target = J_hit.collider.gameObject.transform.parent.gameObject;
                            //J_Camera.transform.position = J_hit.collider.gameObject.transform.parent.position - J_hit.collider.gameObject.transform.parent.forward * 0.5f;
                            J_Camera.transform.position = J_hit.collider.gameObject.transform.parent.parent.position + J_hit.collider.gameObject.transform.parent.parent.parent.forward * 0.5f;
                            J_Camera.GetComponent<FollowPlayer>().enabled = true;
                        }
                    }
                    else
                    {
                        transform.root.GetComponent<CreatModelCon>().J_IsUpdate = false;
                        J_drag.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        //J_Player.SetActive(true);
                        //J_Camera.GetComponent<Camera>().enabled = true;
                        //J_NearCamera.SetActive(false);
                    }
                    if (Input.GetMouseButtonUp(0))
                    {
                        //J_NearCamera.GetComponent<Camera>().depth = -1;
                        //print("代码执行了");

                    }
                }
                else
                {
                    J_drag.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (Physics.Raycast(J_ray, out J_hit, 1000f, clickedLayerMask.value))
            {
                //网线连接
                if (J_nameprefab == "xian" || J_nameprefab == "xian 1")
                {
                    if (J_hit.collider.gameObject.tag == "Port" && J_isCreat /*|| !AreaTipCon.instance.J_IsFirst*/)
                    {
                        if (this.gameObject.name == J_St)
                        {
                            if (EventSystem.current.IsPointerOverGameObject())
                            {
                                print("鼠标点到UI上了");
                                return;
                            }
                            J_Port = J_hit.collider.gameObject;
                            //WaitContent();
                            J_portname = int.Parse(J_hit.collider.gameObject.name) - 1 + "";
                            J_Click = J_hit.collider.transform.parent.parent.gameObject;
                            //
                            string url = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
                            //string url = J_CUrl+"switch/equipMentSelect?id=3";
                            StartCoroutine(GetEquipInfo(url, "EquipName", "ID", "CabinetID"));
                            //string url = "/equipment/PortsidByEquipment";
                            //JsonContent.J_JsonContent.ContentJson_1(url, "id", "portstatus", "porttype", "", ""); 
                        }

                    }
                    else if (J_hit.collider.gameObject.tag == "Area" && J_isCreat)
                    {
                        //J_Tip.text = "";
                        J_Camera.GetComponent<FollowPlayer>().enabled = false;
                        J_Camera.GetComponent<FollowPlayer>().Target = null;
                        J_Camera.GetComponent<FollowPlayer>().MinDistance = 1.5f;
                        J_Camera.GetComponent<FollowPlayer>().enabled = true;
                    }
                    //else if (!AreaTipCon.instance.J_IsFirst)
                    //{
                    //    J_Port = J_hit.collider.gameObject;
                    //    //WaitContent();
                    //    J_portname = int.Parse(J_hit.collider.gameObject.name) - 1 + "";
                    //    J_Click = J_hit.collider.transform.parent.parent.gameObject;
                    //    //
                    //    string url = J_CUrl+"switch/equipMentSelect?id=" + AreaTipCon.instance.J_floorid;
                    //    //string url = J_CUrl+"switch/equipMentSelect?id=3";
                    //    StartCoroutine(GetEquipInfo(url, "equipname", "id", "cabinetid"));
                    //}
                }
                //模型创建
                else
                {
                    DataMgr.Instance.PrintDataFormat("J_hit.collider.gameObject.tag : {0},J_isCreat:{1}", J_hit.collider.gameObject.tag, J_isCreat, J_hit.collider.gameObject);
                    if (J_hit.collider.gameObject.tag == "Terrain" && J_isCreat)
                    {
                        //每次拖拽只创建一次模型
                        J_isCreat = false;
                        //模型生成在指定位置
                        //J_obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        //J_obj.transform.position = J_hit.point;
                        //J_obj = Instantiate(J_Obj, new Vector3(J_bojx, J_bojy, J_bojz), Quaternion.identity);

                        J_Obj = Resources.Load<GameObject>(J_nameprefab);

                        //if (J_isjigui)
                        //{
                        //    J_scenemanger.J_jiguiindex++;
                        //    GameObject ui = Instantiate(J_uitip, J_obj.transform.position + Vector3.up * 2.5f, Quaternion.identity);
                        //    ui.transform.Find("Button/Text").GetComponent<Text>().text = "" + J_scenemanger.J_jiguiindex;
                        //    ui.transform.SetParent(J_obj.transform);
                        //}

                        //J_searchlogic.SearchObj();
                        //J_obj.layer = 8;
                        if (J_isjigui)
                        {


                            //下面注释的是：拖动图片生成机柜时，机柜位置自动挪到相对应的Serial序号的位置，此时机柜未保存。
                            X_Data = new Dictionary<string, string>();
                            X_Data.Add("id", AreaTipCon.instance.J_floorid);
                            JsonContent.J_JsonContent.ContentJson_4_1("/APIS/device/api/web/switchrooms/get-switchrooms-byid", X_Data, "RoomType");
                            X_RoomType = JsonContent.J_JsonContent.ReturnJsonData_4_1();
                            Debug.Log(AreaTipCon.instance.J_floorid + "当前房间类型是：" + X_RoomType);

                            X_Data_1 = new Dictionary<string, string>();
                            X_Data_1.Add("RoomID", AreaTipCon.instance.J_floorid);
                            Debug.Log("当前新建机柜的机房ID是：  " + AreaTipCon.instance.J_floorid);

                            JsonContent.J_JsonContent.ContentJson_5("/APIS/device/api/web/cabinets/room-position", X_Data_1, "Serial");
                            X_Serial = JsonContent.J_JsonContent.ReturnJsonData_5_1();
                            if (X_Serial==null)
                            {

                                Debug.Log("机房机柜已经满");

                            }
                            else
                            {
                                Debug.Log("当前机房内可用位置第一个：    " + X_Serial[0]);

                            }


                            J_Data = new Dictionary<string, string>();
                            J_Data.Add("RoomType", X_RoomType);
                            if (J_nameprefab == "jigui01")
                            {
                                J_Data.Add("CabinetsType", "1200");
                            }
                            else if (J_nameprefab == "jigui02")
                            {
                                J_Data.Add("CabinetsType", "600");
                            }


                            J_Data.Add("Serial", X_Serial[0]);
                            StartCoroutine(WaitForJsonPosXY(J_CUrl+"/APIS/device/api/web/cabinets/get-xy", J_Data, "PosX", "PosY"));

                            Debug.Log("机柜位置是：=========" + CabinetPosX + CabinetPosY);

                            //Vector3 cPosition = new Vector3(-float.Parse(CabinetPosX), 0, float.Parse(CabinetPosY));




                            J_obj = Instantiate(J_Obj,J_hit.point, Quaternion.identity);//生成模型并设置位置和旋转

                            J_obj.name = J_nameprefab + J_scenemanger.J_creatindex;
                            J_obj.transform.SetParent(J_jifang.transform);
                            J_scenemanger.J_creatindex++;

                            J_SBInfoPanel.SetActive(false);
                            J_InfoPanel.SetActive(true);
                            J_InfoPanel.transform.Find("NameText").GetComponent<Text>().text = J_obj.name;
                            J_InfoPanel.transform.Find("PlantText").GetComponent<Text>().text = "";
                            J_InfoPanel.transform.Find("NumText").GetComponent<Text>().text = "";
                            J_InfoPanel.transform.Find("PropertyText").GetComponent<Text>().text = "";
                            J_InfoPanel.transform.Find("NameInput").GetComponent<InputField>().text = "";
                            J_InfoPanel.transform.Find("PlantInput").GetComponent<InputField>().text = "";
                            J_InfoPanel.transform.Find("NumInput").GetComponent<InputField>().text = "";
                            J_InfoPanel.transform.Find("PropertyInput").GetComponent<InputField>().text = "";
                            J_InfoPanel.transform.Find("BuyTimeText").GetComponent<Text>().text = "";
                            J_InfoPanel.transform.Find("TimeText").GetComponent<Text>().text = "";
                            this.gameObject.transform.root.GetComponent<CreatModelCon>().J_IsNew = true;
                            this.gameObject.transform.root.GetComponent<CreatModelCon>().J_ClickObj = J_obj;
                            J_obj = null;
                            GameObject[] G = GameObject.FindGameObjectsWithTag("jigui");
                            AreaTipCon.instance.J_CabinetNum = G.Length.ToString();
                        }
                        if (J_isshebei)
                        {
                            J_InfoPanel.SetActive(false);
                            J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_Obj;
                            J_SBInfoPanel.SetActive(true);
                        }
                    }

                    DataMgr.Instance.PrintDataFormat("J_isshebei:{0},J_hit.collider.gameObject.tag:{1}, J_isCreat :{2}", J_isshebei, J_hit.collider.gameObject.tag, J_isCreat);
                    if (J_isshebei)
                    {
                        if ((J_hit.collider.gameObject.tag == "jigui" || J_hit.collider.gameObject.tag == "dangban") && J_isCreat)
                        {
                            J_isCreat = false;
                            J_rayobj = J_hit.collider.gameObject;
                            J_upanel.SetActive(true);
                        }
                    }
                    if (J_isCreat)
                    {
                        J_isCreat = false;
                    }
                }
            }
            J_isCreat = false;
        }
        if (J_isshow)
        {
            J_tiptext.gameObject.transform.position = Input.mousePosition;
        }
    }

    IEnumerator WaitForJsonPosXY(string url, Dictionary<string, string> post, string node_1,string node_2)
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);

        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        if (jo["code"].ToString() == "0")
        {
            //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
            //print(jar.ToString());
            CabinetPosX = jo["data"]["PosX"].ToString();
            CabinetPosY = jo["data"]["PosY"].ToString();
            Debug.Log("X坐标是:  " + CabinetPosX + "    Y坐标是：" + CabinetPosY);
            
        }
        else
        {
            if (jo["code"].ToString() == "-1")
            {
                Debug.Log("失败..");
            }
        }
        yield return new WaitForSeconds(1f);
    }



    IEnumerator GetEquipInfo(string url, string node_1, string node_2, string node_3)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());
        J_EquiName = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_EquiName[i] = jar[i][node_1].ToString();
            //print(J_EquiName[i] + node_1 + "---node_1---");
        }

        if (node_2 != "")
        {
            J_EquiID = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_EquiID[i] = jar[i][node_2].ToString();
                //print(J_EquiID[i] + node_2 + "---node_2---" + "----url     " + url);
            }
        }

        if (node_3 != "")
        {
            J_CabinetId = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_CabinetId[i] = jar[i][node_3].ToString();
                //print(J_CabinetId[i] + node_2 + "---node_2---" + "----url     " + url);
            }
        }

        for (int i = 0; i < J_EquiName.Length; i++)
        {
            print(J_EquiName[i] + "---537---" + J_EquiID[i] + "---537---" + J_CabinetId[i] + "---537---" + J_Click.name);
            if (J_Click.name == J_EquiName[i])
            {
                //print(J_EquiID[i] + "-----J_EquiID[i]-----");
                string url1 = "/APIS3d/device/api/web/ports/get-ports?id=" + J_EquiID[i];
                print(url1 + "-----url1-----696-----");
                JsonContent.J_JsonContent.ContentJson_1(url1, "ID", "portstatus", "portType", "", "", "");

                //等待接受数据
                Invoke("GetDate", 0.5f);
            }
        }
    }

    private bool J_IsContent = true;
    private bool J_IsOk;
    private bool J_IsFirstContent;

    private int inde1;
    private int inde2;

    void GetDate()
    {
        J_portid = JsonContent.J_JsonContent.ReturnJsonData_1();//端口id
        J_portstatus = JsonContent.J_JsonContent.ReturnJsonData_2();//端口状态
        J_porttype = JsonContent.J_JsonContent.ReturnJsonData_3();//端口类型

        for (int i = 0; i < J_EquiName.Length; i++)
        {
            if (J_Port.transform.parent.parent.name == J_EquiName[i])
            {
                //Debug.Log(J_CabinetName.Length + "   " + J_CabinetId.Length);
                //Debug.Log(J_CabinetName[i] + "---13545346-----");
                //Debug.Log(J_CabinetId[i] + "-----41641864------");
                J_cabinetid = J_CabinetId[i];
            }
        }

        for (int i = 0; i < J_portid.Length; i++)
        {
            if (J_ID[i] == J_portname)
            {
                if (J_portstatus[i] == "未连接")
                {
                    string st = "设备" + J_Port.transform.parent.parent.name + "端口" + J_Port.name;
                    if (st != PlayerPrefs.GetString("连接端口信息"))
                    {
                        //J_IsContent = false;
                        J_portId = J_portid[i];
                        inde1 = i;
                        print(inde1 + "-------------inde1-----------");
                        J_IsOk = true;
                        if ((J_porttype[i] =="光口"&& J_TypeName=="1")||(J_porttype[i]=="网口"&&J_TypeName=="2"))
                        {
                            J_endPortType = J_porttype[i];
                            AreaTipCon.instance.J_EndProtType = J_endPortType;
                            WaitContent();
                            Debug.Log("解析到的类型" + J_porttype[i] + "      应该是ui上显示的" + J_TypeName);
                        }
                        else
                        {
                            J_Tip.text = "线缆类型选择错误";
                            Debug.Log("解析到的类型"+J_porttype[i] + "      应该是ui上显示的" + J_TypeName);
                            J_Tip.gameObject.SetActive(true);
                            StartCoroutine(WaitCloseTip());
                            return;
                        }
                    }
                    else if (st == PlayerPrefs.GetString("连接端口信息"))
                    {
                        print("端口被占用");
                        J_Tip.text = "无法重复连接";
                        J_Tip.gameObject.SetActive(true);
                        StartCoroutine(WaitCloseTip());
                        J_Camera.GetComponent<FollowPlayer>().enabled = false;
                        J_Camera.GetComponent<FollowPlayer>().Target = null;
                        J_Camera.GetComponent<FollowPlayer>().MinDistance = 1.5f;
                        J_Camera.GetComponent<FollowPlayer>().enabled = true;
                    }
                }
                else /*if (J_IsContent)*/
                {
                    print("端口被占用");
                    J_Tip.text = "端口被占用";
                    J_Tip.gameObject.SetActive(true);
                    StartCoroutine(WaitCloseTip());
                    J_Camera.GetComponent<FollowPlayer>().enabled = false;
                    J_Camera.GetComponent<FollowPlayer>().Target = null;
                    J_Camera.GetComponent<FollowPlayer>().MinDistance = 1.5f;
                    J_Camera.GetComponent<FollowPlayer>().enabled = true;
                }
            }
        }
        if (!J_IsOk)
        {
            //J_Tip.text = "未知错误，无法连接";
            //J_Tip.gameObject.SetActive(true);
            //StartCoroutine(WaitChangeCamera());
            J_Camera.GetComponent<FollowPlayer>().enabled = false;
            J_Camera.GetComponent<FollowPlayer>().Target = null;
            J_Camera.GetComponent<FollowPlayer>().MinDistance = 1.5f;
            J_Camera.GetComponent<FollowPlayer>().enabled = true;
        }
        J_CabinetObj.GetComponent<BoxCollider>().enabled = true;
        //J_CabinetObj.GetComponent<Rigidbody>().useGravity = true;
    }

    void WaitContent()
    {
        if (this.gameObject.name == J_St)
        {
            if (!AreaTipCon.instance.J_IsFirst)
            {
                if (PlayerPrefs.GetString("X") == "1")
                {
                    if (this.gameObject.name != "xianBtn")
                    {
                        J_Tip.gameObject.SetActive(true);
                        J_Tip.text = "线缆类型选择错误";
                        StartCoroutine(WaitCloseTip());
                        return;
                    }
                }
                else
                {
                    if (this.gameObject.name == "xianBtn")
                    {
                        J_Tip.gameObject.SetActive(true);
                        J_Tip.text = "线缆类型选择错误";
                        StartCoroutine(WaitCloseTip());
                        return;
                    }
                }
            }

            PlayerPrefs.DeleteKey("连接端口信息");
            J_Obj = Resources.Load<GameObject>(J_nameprefab);
            J_obj = Instantiate(J_Obj, J_hit.collider.gameObject.transform.position /*- J_hit.collider.gameObject.transform.up * 0.01f*/, J_hit.collider.gameObject.transform.rotation);
            J_obj.name = J_nameprefab + J_scenemanger.J_creatindex;
            J_obj.transform.SetParent(J_Port.transform);
            J_scenemanger.J_creatindex++;
            J_isCreat = false;
            if (!J_isFreeView)
            {
                StartCoroutine(WaitChangeCamera());
            }
            else
            {
                J_Camera.GetComponent<FollowPlayer>().Target = null;
                J_Camera.GetComponent<FollowPlayer>().enabled = false;
            }

            Debug.Log(AreaTipCon.instance.J_IsFirst + "====现在的连接情况是====");
            if (!AreaTipCon.instance.J_IsFirst)
            {
                AreaTipCon.instance.J_IsFirst = true;
                //AreaTipCon.instance.J_StartPortID = "";
                AreaTipCon.instance.J_EndPortID = J_portId;
                
                AreaTipCon.instance.J_EndPortCabinetId = J_cabinetid;
                J_IsFirst = true;
                PlayerPrefs.SetString("结束端口信息", "设备" + J_Port.transform.parent.parent.name + "端口" + J_Port.name);
                JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/cables/get-cables", "CableLength", "ID", "", "", "", "");
                J_IsF = true;
                Invoke("GetCableInfo", 0.5f);
                print("+++++++++++++++++++" + this.gameObject.name);
            }

            else if (AreaTipCon.instance.J_IsFirst)
            {
                if (this.gameObject.name == "xianBtn")
                {
                    PlayerPrefs.SetString("X", "1");
                    PlayerPrefs.SetString("线缆模型名称", "xian");
                    Debug.Log("------进行了首次连接------");
                }
                else
                {
                    PlayerPrefs.SetString("X", "2");
                    PlayerPrefs.SetString("线缆模型名称", "xian 1");
                    Debug.Log("------进行了首次连接------");
                }
                PlayerPrefs.SetString("端口", J_Port.transform.parent.parent.parent.name + "/" + J_Port.transform.parent.parent.name + "/SB/" + J_Port.name);
                PlayerPrefs.SetFloat("线缆位置X", J_obj.transform.position.x);
                PlayerPrefs.SetFloat("线缆位置Y", J_obj.transform.position.y);
                PlayerPrefs.SetFloat("线缆位置Z", J_obj.transform.position.z);
                PlayerPrefs.SetFloat("旋转位置X", J_obj.transform.eulerAngles.x);
                PlayerPrefs.SetFloat("旋转位置Y", J_obj.transform.eulerAngles.y);
                PlayerPrefs.SetFloat("旋转位置Z", J_obj.transform.eulerAngles.z);

                PlayerPrefs.SetString("机柜端口", J_Port.transform.parent.parent.parent.name + "/" + J_Port.transform.parent.parent.name);
                AreaTipCon.instance.J_port = J_Port.transform.parent.parent.gameObject;
                //if (AreaTipCon.instance.J_StartPortID == "")
                //{
                //    AreaTipCon.instance.J_StartPortID = J_portId;
                //}

                //if (AreaTipCon.instance.J_StartPortCabinetId == "")
                //{
                //    AreaTipCon.instance.J_StartPortCabinetId = J_cabinetid;
                //}
                PlayerPrefs.SetString("机柜ID", J_cabinetid);
                PlayerPrefs.SetString("端口ID", J_portId);
                PlayerPrefs.SetString("起始端口信息", "设备" + J_Port.transform.parent.parent.name + "端口" + J_Port.name);
                PlayerPrefs.SetString("连接端口信息", "设备" + J_Port.transform.parent.parent.name + "端口" + J_Port.name);
                AreaTipCon.instance.J_IsFirst = false;
                J_ChosePanel.SetActive(true);
                print("------------------" + this.gameObject.name);
            }

            J_LenghtPort.text = J_obj.name;
        }
    }

    void GetCableInfo()
    {
        J_cablelength = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_cableid = JsonContent.J_JsonContent.ReturnJsonData_1();
        for (int i = 0; i < J_cablelength.Length; i++)
        {
            print(J_cableid[i]);
            if (J_cablelength[i] == "0" && J_IsF)
            {
                J_IsF = false;
                J_CableId = J_cableid[i];
                AreaTipCon.instance.J_CabelID = J_CableId;

                //   /cables/linkPorts

            }
        }

        J_StartPort.text = PlayerPrefs.GetString("起始端口信息");
        J_EndPort.text = PlayerPrefs.GetString("结束端口信息");
        J_PortPanel.SetActive(true);
    }

    IEnumerator PostDataToServer(string url, Dictionary<string, string> post)
    {
        print(url + post);
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {
            print(saveElectric.Key + "------" + saveElectric.Value.ToString());
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }

        WWW www = new WWW(url, wwwForm);
        Debug.Log("url 地址==" + www.url + "----" + this.gameObject.name);
        yield return www;
        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text + "----www.text----");
        //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["code"].ToString());
        print(jo["code"].ToString() + "-------863------");
        if (jo["code"].ToString() != "0")
        {
            //Newtonsoft.Json.Linq.JArray jar1 = Newtonsoft.Json.Linq.JArray.Parse(jo["message"].ToString());
            J_Tip.gameObject.SetActive(true);
            J_Tip.text = "连接失败";
            StartCoroutine(WaitCloseTip());
            Destroy(J_obj);
        }
        else
        {
            print("----连接成功----");
            J_Tip.text = "连接成功";
            J_Tip.gameObject.SetActive(true);
            //if (AreaTipCon.instance.J_port != null)
            //{
            //    AreaTipCon.instance.J_port.GetComponent<CableCon>().J_IsUpdate = true;
            //    AreaTipCon.instance.J_port = null;
            //}
            //else
            //{
            //    print("数据丢失了");
            //}
            //Debug.Log(PlayerPrefs.GetString("机柜端口"));
            //J_jifang.transform.Find(PlayerPrefs.GetString("机柜端口")).GetComponent<CableCon>().J_IsUpdate = true;
            GameObject[] J_Model = GameObject.FindGameObjectsWithTag("Model");
            foreach (GameObject J_Item in J_Model)
            {
                J_Item.GetComponent<CableCon>().OffCable();
                Debug.Log("--------设备强刷数据库了-------生成线缆了------");
            }
            if (GameObject.Find("LS") != null)
            {
                Destroy(GameObject.Find("LS"));
            }
            J_obj = null;
            StartCoroutine(WaitCloseTip());
            //CableCon.J_cable.J_IsUpdate = true;
        }
        print(www.text);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        J_TipPanel.SetActive(true);
        //J_isshow = true;
        J_tiptext.text = J_name;
        J_tiptext.gameObject.SetActive(true);
        //J_tiptext.gameObject.transform.position = Input.mousePosition;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        J_TipPanel.SetActive(false);
        J_isshow = false;
        J_tiptext.gameObject.SetActive(false);
    }

    IEnumerator WaitCloseTip()
    {
        yield return new WaitForSeconds(2);
        J_Tip.gameObject.SetActive(false);
    }

    IEnumerator WaitChangeCamera()
    {
        yield return new WaitForSeconds(1.5f);

        J_Camera.GetComponent<FollowPlayer>().enabled = false;
        J_Camera.GetComponent<FollowPlayer>().Target = null;
        J_Camera.GetComponent<FollowPlayer>().MinDistance = 1.5f;
        J_Camera.GetComponent<FollowPlayer>().enabled = true;
    }
}
