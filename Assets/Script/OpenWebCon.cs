﻿using UnityEngine;
using UnityEngine.UI;

public class OpenWebCon : MonoBehaviour
{

    void Start()
    {
        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }
    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            case "ManaBtn":
                //设备管理
                //Application.ExternalEval("window.open('http://wwww.baidu.com','_blank')");
                Application.OpenURL("http://wwww.baidu.com");
                print("正在点击设备管理按钮");
                break;
            case "StatisBtn":
                //统计分析
                //Application.ExternalEval("window.open('http://wwww.baidu.com','_blank')");
                Application.OpenURL("http://cams.zcwjvr.com:8100/view/dynamic.html?userId=" + LoginCon.X_UserID);
                print("正在点击统计分析按钮");
                break;
            case "WorkBtn":
                //工单管理
                //Application.ExternalEval("window.open('http://wwww.baidu.com','_blank')");
                Application.OpenURL("http://cams.zcwjvr.com:8100/view/workorders.html?userId="+LoginCon.X_UserID);
                print("正在点击工单管理按钮");
                break;
            case "SysBtn":
                //系统管理
                //Application.ExternalEval("window.open('http://wwww.baidu.com','_blank')");
                Application.OpenURL("http://cams.zcwjvr.com:8100/view/data.html?userId="+LoginCon.X_UserID);
                print("正在点击系统管理按钮");
                break;
        }
    }
}
