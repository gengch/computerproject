﻿public class Singleton<T> where T :class
{
    private static Singleton<T> _instance = null;

    public static Singleton<T> Instance
    {
        get
        {
            if (_instance == null)
                _instance = new Singleton<T>();
            return _instance;
        }

    }

}