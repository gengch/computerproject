﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FollowPlayer : MonoBehaviour
{
    private Transform Player;
    private Vector3 offsetPosition;

    private Transform Position;//相机初始位置

    private bool IsView;//更改视角
    private bool isShowing = true;
    private bool isActive;

    public GameObject J_ding;

    public GameObject Target;
    [HideInInspector]
    public bool J_IsThree = true;//当前是否为第三人称

    private void OnEnable()
    {
        if (Target != null)
        {
            Player = Target.transform;
        }
        else
        {
            Player = GameObject.FindGameObjectWithTag("Player").transform;
            Position = Player.parent.Find("Position");
            transform.position = Position.position;
        }
        offsetPosition = transform.position - Player.position;
        transform.LookAt(Player.position);
    }

    public Ray ray;
    public RaycastHit hitInfo;
    public GameObject gameObj;
    private GameObject J_Parent;
    void Start()
    {
        Button[] J_Buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_Buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }

    private void LateUpdate()
    {
        if (Target != null && Target.transform.parent != null)
        {
            J_Parent = Target.transform.parent.gameObject;
            J_Parent.transform.GetComponent<BoxCollider>().enabled = false;
        }
        else if (J_Parent != null)
        {
            J_Parent.transform.GetComponent<BoxCollider>().enabled = true;
        }

        transform.position = offsetPosition + Player.position;
        //ScrollView();
        RotatePlayer();
        //RotateView();
        transform.LookAt(Player.position);
        if (Input.GetMouseButton(0))
        {
            //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            ray = GameObject.FindGameObjectWithTag("PlayerCamera")
                .GetComponent<Camera>()
                .ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                Debug.DrawLine(ray.origin, hitInfo.point);
                gameObj = hitInfo.collider.gameObject;
                //Debug.Log("点击物体的名字 " + gameObj.name);
            }
            //RayObj();
        }
    }

    private float rotateSpeed1 = 2.0f;
    private Vector3 normalQuaternion = new Vector3(0, 90, 0);
    private Vector2 first = Vector2.zero;
    private Vector2 second = Vector2.zero;
    private void RotatePlayer()
    {
        if (Input.GetMouseButton(1))
        {
            Debug.LogError("RotatePlayer .. ");
            //记录鼠标拖动的位置 　　
            second = Input.mousePosition;
            if (second.x < first.x)
            {
                //拖动的位置的x坐标比按下的位置的x坐标小时,响应向左事件 　　
                Player.transform.rotation = Player.transform.rotation * Quaternion.Euler(-normalQuaternion * rotateSpeed1 * Time.deltaTime);
            }
            if (second.x > first.x)
            {
                //拖动的位置的x坐标比按下的位置的x坐标大时,响应向右事件
                Player.transform.rotation = Player.transform.rotation * Quaternion.Euler(normalQuaternion * rotateSpeed1 * Time.deltaTime);
            }
            first = second;
        }

    }


    private float distance;

    public float scrollSpeed = 10;
    public float MinDistance = 1.5f;
    public float MaxDistance = 10;

    private bool isRotating;
    public float rotateSpeed = 2;
    private bool J_isZX;
    private bool J_isYX;
    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            case "FDBtn":
                distance -= 0.5f;
                CameraMove();
                break;
            case "SXBtn":
                distance += 0.5f;
                CameraMove();
                break;
            case "YXBtn":
                J_isYX = true;
                break;
            case "ZXBtn":
                J_isZX = true;
                break;
        }
    }


    private void RotateView()
    {
        //鼠标右键按下可以旋转视野
        if (J_isYX)
        {
            J_isYX = false;
            transform.RotateAround(Player.position, Player.up, -rotateSpeed * 5);
        }
        if (J_isZX)
        {
            J_isZX = false;
            transform.RotateAround(Player.position, Player.up, rotateSpeed * 5);
        }

        if (Input.GetMouseButton(1))
        {
            Vector3 originalPosition = transform.position;
            Quaternion originalRotation = transform.rotation;
            transform.RotateAround(Player.position, Player.up, rotateSpeed * Input.GetAxis("Mouse X"));
            if (J_IsThree)
            {
                transform.RotateAround(Player.position, transform.right, -rotateSpeed * Input.GetAxis("Mouse Y"));
            }
            else
            {
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            }
            float x = transform.eulerAngles.x;
            // print(x);
            //旋转的范围为10度到80度
            if ((x < -50 || x > 80) && x < 353)
            {
                transform.position = originalPosition;
                transform.rotation = originalRotation;
            }
        }

        //Vector3 originalPosition1 = transform.position;
        //Quaternion originalRotation1 = transform.rotation;
        //transform.RotateAround(Player.position, Player.up, rotateSpeed * Input.GetAxis("Mouse X"));
        //transform.RotateAround(Player.position, transform.right, -rotateSpeed * Input.GetAxis("Mouse Y"));
        //float x1 = transform.eulerAngles.x;
        //// print(x);
        ////旋转的范围为10度到80度
        //if ((x1 < -50 || x1 > 80) && x1 < 353)
        //{
        //    transform.position = originalPosition1;
        //    transform.rotation = originalRotation1;
        //}

        offsetPosition = transform.position - Player.position;

        distance = offsetPosition.magnitude;
        //向前滑动拉近 向后滑动拉远
        distance -= Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
        if (Input.GetKeyDown(KeyCode.Q))
        {
            print("相机向前移动");
            distance -= 0.5f;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            print("相机向后移动");
            distance += 0.5f;
        }
        CameraMove();
    }
    void CameraMove()
    {
        distance = Mathf.Clamp(distance, MinDistance, MaxDistance);
        offsetPosition = offsetPosition.normalized * distance;
    }
    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.gameObject.name == "ding")
    //    {
    //        if (this.gameObject.transform.position.y > 17.94f && isShowing)
    //        {
    //            other.gameObject.GetComponent<MeshRenderer>().enabled = false;
    //            other.gameObject.GetComponent<MeshCollider>().enabled = false;
    //            isShowing = false;
    //        }
    //        else if (this.gameObject.transform.position.y < 17.95f && !isShowing)
    //        {
    //            other.gameObject.GetComponent<MeshRenderer>().enabled = true;
    //            other.gameObject.GetComponent<MeshCollider>().enabled = true;
    //            isShowing = true;
    //        }
    //    }
    //}
}
