﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputNavigator : MonoBehaviour
{
    private EventSystem eventsystem;
    public GameObject[] inputFields;
    [HideInInspector]
    public int index = 0;


    void Start()
    {
        eventsystem = EventSystem.current;
        eventsystem.SetSelectedGameObject(inputFields[index].gameObject, new BaseEventData(eventsystem));
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            index++;
            if (index >= inputFields.Length)
            {
                index = 0;
            }
            eventsystem.SetSelectedGameObject(inputFields[index].gameObject, new BaseEventData(eventsystem));
        }
    }
}
