﻿using UnityEngine;
using UnityEngine.UI;

public class SearchData : MonoBehaviour
{
    private string[] switchroomname;
    private string[] roomtype;
    private string[] id;
    private string[] floorareaid;

    private InputField SeaField;

    void Start()
    {


        SeaField = transform.Find("SeaField").GetComponent<InputField>();
        Button[] J_Buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button tempbtn in J_Buttons)
        {
            Button J_tempbtn = tempbtn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }

    void OnClickBtn(Button btn)
    {
        if (btn.name == "findnamegridcontent(Clone)")
        {

        }
        if (btn.name == "SeaBtn")
        {
            //链接数据库，开始搜索
            JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/switchrooms/searche-switchrooms", "SwitchRoomName", "RoomType", "ID", "FloorAreaID", "","");

            Invoke("WaitData", 0.5f);
        }
    }

    void WaitData()
    {
        switchroomname = JsonContent.J_JsonContent.ReturnJsonData_1();
        roomtype = JsonContent.J_JsonContent.ReturnJsonData_2();
        id = JsonContent.J_JsonContent.ReturnJsonData_3();
        floorareaid = JsonContent.J_JsonContent.ReturnJsonData_4();

        for (int i = 0; i < switchroomname.Length; i++)
        {
            if (SeaField.text != "" && switchroomname[i].Contains(SeaField.text))
            {
                //此处生成按钮等== SeaField.text
                print(switchroomname[i] + "----switchroomname[i]----");
                //print(roomtype[i] + "----roomtype[i]----");
                //print(id[i] + "----id[i]----");
                //print(floorareaid[i] + "----floorareaid[i]----");
            }
        }
    }
}
