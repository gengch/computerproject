﻿using UnityEngine;

public class BirdCameraCon : MonoBehaviour
{
    public float rotateSpeed = 5;
    private float J_timer;
    private Vector3 J_startpos;
    private Quaternion J_startrot;

    //private float xSpeed = 125.0f;
    //private float ySpeed = 60.0f;

    //方向灵敏度  
    public float sensitivityX = 10F;
    public float sensitivityY = 10F;
    //上下最大视角(Y视角)  
    public float minimumY = -60F;
    public float maximumY = 60F;
    float rotationY = 0F;

    void Start()
    {
        J_startpos = transform.position;
        J_startrot = transform.rotation;
    }
    private void LateUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(-transform.forward);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(transform.forward);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(-transform.right);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(transform.right);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(transform.up);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Translate(-transform.up);
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            transform.position += transform.forward * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 100;
        }
        if (Input.GetMouseButton(1))
        {
            //根据鼠标移动的快慢(增量), 获得相机左右旋转的角度(处理X)  
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            //根据鼠标移动的快慢(增量), 获得相机上下旋转的角度(处理Y)  
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            //角度限制. rotationY小于min,返回min. 大于max,返回max. 否则返回value   
            //rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            //总体设置一下相机角度  
            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
            //this.transform.Translate(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"))*Time.deltaTime);


            //Vector3 originalPosition = transform.position;
            //Quaternion originalRotation = transform.rotation;
            //transform.RotateAround(transform.position, transform.up, rotateSpeed * Input.GetAxis("Mouse X"));
            //transform.RotateAround(transform.position, transform.right, -rotateSpeed * Input.GetAxis("Mouse Y"));
            //transform.RotateAround(transform.position, Vector3.up, rotateSpeed * Input.GetAxis("Mouse X"));
            //transform.RotateAround(transform.position, Vector3.right, -rotateSpeed * Input.GetAxis("Mouse Y"));

            //float x = 90;
            //float y = 30;
            //float z = 0;
            ////设置横向旋转距离
            //x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
            ////设置纵向旋转距离
            //y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            //transform.rotation = Quaternion.Euler(y, x, z); 
            //float x = transform.eulerAngles.x;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.rotation = new Quaternion(0, 0.5f, 0, 0);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            J_timer += Time.deltaTime;
            if (J_timer >= 2)
            {
                transform.position = J_startpos;
                transform.rotation = J_startrot;
            }
        }
    }
}
