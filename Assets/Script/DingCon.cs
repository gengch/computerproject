﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DingCon : MonoBehaviour
{
    public GameObject J_ding;
    [HideInInspector]
    public bool J_IsThree = true;
    void Update()
    {
        if (J_IsThree)
        {
            if (SceneManager.GetActiveScene().name != "Computer_1")
            {
                if (this.gameObject.transform.position.y > 17.94f)
                {
                    J_ding.GetComponent<MeshRenderer>().enabled = false;
                    J_ding.GetComponent<MeshCollider>().enabled = false;
                    J_ding.SetActive(false);
                }
                else if (this.gameObject.transform.position.y < 17.95f)
                {
                    J_ding.SetActive(true);
                    J_ding.GetComponent<MeshRenderer>().enabled = true;
                    //J_ding.GetComponent<MeshCollider>().enabled = true;
                }
            } 
        }
    }
}
