﻿using UnityEngine;

public class PortConnectCon : MonoBehaviour
{
    private Vector3 J_StartPos;

    private void Start()
    {
        J_StartPos = this.gameObject.transform.localPosition;
    }

    private void Update()
    {
        this.gameObject.transform.localPosition = J_StartPos;
    }

    //每次移动后纠正网线的正确的位置
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Port")
        {
            if (Input.GetMouseButtonUp(0))
            {
                print(other.gameObject.name);
                this.gameObject.transform.position = other.gameObject.transform.position;
                this.gameObject.transform.rotation = other.gameObject.transform.rotation;
                this.gameObject.transform.SetParent(other.gameObject.transform);
            }
        }
    }
}
