﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// 机房搜索脚本
/// </summary>
public class searchlogic : MonoBehaviour
{
    private string[] switchroomname;//机房名称
    private string[] roomtype;//机房类型
    private string[] id;//机房id
    private string[] floorareaid;//楼层id

    private GameObject gridnameshow;
    private Text J_NumText;//搜索个数

    public Button searchbutton;
    public GameObject J_rect;
    /// <summary>  
    /// List 里存的是场景里所有的被查找物体的名称和位置  
    /// </summary>  
    ///   
    List<Transform> allnameslist = new List<Transform>();

    Transform[] J_transform;
    string inputtext = "";
    GameObject searchbg;//生成的每一行的显示物体  

    public GameObject J_camera;
    public GameObject J_player;
    public GameObject J_scrollbar;
    //public Text J_tiptext;
    private int J_index;
    public GameObject J_Girl;

    private void Start()
    {
        //J_NumText = transform.parent.parent.Find("NumText").GetComponent<Text>();
        J_NumText = transform.Find("SearchPanel/NumText").GetComponent<Text>();
        J_NumText.gameObject.SetActive(false);
        //J_tiptext.gameObject.SetActive(false);
        string gridpath = "findnamegridcontent";//生成列表的路径  
        gridnameshow = Resources.Load(gridpath, typeof(GameObject)) as GameObject;//加载生成的子物体  

        SearchObj();

        //初始化查找按钮  
        searchbutton.onClick.AddListener(Findbutton);
    }

    public void SearchObj()
    {
        //找到场景中所有的目标物体，然后添加到list里  
        //GameObject go = GameObject.Find("Tfather");
        //J_transform = GameObject.FindObjectsOfType<Transform>();
        //if (go != null)
        //{
        //    //找到场景中所有的目标物体，然后添加到list里  
        //    allnameslist = new List<Transform>();

        //    foreach (Transform child in J_transform)
        //    {
        //        allnameslist.Add(child);

        //        //Debug.Log(child.gameObject.name);
        //    }
        //}
    }
    /// <summary>  
    /// 查找方法触发  
    /// </summary>  
    public void Findbutton()
    {
        J_rect.SetActive(true);
        J_scrollbar.SetActive(true);

        J_scrollbar.GetComponent<Scrollbar>().value = 0;
        Debug.Log("Serachlogic----FindButton");
        JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/switchrooms/searche-switchrooms", "SwitchRoomName", "RoomType", "ID", "FloorAreaID", "","");
        Invoke("WaitData", 0.5f);
        SearchObj();
        //Grid的长度随着生成物体个数变化  
        J_Girl.GetComponent<RectTransform>().sizeDelta = new Vector2(J_Girl.GetComponent<RectTransform>().sizeDelta.x, 0);
        if (SceneManager.GetActiveScene().name == "Computer_1")
        {
            inputtext = GameObject.Find("Canvas").transform.Find("SaidPanel/SeaField").GetComponent<InputField>().text;
        }
        else
        {
            inputtext = GameObject.Find("Canvas").transform.Find("SaidPanel/SeaField").GetComponent<InputField>().text;
        }

        // 清空grid里的所有东西  
        List<Transform> lst = new List<Transform>();
        foreach (Transform child in J_Girl.transform)
        {
            lst.Add(child);
            //Debug.Log(child.gameObject.name);
        }
        for (int i = 0; i < lst.Count; i++)
        {
            Destroy(lst[i].gameObject);
        }
        SearchObj();
        compared();

        Invoke("WaitAddListener", 0.5f);
    }

    void WaitAddListener()
    {
        //对新生成的按钮添加监听
        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            print(J_btn.name + "---------");
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }

    void WaitData()
    {
        J_index = 0;//每次查询之前进行清零处理。
        switchroomname = JsonContent.J_JsonContent.ReturnJsonData_1();
        roomtype = JsonContent.J_JsonContent.ReturnJsonData_2();
        id = JsonContent.J_JsonContent.ReturnJsonData_3();
        floorareaid = JsonContent.J_JsonContent.ReturnJsonData_4();

        for (int i = 0; i < switchroomname.Length; i++)
        {
            print(inputtext + "---inputtext---");
            if (inputtext != "" && switchroomname[i].Contains(inputtext))
            {
                Generatenamegrids(switchroomname[i], id[i], "机房");//生成列表  
                //此处生成按钮等== SeaField.text
                print(switchroomname[i] + "----switchroomname[i]----");
                //print(roomtype[i] + "----roomtype[i]----");
                //print(id[i] + "----id[i]----");
                //print(floorareaid[i] + "----floorareaid[i]----");
                J_index++;
            }
            if (inputtext == "")
            {
                Generatenamegrids(switchroomname[i], id[i], "机房");//生成列表  
                J_index++;
            }
        }
        Debug.LogWarning("索引个数" + J_index.ToString());

        J_NumText.text = J_index.ToString();
        J_NumText.gameObject.SetActive(true);

        Invoke("Change", 0.5f);
    }

    /// <summary>
    /// 对新生成的按钮添加监听
    /// </summary>
    /// <param name="btn"></param>
    private void OnClickBtn(Button btn)
    {
        if (btn.name == "CB")
        {
            btn.transform.parent.gameObject.SetActive(false);
            J_scrollbar.SetActive(false);
        }
        if (btn.name == "CheckBtn")
        {
            GameObject.Find("Canvas").transform.Find("SaidPanel/SeaField").GetComponent<InputField>().text = "";
            //this.gameObject.transform.parent.parent.gameObject.SetActive(false);
            //J_scrollbar.SetActive(false);
            //若机房id相同则不进行场景切换
            if (btn.transform.parent.Find("Text").GetComponent<Text>().text != AreaTipCon.instance.J_floorid)
            {
                for (int i = 0; i < switchroomname.Length; i++)
                {
                    if (btn.transform.parent.Find("positontext").GetComponent<Text>().text == switchroomname[i] && btn.transform.parent.Find("Text").GetComponent<Text>().text == id[i])
                    {
                        print(i + "------------" + roomtype[i]);
                        if (roomtype[i] == "机房")
                        {
                            AreaTipCon.instance.J_floorstr = switchroomname[i];
                            AreaTipCon.instance.J_floorid = id[i];
                            SceneManager.LoadScene("jifang01");
                        }
                        if (roomtype[i] == "配电室")
                        {
                            AreaTipCon.instance.J_floorstr = switchroomname[i];
                            AreaTipCon.instance.J_floorid = id[i];
                            //确定后场景后更换
                            SceneManager.LoadScene("jifang01");
                        }
                    }
                }
            }

            #region 弃用的搜索功能
            //J_transform = GameObject.FindObjectsOfType<Transform>();
            //foreach (Transform child in J_transform)
            //{
            //    print(child.gameObject.name);
            //    if (btn.transform.Find("positontext").GetComponent<Text>().text == child.gameObject.name)
            //    {
            //            GameObject.Find("Canvas").transform.Find("SaidPanel/SeaField").GetComponent<InputField>().text = "";
            //        J_camera.SetActive(false);
            //        if (SceneManager.GetActiveScene().name == "Computer_1")
            //        {
            //            string houseindex = "";
            //            string house = "";
            //            if (child.gameObject.name.Length > 5)
            //            {
            //                house = child.gameObject.name.Substring(0, 5);
            //            }
            //            //if (house == "House")
            //            //{
            //            houseindex = child.gameObject.name.Substring(child.gameObject.name.Length - 1, 1);
            //            if (child.transform.position.x > 50)
            //            {
            //                J_player.transform.position = new Vector3(child.gameObject.transform.position.x - 33.04f, J_player.transform.position.y, child.gameObject.transform.position.z);
            //                J_player.transform.rotation = new Quaternion(0, 0.7f, 0, 0.7f);
            //            }
            //            else
            //            {
            //                J_player.transform.position = new Vector3(child.gameObject.transform.position.x + 33.04f, J_player.transform.position.y, child.gameObject.transform.position.z);
            //                J_player.transform.rotation = new Quaternion(0, -0.7f, 0, 0.7f);
            //            }
            //            //}
            //            //J_player.transform.position = new Vector3(child.gameObject.transform.position.x, J_player.transform.position.y, child.gameObject.transform.position.z + 60);
            //        }
            //        else
            //        {
            //            string jigui = "";
            //            if (child.gameObject.name.Length > 2)
            //            {
            //                jigui = child.gameObject.name.Substring(0, 2);
            //            }
            //            if (jigui == "机柜")
            //            {
            //                J_player.transform.position = new Vector3(child.gameObject.transform.position.x, J_player.transform.position.y, child.gameObject.transform.position.z) - child.gameObject.transform.right * 0.3f + child.gameObject.transform.forward * 1.3f;
            //                //J_player.transform.rotation = new Quaternion(0, 0.5f, 0, 0);

            //                J_player.transform.eulerAngles = new Vector3(0, child.gameObject.transform.eulerAngles.y + 180, 0);
            //            }
            //            else
            //            {
            //                J_player.transform.position = new Vector3(child.gameObject.transform.position.x, J_player.transform.position.y, child.gameObject.transform.position.z + 0.7f) + child.gameObject.transform.forward * 0.7f;
            //                J_player.transform.eulerAngles = new Vector3(0, child.gameObject.transform.eulerAngles.y + 180, 0);
            //            }
            //        }
            //        J_camera.SetActive(true);
            //        //J_scrollbar.SetActive(false);

            //    }
            //}
            #endregion

            // 清空grid里的所有东西  
            List<Transform> lst = new List<Transform>();
            foreach (Transform child in J_Girl.transform)
            {
                lst.Add(child);
                Debug.Log(child.gameObject.name);
            }
            for (int i = 0; i < lst.Count; i++)
            {
                Destroy(lst[i].gameObject);
            }

            J_scrollbar.SetActive(false);
            J_Girl.transform.parent.parent.gameObject.SetActive(false);
        }
    }

    /// <summary>  
    /// 将查找文字与库里的数据对比，然后生成列表  
    /// </summary>  
    private void compared()
    {
        //for (int i = 0; i < allnameslist.Count; i++)
        //{
        //    Debug.Log("list 里有：" + allnameslist[i].name);
        //    if (inputtext != "" && allnameslist[i].name.Contains(inputtext))
        //    {
        //        Debug.Log("包含" + "。。。。该字符串是：" + allnameslist[i]);
        //        Generatenamegrids(allnameslist[i].name);//生成列表  
        //    }

        //    else
        //    {
        //        Debug.Log("不包含");
        //        //J_tiptext.gameObject.SetActive(true);
        //        //J_tiptext.text = "没有该设备";
        //        //StartCoroutine(Obj());
        //    }
        //}


    }

    /// <summary>  
    /// 生成整个gird子物体  
    /// </summary>  
    private void Generatenamegrids(string thename, string id, string typename)
    {
        //生成record的物体、  
        searchbg = Instantiate(gridnameshow, this.transform.position, Quaternion.identity) as GameObject;
        searchbg.transform.SetParent(J_Girl.transform);
        searchbg.transform.localScale = new Vector3(1, 1, 1);
        searchbg.transform.Find("positontext").GetComponent<Text>().text = thename;
        searchbg.transform.Find("Text").GetComponent<Text>().text = id;
        searchbg.transform.Find("TypeText").GetComponent<Text>().text = typename;

        //本grid长度加60  
        //this.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(this.gameObject.GetComponent<RectTransform>().sizeDelta.x, this.gameObject.GetComponent<RectTransform>().sizeDelta.y + this.GetComponent<GridLayoutGroup>().cellSize.y + this.GetComponent<GridLayoutGroup>().spacing.y);
        J_Girl.GetComponent<RectTransform>().sizeDelta = new Vector2(J_Girl.GetComponent<RectTransform>().sizeDelta.x,
            J_Girl.GetComponent<RectTransform>().sizeDelta.y +
            J_Girl.GetComponent<GridLayoutGroup>().cellSize.y +
            J_Girl.GetComponent<GridLayoutGroup>().spacing.y);
        J_scrollbar.GetComponent<Scrollbar>().value = 1;
    }
    void Change()
    {
        J_scrollbar.GetComponent<Scrollbar>().value = 1;
    }
    //private IEnumerator Obj()
    //{
    //    yield return new WaitForSeconds(2);
    //    J_tiptext.gameObject.SetActive(false);
    //}
}
