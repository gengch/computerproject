﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EqCon : MonoBehaviour
{
    private Vector3 J_Pos;
    private bool J_IsDestroy;
    private bool J_IsDec;
    private bool J_IsAdd;
    private Camera J_camera;
    private RaycastHit J_hit;
    [HideInInspector]
    public Vector3 J_EquipPos;
    private GameObject J_SBInfoPanel;
    private float J_StartTime;
    private string J_MoveName;

    private float J_PosX;
    private float J_PosZ;
    public string J_CabinetPos;
    void Start()
    {
        J_SBInfoPanel = GameObject.Find("Canvas").transform.Find("SBInfoPanel").gameObject;
        J_EquipPos = this.gameObject.transform.localPosition;
        J_Pos = this.transform.localPosition;
        J_camera = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();

        J_PosX = this.gameObject.transform.localPosition.x;
        J_PosZ = this.gameObject.transform.localPosition.z;
    }

    void Update()
    {
        this.gameObject.transform.localPosition = new Vector3(J_PosX, this.gameObject.transform.localPosition.y, J_PosZ);
        if (!J_SBInfoPanel.activeInHierarchy)
        {
            J_StartTime += Time.deltaTime;
            if (J_StartTime >= 3)
            {
                this.gameObject.transform.localPosition = J_EquipPos;
            }
        }
        else
        {
            J_StartTime = 0;
        }
        if (Input.GetMouseButton(0) && J_Pos != this.transform.localPosition && !J_IsAdd)
        {
            //J_IsDec = true;
            J_IsAdd = true;

            J_IsDestroy = true;
            print("---正在移动机柜的位置--" + this.gameObject.transform.name);
            J_MoveName = this.gameObject.transform.name;
        }
        if (Input.GetMouseButtonUp(0) && J_IsDestroy)
        {
            gameObject.AddComponent<Rigidbody>();
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

            J_IsDestroy = false;
            J_IsDec = true;
            J_IsAdd = false;
            Invoke("WaitChangePos", 0.5f);
        }
        //if (Input.GetMouseButtonUp(0))
        //{
        //    Ray J_ray = J_camera.ScreenPointToRay(Input.mousePosition);
        //    if (Physics.Raycast(J_ray, out J_hit))
        //    {
        //        if (J_hit.collider.gameObject.tag != "jigui" || J_hit.collider.gameObject.tag != "dangban" || J_hit.collider.gameObject.tag != "Model" || J_hit.collider.gameObject.name != "jiguimen")
        //        {
        //            this.gameObject.transform.position = J_Pos;
        //        }
        //    }
        //}
    }

    void WaitChangePos()
    {
        print("----这个方法执行了----");
        this.gameObject.transform.localPosition = J_Pos;
        Destroy(gameObject.GetComponent<Rigidbody>());
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "dangban" && J_IsDec && this.gameObject.name == J_MoveName)
        {
            this.gameObject.transform.position = other.gameObject.transform.position;
            //other.gameObject.GetComponent<MeshRenderer>().enabled = true;
            J_Pos = this.gameObject.transform.localPosition;
            J_IsDec = false;
            print("----------设备移动过了--------" + other.gameObject.name.Length);
            SBInfoPanelCon.J_SB.J_IsMove = true;
            if (other.gameObject.name.Length == 8)
            {
                SBInfoPanelCon.J_SB.J_A = this.gameObject.name;
                SBInfoPanelCon.J_SB.J_PosText.text = other.gameObject.name.Substring(7, 1);
                SBInfoPanelCon.J_SB.J_IsChange = true;
                //SBInfoPanelCon.J_SB.UpdateEquipInfo();

                print(other.gameObject.name.Substring(7, 1) + "---------------64------------" + this.gameObject.name);
                J_CabinetPos = other.gameObject.name.Substring(7, 1);
                PlayerPrefs.SetString(this.gameObject.name + "位置", other.gameObject.name.Substring(7, 1));
            }
            if (other.gameObject.name.Length == 9)
            {
                SBInfoPanelCon.J_SB.J_A = this.gameObject.name;
                SBInfoPanelCon.J_SB.J_PosText.text = other.gameObject.name.Substring(7, 2);
                SBInfoPanelCon.J_SB.J_IsChange = true;
                print(other.gameObject.name.Substring(7, 2) + "------------72--------------" + this.gameObject.name);
                //SBInfoPanelCon.J_SB.UpdateEquipInfo();

                J_CabinetPos = other.gameObject.name.Substring(7, 2);

                PlayerPrefs.SetString(this.gameObject.name + "位置", other.gameObject.name.Substring(7, 2));
            }
            //SBInfoPanelCon.J_SB.J_PosText.text = other.gameObject.name;

            //PlayerPrefs.SetString(this.gameObject.name + "位置", other.gameObject.name);
            print("挡板位置出现了");
        }
        else
        {
            this.gameObject.transform.localPosition = J_Pos;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "dangban")
        {
            print(other.gameObject.name + "----挡板消失了----");
            other.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
