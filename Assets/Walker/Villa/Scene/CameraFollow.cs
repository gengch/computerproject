﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform objBeFollowed;

    public float hLength = 1;
    public float vHeight = 2;
    Vector3 gapAngles = Vector3.zero;
    Vector3 offset = Vector3.zero;

    Quaternion gapQuaternion = Quaternion.identity;
    private void Awake()
    {
        gapAngles = transform.localEulerAngles - objBeFollowed.localEulerAngles;
        offset = transform.position - objBeFollowed.position;;
    }

    private void Update()
    {
        Follow();
    }

    [ContextMenu("Follow")]
    private void Follow()
    {
        offset = objBeFollowed.forward * (-hLength) + objBeFollowed.up * vHeight;
        transform.position = objBeFollowed.position + offset;

        Vector3 nextAngles = gapAngles + objBeFollowed.localEulerAngles;
        transform.localEulerAngles = nextAngles;

    }
}
