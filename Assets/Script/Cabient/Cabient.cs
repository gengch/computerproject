﻿using UnityEngine;

public class Cabient : MonoBehaviour
{
    private bool J_IsDetection;
    private bool J_IsDestroy;
    private Vector3 J_Pos;
    private bool J_Add = true;

    void Start()
    {
        J_Pos = this.transform.parent.position;
    }

    void Update()
    {
        if (transform.parent.position != J_Pos && J_Add)
        {
            J_Add = false;
            gameObject.AddComponent<Rigidbody>();
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            print("正在移动机柜----" + this.transform.parent.name);
        }
        if (J_IsDestroy)
        {
            J_IsDestroy = false;
            Destroy(GetComponent<Rigidbody>());
            J_Add = true;
        }
        if (Input.GetMouseButton(0))
        {
            J_IsDetection = false;
        }
        if (Input.GetMouseButtonUp(0))
        {
            J_IsDetection = true;
            Invoke("ChangePos", 0.5f);
        }
    }

    void ChangePos()
    {
        J_Pos = this.transform.parent.position;
        Destroy(GetComponent<Rigidbody>());
        J_Add = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (J_IsDetection && other.gameObject.tag == "jigui" && other.gameObject != this.gameObject)
        {
            print("----机柜恢复原位置----");
            this.transform.parent.position = J_Pos;
            J_IsDestroy = true;
        }
    }
}
