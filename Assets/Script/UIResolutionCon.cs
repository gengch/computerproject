﻿using UnityEngine;
using UnityEngine.UI;

public class UIResolutionCon : MonoBehaviour
{
    public GameObject J_ChangeBtn;

    void Start()
    {
        var grid = J_ChangeBtn.GetComponent<GridLayoutGroup>();
        var rect = GetComponent<RectTransform>().rect;
        grid.cellSize = new Vector2(rect.width, grid.cellSize.y);
    }
}
