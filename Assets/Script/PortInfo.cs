﻿using UnityEngine;
using UnityEngine.UI;

public class PortInfo : MonoBehaviour
{
    private Text J_DText;
    private Text J_WText;
    private RaycastHit J_hit;
    private Camera J_camera;
    private GameObject J_canvas;
    void Start()
    {
        J_camera = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();
        J_canvas = transform.Find("Canvas").gameObject;
        J_DText = J_canvas.transform.Find("DText").GetComponent<Text>();
        J_WText = J_canvas.transform.Find("WText").GetComponent<Text>();
        J_canvas.SetActive(false);
    }

    void Update()
    {
        Ray J_ray = J_camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(J_ray, out J_hit))
        {
            if (J_hit.collider.gameObject.tag == "Port")
            {
                int index;
                //检测该物体的名字是否由纯数字组成
                if (int.TryParse(J_hit.collider.gameObject.name, out index))
                {
                    J_canvas.transform.position = J_hit.collider.gameObject.transform.position + Vector3.up * 0.05f;
                    J_canvas.SetActive(true);
                    J_DText.text = "端口" + index;
                    if (index <= 24)
                    {
                        J_WText.text = "网线端口";
                    }
                    else
                    {
                        J_WText.text = "光纤接口";
                    }
                }
            }
            else
            {
                J_canvas.SetActive(false);
            }
        }
    }
}
