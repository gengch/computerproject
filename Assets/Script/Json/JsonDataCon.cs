﻿using System.Collections;
using UnityEngine;

public class JsonDataCon : MonoBehaviour
{
    //可能在一个项目中需要好几个地方需要连接数据的，所以我们就把前面相同的部分摘出来
    private string url_1 = "http://3d5i3.zcwjvr.com/";

    //解析Json后内容存放的数组
    private string[] J_Str;

    public static JsonDataCon J_JsonDataCon;

    void Awake()
    {
        if (J_JsonDataCon == null)
        {
            J_JsonDataCon = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// 类型①
    /// </summary>
    /// <param name="url_2=>连接Json的后半部分链接"></param>
    /// <param name="node=>需要的数据前面的节点名称"></param>
    /// <param name="cont=>节点名称"></param>
    public void ContentJson_1(string url_2, string node, string cont)
    {
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_1(url, node, cont));
    }

    /// <summary>
    /// 类型②
    /// </summary>
    /// <param name="url_2"></param>
    /// <param name="node"></param>
    /// <param name="cont_1"></param>
    /// <param name="cont_2"></param>
    /// <param name="index=>Json相当于一个数组，当Json结构比较复杂时就需要使用数组下标来解决"></param>
    public void ContentJson_2(string url_2, string node, string cont_1, string cont_2, int index)
    {
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_2(url, node, cont_1, cont_2, index));
    }

    IEnumerator WaitForJsonData_1(string url, string node, string cont)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo[cont].ToString());
        J_Str = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_Str[i] = jar[i][node].ToString();
        }
        print(jar.ToString());
    }

    IEnumerator WaitForJsonData_2(string url, string node, string cont_1, string cont_2, int index)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo[cont_1].ToString());
        print(jar[0].ToString());
        Newtonsoft.Json.Linq.JArray newJar = Newtonsoft.Json.Linq.JArray.Parse(jar[index][cont_2].ToString());
        J_Str = new string[newJar.Count];
        for (int i = 0; i < newJar.Count; i++)
        {
            J_Str[i] = newJar[i][node].ToString();
        }
    }

    /// <summary>
    /// 得到需要的内容后通过此方法返回数组
    /// </summary>
    /// <returns></returns>
    public string[] ReturnJsonData()
    {
        return J_Str;
    }
}

