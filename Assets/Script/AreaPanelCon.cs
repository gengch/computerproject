﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class AreaPanelCon : MonoBehaviour
{
    private Transform J_enterpanel;//进入面板
    private GameObject J_areabtn;//按钮预制件
    private GameObject J_creatareabtn;//新创建的按钮
    [HideInInspector]
    public string J_str;

    private string[] J_AreaCont = new string[10];//区域名称数组
    private string[] J_IDCont = new string[10];//区域ID数组
    private Text J_ChangeText;
    private Text J_ProText;
    private int J_index;
    private GameObject J_TipPanel;
    private Image J_Panel;//AreaPanel下的图片,未上传imageurl时，不显示；有imageurl时显示
    private Image J_Panel_1;//AreaPanel上的图片,未上传imageurl时，显示.

    private WWW www;

    private GameObject J_LoadPanel;
    private Slider J_LoadSlider;
    private Text J_LoadText;

    private bool J_IsDestroy;
    private Text J_Text;
    private string J_AreaInfo;
    private void Awake()
    {
        J_LoadPanel = transform.parent.Find("LoadPanel").gameObject;
        J_LoadSlider = J_LoadPanel.transform.Find("LoadSlider").GetComponent<Slider>();
        J_LoadText = J_LoadPanel.transform.Find("LoadText").GetComponent<Text>();
        J_Text = J_LoadPanel.transform.Find("Text").GetComponent<Text>();
        J_Panel = transform.parent.Find("AreaPanel/Panel").GetComponent<Image>();
        J_Panel_1 = transform.parent.Find("AreaPanel").GetComponent<Image>();

        //string path_1 = AreaTipCon.instance.J_ImageUrl;
        //Debug.Log(path_1 + "------37-------");
        //StartCoroutine(LoadImage(path_1));
        J_Text.text = "正在进入" + AreaTipCon.instance.J_prostr;
    }

    //读取加载进度条
    private void Update()
    {
        if (www != null && !www.isDone)
        {
            J_IsDestroy = true;
            J_LoadSlider.value = www.progress;
            string J_LoadNum = (www.progress * 100).ToString();
            if (J_LoadNum.Length > 4)
            {
                J_LoadText.text = J_LoadNum.Substring(0, 4) + "%";
            }
            else
            {
                J_LoadText.text = J_LoadNum + "%";
            }
        }
        else if (J_IsDestroy)
        {
            J_IsDestroy = false;
            J_LoadSlider.value = 1;
            J_LoadText.text = "100%";
            J_LoadPanel.SetActive(false);
        }
    }

    //实时加载图片
    IEnumerator LoadImage(string path)
    {
        if (path != "")
        {

            www = new WWW(path);
            //Debug.Log(www.text);
            yield return www;
            Texture2D texture = www.texture;
            Sprite sprites = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            J_Panel.transform.Find("Image").GetComponent<Image>().sprite = sprites;
            www = null;
        }
        else
        {
            J_Panel.gameObject.SetActive(false);
            Texture2D  texture = (Texture2D)Resources.Load("AreaDefaultImage");
            Sprite sprites = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            J_Panel_1.sprite = sprites;
            J_LoadPanel.SetActive(false);
            yield return 1f;
        }
        
    }

    private void Start()
    {
        string path_1 = AreaTipCon.instance.J_ImageUrl;
        Debug.Log(path_1 + "------37-------");
        StartCoroutine(LoadImage(path_1));

        J_TipPanel = transform.Find("TipPanel").gameObject;
        J_TipPanel.SetActive(false);
        string url = "/APIS3d/device/api/web/areas/area-select?id=" + AreaTipCon.instance.J_proNum;
        JsonContent.J_JsonContent.ContentJson_1(url, "AreaName", "ID", "", "", "", "");
        transform.Find("ProBtn/Text").GetComponent<Text>().text = AreaTipCon.instance.J_prostr;
        Invoke("StartInfo", 0.5f);
    }

    void StartInfo()
    {
        J_AreaCont = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_IDCont = JsonContent.J_JsonContent.ReturnJsonData_2();
        J_areabtn = Resources.Load<GameObject>("AreaBtn");
        J_enterpanel = transform.Find("EnterBtn/EnterPanel");
        J_ChangeText = transform.Find("EnterBtn/Text").GetComponent<Text>();

        int ChildCount = J_enterpanel.childCount;
        for (int i = 0; i < ChildCount; i++)
        {
            Destroy(J_enterpanel.GetChild(0).gameObject);
        }

        for (int i = 0; i < J_AreaCont.Length; i++)
        {
            if (J_AreaCont[i] != "")
            {
                int index = i + 1;
                J_creatareabtn = Instantiate(J_areabtn);
                J_creatareabtn.name = "AreaBtn_" + index;
                Debug.Log("区域信息=" + J_AreaCont[i]);
                //确保不出现乱码
                char[] ch = J_AreaCont[i].ToCharArray();
                if (J_AreaCont[i] != null)
                {
                    if (ch[0] >= 0x4E00 && ch[0] <= 0x9FA5)
                    {

                    }
                    else
                    {
                        //第一个字符是非汉字时跳转场景
                        //SceneManager.LoadScene("ProjectScene");
                    }
                }
                J_creatareabtn.transform.Find("Text").GetComponent<Text>().text = J_AreaCont[i];
                J_creatareabtn.transform.SetParent(J_enterpanel);
            }
        }

        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                this.OnClickBtn(J_tempbtn);
            });
        }

        J_enterpanel.gameObject.SetActive(false);
        //JsonContent.J_JsonContent.ContentJson_1("/area/list", "id", "", "");
        //Invoke("StartID", 1);
    }

    void OnClickBtn(Button btn)
    {
        if (btn.name.Length >= 9)
        {
            if (btn.name.Substring(0, 7) == "AreaBtn")
            {
                J_ChangeText.text = btn.transform.Find("Text").GetComponent<Text>().text;
                J_AreaInfo = btn.transform.Find("Text").GetComponent<Text>().text;
                btn.transform.parent.gameObject.SetActive(false);
                int number;
                //确保所得到的是数字
                if (int.TryParse(btn.name.Substring(8, 1), out number))
                {
                    J_index = number;
                }
            }
        }
        switch (btn.name)
        {
            case "EnterBtn":
                //print("正在点击进入按钮");

                if (J_enterpanel.GetComponent<Image>().IsActive())
                {
                    J_enterpanel.gameObject.SetActive(false);
                }
                else
                {
                    J_enterpanel.gameObject.SetActive(true);
                }
                break;
            case "IntoBtn":
                if (J_index == 0)
                {
                    J_TipPanel.SetActive(true);
                    StartCoroutine(WaitCloseTipPanel());
                    return;
                }
                AreaTipCon.instance.J_areastr = J_AreaInfo;
                AreaTipCon.instance.J_AreaId = J_IDCont[J_index - 1];


                SceneManager.LoadScene("Computer_1");
                break;
            case "ProBtn":
                SceneManager.LoadScene("AreaScene");
                break;

            case "BackBtn":
                SceneManager.LoadScene("ProjectScene");
                break;
            case "Refresh":
                SceneManager.LoadScene("AreaScene");
                break;

                #region  暂时弃用的区域跳转功能
                //点击不同区域，获取不同数据，生成相应区域内的模型
                //case "AreaBtn_1":
                //    break;
                //case "AreaBtn_2":
                //    AreaTipCon.instance.J_areastr = "区域2";
                //    AreaTipCon.instance.J_AreaId = J_IDCont[1];
                //    SceneManager.LoadScene("Computer_1");
                //    break;
                //case "AreaBtn_3":
                //    AreaTipCon.instance.J_areastr = "区域3";
                //    AreaTipCon.instance.J_AreaId = J_IDCont[2];
                //    SceneManager.LoadScene("Computer_1");
                //    break;
                //case "AreaBtn_4":
                //    AreaTipCon.instance.J_areastr = "区域4";
                //    AreaTipCon.instance.J_AreaId = J_IDCont[3];
                //    SceneManager.LoadScene("Computer_1");
                //    break;
                //case "AreaBtn_5":
                //    AreaTipCon.instance.J_areastr = "区域5";
                //    AreaTipCon.instance.J_AreaId = J_IDCont[4];
                //    SceneManager.LoadScene("Computer_1");
                //    break;
                //case "AreaBtn_6":
                //    AreaTipCon.instance.J_areastr = "区域6";
                //    AreaTipCon.instance.J_AreaId = J_IDCont[5];
                //    SceneManager.LoadScene("Computer_1");
                //    break;
                //case "AreaBtn_7":
                //    AreaTipCon.instance.J_areastr = "区域7";
                //    AreaTipCon.instance.J_AreaId = J_IDCont[6];
                //    SceneManager.LoadScene("Computer_1");
                //    break;
                //case "AreaBtn_8":
                //    AreaTipCon.instance.J_areastr = "区域8";
                //    AreaTipCon.instance.J_AreaId = J_IDCont[7];
                //    SceneManager.LoadScene("Computer_1");
                //    break;
                #endregion

        }
    }

    IEnumerator WaitCloseTipPanel()
    {
        yield return new WaitForSeconds(1.5f);
        J_TipPanel.SetActive(false);
    }

    private void OnDestroy()
    {

        Debug.Log("这个代码执行了");
    }
}
