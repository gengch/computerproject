﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragImageCon : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IInitializePotentialDragHandler
{
    [Header("是否精准拖拽")]
    public bool J_isPrecision;

    //存储图片中心点与鼠标点击点的偏移量
    private Vector3 J_offset;

    //存储当前拖拽图片的RectTransform组件
    private RectTransform J_rt;

    void Start()
    {
        //初始化
        J_rt = gameObject.GetComponent<RectTransform>();
    }

    //开始拖拽触发
    public void OnBeginDrag(PointerEventData eventData)
    {
        print("-----开始拖拽了-----");
        //如果精准拖拽则进行计算偏移量操作
        if (J_isPrecision)
        {
            // 存储点击时的鼠标坐标
            Vector3 tWorldPos;
            //UI屏幕坐标转换为世界坐标
            RectTransformUtility.ScreenPointToWorldPointInRectangle(J_rt, eventData.position, eventData.pressEventCamera, out tWorldPos);
            //计算偏移量   
            J_offset = transform.position - tWorldPos;
        }
        //否则，默认偏移量为0
        else
        {
            J_offset = Vector3.zero;
        }
        SetDraggedPosition(eventData);
    }

    //鼠标按下还没开始拖拽时
    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
        
    }

    //拖拽过程中触发
    public void OnDrag(PointerEventData eventData)
    {
        print("-----正在拖拽中-----");
        SetDraggedPosition(eventData);
    }

    //拖拽结束触发
    public void OnEndDrag(PointerEventData eventData)
    {
        print("-----拖拽结束了-----");
        SetDraggedPosition(eventData);
    }

    private void SetDraggedPosition(PointerEventData eventData)
    {
        //存储当前鼠标所在位置
        Vector3 globalMousePos;
        //UI屏幕坐标转换为世界坐标
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(J_rt, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            //设置位置及偏移量
            J_rt.position = globalMousePos + J_offset;
        }
    }
}
