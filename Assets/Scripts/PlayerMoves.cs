﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMoves : MonoSingleton<PlayerMoves>
{
    public GameObject Player;
    private Vector3 J_initPos = new Vector3(0, 0, 0);
    public float h;
    public float v;
    public Transform J_cameraPos;
    Transform m_transform, m_camera; //人物自己以及相机的对象
    public float newSpeed = 5f;
    private Animator anim;
    private float J_time;
    private GameObject J_Parent;
    public GameObject J_Jifang;

    private bool J_IsMove;
    private bool J_ISMove = true;
    private bool J_IsStart;

    public GameObject J_SBInfoPanel;
    public GameObject J_InfoPanel;

    public Animator _Anim
    {
        get
        {
            if (anim == null)
            {
                anim = Player.GetComponent<Animator>();
            }
            return anim;
        }
    }

    private CharacterController _controller;
    private float J_PlayerPosY;

    public InputField J_SeaField;

    public CharacterController _Controller
    {
        get
        {
            if (_controller == null)
            {
                _controller = this.GetComponent<CharacterController>();
            }
            return _controller;
        }
    }
    void Start()
    {
        J_SeaField.onValueChanged.AddListener(Changed_Value);
        J_SeaField.onEndEdit.AddListener(End_Value);


        if (J_SBInfoPanel != null && J_InfoPanel != null)
        {
            J_SBInfoPanel.transform.Find("SeaveB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(0); });
            J_SBInfoPanel.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(0); });
            J_SBInfoPanel.transform.Find("NameB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_SBInfoPanel.transform.Find("IPB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_SBInfoPanel.transform.Find("NuB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_SBInfoPanel.transform.Find("CJB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_SBInfoPanel.transform.Find("CPB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_SBInfoPanel.transform.Find("BuyB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_SBInfoPanel.transform.Find("SJB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_SBInfoPanel.transform.Find("WorkB").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });

            J_InfoPanel.transform.Find("SeaveBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(0); });
            J_InfoPanel.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(0); });
            J_InfoPanel.transform.Find("NameBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_InfoPanel.transform.Find("PlantBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_InfoPanel.transform.Find("NumBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_InfoPanel.transform.Find("PropertyBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_InfoPanel.transform.Find("BuyTimeBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
            J_InfoPanel.transform.Find("TimeBtn").GetComponent<Button>().onClick.AddListener(delegate () { this.OnClickBtn(1); });
        }
        J_Parent = transform.parent.gameObject;
        m_transform = this.transform; //尽量不要再update里获取this.transform，而是这样保存起来，这样能节约性能
        m_camera = MyCamera.Instance.transform;
        J_initPos = transform.position;

        if (AreaTipCon.instance.J_Posx != 0 || AreaTipCon.instance.J_Posy != 0)
        {
            if (J_Jifang != null)
            {
                //m_camera.GetComponent<FollowPlayer>().enabled = false;
                transform.SetParent(J_Jifang.transform);
                transform.localPosition = new Vector3(-AreaTipCon.instance.J_Posx - 0.3f, transform.localPosition.y, AreaTipCon.instance.J_Posy + 2);
                transform.eulerAngles = new Vector3(0, AreaTipCon.instance.J_Angle + 180, 0);
                print(AreaTipCon.instance.J_Posx + "-----------" + AreaTipCon.instance.J_Posy);
                transform.SetParent(J_Parent.transform);
                AreaTipCon.instance.J_Posx = 0;
                AreaTipCon.instance.J_Posy = 0;
                AreaTipCon.instance.J_cabinetid = "";
                //m_camera.GetComponent<FollowPlayer>().enabled = true;
                J_IsMove = true;
            }
        }
        Invoke("MovePlayer", 0.8f);
        Invoke("PlayerPosInfo", 2);
    }

    private void End_Value(string arg0)
    {
        J_ISMove = true;
    }

    private void Changed_Value(string arg0)
    {
        J_ISMove = false;
    }

    void PlayerPosInfo()
    {
        J_PlayerPosY = this.gameObject.transform.position.y;
        J_IsStart = true;
    }

    void MovePlayer()
    {
        if (!J_IsMove)
        {
            print(AreaTipCon.instance.J_Posx + "-----77------" + AreaTipCon.instance.J_Posy);
            if (AreaTipCon.instance.J_Posx != 0 || AreaTipCon.instance.J_Posy != 0)
            {
                if (J_Jifang != null)
                {
                    //m_camera.GetComponent<FollowPlayer>().enabled = false;
                    transform.SetParent(J_Jifang.transform);
                    transform.localPosition = new Vector3(-AreaTipCon.instance.J_Posx - 0.3f, transform.localPosition.y, AreaTipCon.instance.J_Posy + 1.5f);
                    transform.eulerAngles = new Vector3(0, AreaTipCon.instance.J_Angle + 180, 0);
                    print(AreaTipCon.instance.J_Posx + "-----84------" + AreaTipCon.instance.J_Posy);

                    transform.SetParent(J_Parent.transform);
                    AreaTipCon.instance.J_Posx = 0;
                    AreaTipCon.instance.J_Posy = 0;
                    AreaTipCon.instance.J_cabinetid = "";
                    //m_camera.GetComponent<FollowPlayer>().enabled = true;
                }
            }
        }
    }

    void Update()
    {
        if (J_IsStart)
        {
            this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, J_PlayerPosY, this.gameObject.transform.position.z);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //m_camera.gameObject.SetActive(false);
            //m_camera.position = J_cameraPos.position;
            //m_camera.gameObject.SetActive(true);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            J_time += Time.deltaTime;
            if (J_time >= 1.5f)
            {
                //m_camera.gameObject.SetActive(false);
                transform.position = J_initPos;
                //m_camera.position = J_cameraPos.position;
                //m_camera.gameObject.SetActive(true);
                J_time = 0;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            J_time = 0;
        }
    }

    void OnClickBtn(int index)
    {
        if (index == 0)
        {
            J_ISMove = true;
        }
        if (index == 1)
        {
            J_ISMove = false;
        }
    }


    void FixedUpdate()
    {
        //RotatePlayer();
        if (J_ISMove)
        {
            h = Input.GetAxis("Horizontal");
            v = Input.GetAxis("Vertical");
            if ((Input.GetKey(KeyCode.W)) || (Input.GetKey(KeyCode.S)) || (Input.GetKey(KeyCode.A)) ||
                (Input.GetKey(KeyCode.D)))
            {
                _Anim.SetFloat("Speed", 4);


            }
            else
                {
                    //newSpeed = 0;
                    _Anim.SetFloat("Speed", 0);
                }

            }
            else
            {
                _Anim.SetFloat("Speed", 0);
            }

            //if (!_Controller.isGrounded)
            //{
            //    //
            //    //模拟简单重力，每秒下降10米，当然你也可以写成抛物线
            //    _Controller.Move(new Vector3(0, -10f * Time.deltaTime, 0));
            //    print("--------------------");
            //}

            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (Mathf.Abs(v) > 0.1 || Mathf.Abs(h) > 0.1)
                {

                    newSpeed = 6;
                    _Anim.SetBool("run", true);
                }
                else
                {
                    newSpeed = 2;
                    _Anim.SetBool("run", false);
                }
            }
            else
            {
                newSpeed = 2;
                _Anim.SetBool("run", false);
            }
    }

    public float length = 10;
    public Transform cTrans;
    [ContextMenu("SetPlayerPos")]
    private void SetPlayerPos()
    {
        SetPlayerPos(cTrans);
    }
    public void SetPlayerPos(Transform cabinetTrans)
    {
        Debug.LogError("cabinetTrans : " + cabinetTrans.name, cabinetTrans.gameObject);
        _Controller.transform.position = cabinetTrans.position + cabinetTrans.forward * 1.3f;
        _Controller.transform.rotation = cabinetTrans.localRotation * Quaternion.Euler(new Vector3(0, 180, 0));
    }
}