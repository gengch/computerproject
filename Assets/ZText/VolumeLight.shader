﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/VolumeLight" {

	Properties{
		_MainTex("Base texture", 2D) = "white" { }
	///    _MainColor ("Color", color) = (1,1,1,1)
	_FadeOutDistNear("Near fadeout dist", float) = 3
		_FadeOutDistFar("Far fadeout dist", float) = 10000
		_MaxAlpha("MaxAlpha", range(0, 1)) = 1
		_MinAlpha("MinAlpha", range(0.001, 1)) = 0.01


		_DampX("DampX", range(0, 6)) = 0.3    // 体积光的衰减系数
		_DampY("DampY", range(0, 6)) = 0.3    // 
		_DampZ("DampZ", float) = 0.3            // 

	}


		SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Opaque" }

		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off Lighting Off ZWrite Off Fog{ Color(0,0,0,0) }

		LOD 100


		CGINCLUDE
#include "UnityCG.cginc"
		sampler2D _MainTex;
	//    fixed4 _MainColor;
	float _FadeOutDistNear;    // 淡出的最近距离，小于该值则透明度为最小，不再变化
	float _FadeOutDistFar;    // 淡出最大距离，大于该值则透明度为最大，不再变化
	float _MaxAlpha;        // 最大透明度
	float _MinAlpha;        // 最小透明度

	float _DampX;            // X轴衰减系数
	float _DampY;            // Y轴衰减系数
	float _DampZ;            // Z轴衰减系数

	struct v2f {
		float4    pos    : SV_POSITION;
		float2    uv        : TEXCOORD0;
		fixed4    color : TEXCOORD1;
	};


	float DampParam(float3 pos, float3 len, float minA, float maxA)
	{
		// 光线的基本衰减函数
		float att;
		att = 1 - pos.z*len.z;    // 以平方计衰减

		att *= (1 - abs(pos.x * len.x * len.x));
		att *= (1 - abs(pos.y * len.y * len.y));

		return lerp(minA, maxA, att);
	}

	float DampViewParam(float dist, float nFade, float fFade, float minA, float maxA)
	{
		// 由于观察点位置引起的衰减
		return (lerp(nFade, fFade, dist) - nFade) * (maxA - minA) / (fFade - nFade);
	}


	v2f vert(appdata_full v)
	{
		v2f o;

		// 模型的局部空间
		float3 mPos = v.vertex.xyz;

		// gl坐标中，摄像机坐标始终是（0，0，0）
		// 所以顶点世界坐标的长度就是与摄像机的距离
		float3 viewPos = mul(UNITY_MATRIX_MV,v.vertex).xyz;
		float dist = length(viewPos);

		float3 len = float3(_DampX, _DampY, _DampZ);
		float att = DampParam(mPos, len, _MinAlpha, _MaxAlpha);

		att *= DampViewParam(dist, _FadeOutDistNear, _FadeOutDistFar, _MinAlpha, _MaxAlpha);
		o.color.a = att;
		//    o.color.rgb    = _MainColor;

		o.uv = v.texcoord.xy;
		o.pos = UnityObjectToClipPos(v.vertex);

		return o;

	}
	ENDCG



		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
		fixed4 frag(v2f i) : COLOR
	{
		fixed4 finalColor;
	finalColor = tex2D(_MainTex, i.uv);
	finalColor.a = i.color.a;

	return finalColor;
	}
		ENDCG
	}
	}
}