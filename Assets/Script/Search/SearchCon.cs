﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SearchCon : MonoBehaviour
{
    private GameObject J_TText;//测试用，接口完善后去掉
    private GameObject J_ChangePanel;
    private Text J_ChangeText;

    private searchlogic J_JFSearch;//机房搜索脚本
    private SearchCabinet J_JHSearch;//机柜搜索脚本
    private SearchEquip J_SBSearch;//设备搜索脚本
    private SearchPort J_DKSearch;//端口搜索脚本

    private GameObject J_SeaBtn1;
    private GameObject J_SeaBtn2;
    private GameObject J_SeaBtn3;
    private GameObject J_SeaBtn4;

    void Start()
    {
        J_SeaBtn1 = transform.Find("SeaBtn_1").gameObject;
        J_SeaBtn2 = transform.Find("SeaBtn_2").gameObject;
        J_SeaBtn3 = transform.Find("SeaBtn_3").gameObject;
        J_SeaBtn4 = transform.Find("SeaBtn_4").gameObject;



        J_ChangePanel = transform.Find("ChangePanel").gameObject;
        J_TText = transform.Find("TText").gameObject;
        J_TText.SetActive(false);
        //J_JFSearch = transform.Find("SearchPanel/rect/grid").GetComponent<searchlogic>();
        J_JFSearch = GetComponent<searchlogic>();
        J_JHSearch = GetComponent<SearchCabinet>();
        J_SBSearch = GetComponent<SearchEquip>();
        J_DKSearch = GetComponent<SearchPort>();
        J_JFSearch.enabled = true;
        J_JHSearch.enabled = false;
        J_SBSearch.enabled = false;
        J_DKSearch.enabled = false;
        J_ChangeText = transform.Find("Text").GetComponent<Text>();
        Button[] J_Buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button tempBtn in J_Buttons)
        {
            Button J_Btn = tempBtn;
            J_Btn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_Btn);
            });
        }

        J_ChangePanel.SetActive(false);
        Invoke("StartInfo", 0.5f);
    }

    void StartInfo()
    {
        J_SeaBtn1.SetActive(true);
        J_SeaBtn2.SetActive(false);
        J_SeaBtn3.SetActive(false);
        J_SeaBtn4.SetActive(false);
    }

    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            case "ChangeBtn":
                if (J_ChangePanel.activeInHierarchy)
                {
                    J_ChangePanel.SetActive(false);
                }
                else
                {
                    J_ChangePanel.SetActive(true);
                }
                break;
            case "FBtn":
                J_ChangeText.text = btn.transform.Find("Text").GetComponent<Text>().text;
                J_JFSearch.enabled = true;
                J_JHSearch.enabled = false;
                J_SBSearch.enabled = false;
                J_DKSearch.enabled = false;

                J_SeaBtn1.SetActive(true);
                J_SeaBtn2.SetActive(false);
                J_SeaBtn3.SetActive(false);
                J_SeaBtn4.SetActive(false);
                J_ChangePanel.SetActive(false);

                break;
            case "GBtn":
                J_ChangeText.text = btn.transform.Find("Text").GetComponent<Text>().text;
                //测试用，接口完成后去掉
                //J_TText.SetActive(true);
                J_JFSearch.enabled = false;
                J_JHSearch.enabled = true;
                //接口完善后改为true，去掉协成
                J_SBSearch.enabled = false;
                J_DKSearch.enabled = false;

                J_SeaBtn1.SetActive(false);
                J_SeaBtn2.SetActive(true);
                J_SeaBtn3.SetActive(false);
                J_SeaBtn4.SetActive(false);

                J_ChangePanel.SetActive(false);
                //测试用，接口完成后去掉
                //StartCoroutine(WaitCloseTip());
                break;
            case "BBtn":
                J_ChangeText.text = btn.transform.Find("Text").GetComponent<Text>().text;
                //测试用，接口完成后去掉
                //J_TText.SetActive(true);
                J_JFSearch.enabled = false;
                J_JHSearch.enabled = false;
                //接口完善后改为true，去掉协成
                J_SBSearch.enabled = true;
                J_DKSearch.enabled = false;

                J_SeaBtn1.SetActive(false);
                J_SeaBtn2.SetActive(false);
                J_SeaBtn3.SetActive(true);
                J_SeaBtn4.SetActive(false);

                J_ChangePanel.SetActive(false);
                //测试用，接口完成后去掉
                //StartCoroutine(WaitCloseTip());
                break;

            case "PBtn":
                J_ChangeText.text = btn.transform.Find("Text").GetComponent<Text>().text;
                //测试用，接口完成后去掉
                //J_TText.SetActive(true);
                J_JFSearch.enabled = false;
                J_JHSearch.enabled = false;
                //接口完善后改为true，去掉协成
                J_SBSearch.enabled = false;
                J_DKSearch.enabled = true;

                J_SeaBtn1.SetActive(false);
                J_SeaBtn2.SetActive(false);
                J_SeaBtn3.SetActive(false);
                J_SeaBtn4.SetActive(true);

                J_ChangePanel.SetActive(false);
                //测试用，接口完成后去掉
                //StartCoroutine(WaitCloseTip());
                break;
        }
    }

    IEnumerator WaitCloseTip()
    {
        yield return new WaitForSeconds(2);
        J_TText.SetActive(false);
    }
}
