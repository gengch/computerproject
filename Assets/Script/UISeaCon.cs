﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISeaCon : MonoBehaviour
{
    private int J_index;
    public Button J_seabtn;
    //对场景中的所有UI进行遍历
    private RectTransform[] J_transform;
    // List 里存的是场景里所有的被查找物体的名称和位置   
    private List<RectTransform> allnameslist = new List<RectTransform>();
    //按钮上的原始图片
    private Sprite[] J_sprite = new Sprite[20];
    //要搜索的目标UI
    private GameObject[] J_seaUI = new GameObject[20];
    //是否进行切换图片
    private bool J_ischangesprite;
    //搜索框内的内容
    private InputField J_seafield;

    void Awake()
    {
        J_transform = GameObject.FindObjectsOfType<RectTransform>();
        //找到场景中所有的目标物体，然后添加到list里  
        allnameslist = new List<RectTransform>();

        foreach (RectTransform child in J_transform)
        {
            allnameslist.Add(child);
        }
    }

    void Start()
    {
        J_seabtn.onClick.AddListener(OnDesClickBtn);
        J_seafield = GetComponent<InputField>();
    }

    void OnDesClickBtn()
    {
        //若之前进行了搜索，则把之前的所有的按钮图片复原
        if (J_ischangesprite)
        {
            for (int i = 0; i < J_index; i++)
            {
                J_seaUI[i].GetComponent<Image>().sprite = J_sprite[i];
                if (i == J_index - 1)
                {
                    //初始化
                    J_index = 0;
                }
            }
            J_ischangesprite = false;
        }
        //遍历list集合，查看里面是否包含该名称的UI
        for (int i = 0; i < allnameslist.Count; i++)
        {
            Debug.Log("list 里有：" + allnameslist[i].name);
            Debug.Log(allnameslist.Count + "---allnameslist.Count----");

            if (J_seafield.text != "" && allnameslist[i].name.Contains(J_seafield.text))
            {
                Debug.Log(" 包含" + "。。。。该字符串是：" + allnameslist[i] + "--------UI变色了--------");
                J_seaUI[J_index] = allnameslist[i].gameObject;
                //对目标UI进行图片切换
                J_sprite[J_index] = J_seaUI[J_index].GetComponent<Image>().sprite;
                J_seaUI[J_index].GetComponent<Image>().sprite = J_seaUI[J_index].GetComponent<Button>().spriteState.highlightedSprite;
                J_ischangesprite = true;
                allnameslist[i].gameObject.transform.parent.gameObject.SetActive(true);
                J_index++;
            }
            else
            {
                Debug.Log("不包含");
            }
        }
        J_seafield.text = "";
    }
    void Update()
    {
        //对搜索目标按钮进行复原
        if (Input.GetMouseButtonDown(0) && J_ischangesprite)
        {
            for (int i = 0; i < J_index; i++)
            {
                J_seaUI[i].GetComponent<Image>().sprite = J_sprite[i];
                if (i == J_index - 1)
                {
                    J_index = 0;
                }
            }
            J_ischangesprite = false;
        }
    }
}
