﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using RTEditor;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.IO;
using System.Threading;

public class CreatModelCon : MonoBehaviour
{
    private string J_CUrl = "http://cams.zcwjvr.com:8080/";
    private bool J_isFreeView;
    private bool J_isCreat;
    private int J_vindex;
    private int J_hindex;
    private bool J_isstop;
    private bool J_isstart;
    private bool J_iscreat;//是否创建模型
    private bool J_isopen1;
    private bool J_isopen2;
    private GameObject J_rotatepanel;//旋转面板
    private GameObject J_modelpanel;
    private GameObject J_text;//提示面板
    //private GameObject J_pospanel;//实时生成机柜面板
    //private GameObject J_mpospanel;//实时生成设备面板
    private GameObject J_creatmodel;//需要创建的模型
    private GameObject J_creatmodel2;//需要创建的模型
    private GameObject J_newObj;//新生成的设备
    private GameObject J_player;//场景中的相机
    private GameObject J_cameraPos;//相机初始位置
    //private InputField J_typefield;//要创建的模型名称
    //private InputField J_xfield;//x轴坐标
    //private InputField J_yfield;//y轴坐标
    //private InputField J_zfield;//z轴坐标
    //private InputField J_numfield;//机柜个数
    //private InputField J_mtypefield;
    //private InputField J_jiguiname;
    //private InputField J_u;
    private InputField J_rotatefield;//需要旋转的度数
    private RaycastHit J_hit;
    private GameObject J_mouseModel;//选中的模型
    private GameObject SbPanel;//设备面板
    private GameObject DlPanel;//电缆面板
    private GameObject J_panel;
    private GameObject J_seaobj;
    private InputField J_seafield;
    private SceneManger J_scenemanger;
    private GameObject J_pos;
    private Vector3 J_startpos;
    private AreaTipCon J_areatip;
    private GameObject J_uitip;
    //private GameObject J_creatpanel;

    private Text J_NameText;
    private Text J_PlantText;
    private Text J_NumText;
    private Text J_PropertyText;
    private Text J_BuyTimeText;
    private Text J_TimeText;

    private InputField J_NameInput;
    private InputField J_PlantInput;
    private InputField J_NumInput;
    private InputField J_PropertInput;
    private InputField J_BuyTimeInput;
    private InputField J_TimeInput;
    private bool J_isEditor;

    public int J_floorindex;
    public GameObject J_renwu;
    public GameObject J_modelPanel;//控制模型移动、旋转的面板
    public EditorGizmoSystem J_editorsystem;
    private Camera J_camera;//发出射线的相机
    public GameObject J_jifang;//机房模型
    public Transform J_rubbish;//废弃物品放置处
    public bool isFirst;
    public GameObject J_rect;
    public GameObject J_scrollbar;
    public searchlogic J_searchlogic;
    public GameObject J_xian;

    private Text J_ProText;
    private Text J_AreaText;
    private Text J_FloorText;
    private Text J_RoomText;
    private string[] J_RoomID;//测试用
    private string[] J_CabinetID;//机柜ID
    private string[] J_CabinetName;//机柜名称
    private string[] J_Angle;//机柜旋转角度
    private string[] J_PosX;//机柜X坐标
    private string[] J_PosY;//机柜Y坐标
    private string J_jiguiID;//要删除的机柜的ID
    private string J_Cabinetid;//选中的机柜id
    [HideInInspector]
    public GameObject J_ClickObj;
    private Dictionary<string, string> J_Data;
    private GameObject J_FirstDoor;
    private GameObject J_LastDoor;

    private float J_LoadTime;
    private GameObject J_LoadPanel;
    private Slider J_LoadSlider;
    private Text J_LoadText;
    [HideInInspector]
    public bool J_IsNew;
    public Text J_Tip;
    public Camera J_FreeCamera;
    public GameObject J_Ding;
    private GameObject J_SBInfoPanel;
    private GameObject J_InfoPanel;

    private string[] J_cabinetname;//设备所属机柜名称
    private string[] J_cabinetpos;//设备所在位置
    private string[] J_equipname;//设备类型
    private string[] J_equiptypeid;//设备类型id
    private string[] J_equipid;
    private string J_EquipID;
    private GameObject J_GuidePanel;
    private string J_PortName;
    private GameObject J_SurePanel;
    private GameObject J_OffPanel;

    public GameObject J_wangxianBtn;
    public GameObject J_jiaxiangBtn;
    public GameObject J_fuwuqi01;
    public GameObject J_fuwuqi02;
    //public GameObject guangpei_12;
    public GameObject guangpei_24;

    private string[] J_equiptypename;
    private string[] J_Equiptypeid;
    private string[] J_modelname;
    private GameObject J_equip;

    private float J_ClickTime;

    private string[] manufacturer;//生产厂家
    private string[] manufactureserial;//序列号
    private string[] assetsserial;//资产编号
    private string[] buytime;//购买时间
    private string[] starttime;//上架时间
    private bool J_IsShow;
    [HideInInspector]
    public GameObject J_ClickModel;
    private GameObject J_PortInfoPanel;
    private string J_Port;
    private int J_Id;
    private Text J_PortNameText;
    private Text J_PortTypeText;
    private Text J_PortStatusText;
    private GameObject J_Des;

    int[] Array = new int[10];

    private string[] J_CabinetType;
    private string[] J_portname;
    private string[] J_portstatus;
    private string[] J_porttype;
    private string[] J_portid;

    private string J_LastName;
    private string J_LastPlant;
    private string J_LastNum;
    private string J_LastProperty;
    private string J_LastBuyTime;
    private string J_LastTime;
    private bool J_ISShow;
    [HideInInspector]
    public bool J_IsThree;
    private Text J_Text;

    private GameObject J_Cable;
    [HideInInspector]
    public bool J_IsUpdate;


    private Dictionary<string, string> X_Data;
    private Dictionary<string, string> X_Data1;
    private Dictionary<string, string> X_Data2;
    private string X_RoomType;//
    private string X_RoomSize;//上传的房间型号
    private string[] X_CabinetPos;//机柜位置
    private string[] X_CabinetSerial;//机柜可用位置序号
    private bool IsOver;//等待解析
    private string X_CurrCabinetSerial;//当前机柜位置号
    private string[] X_CurrSerialArr;//

    public static bool isReturn;//是否是从机房跳转回computer_1场景的，用于控制跳回场景的刷新按键功能。

    private Button Refresh;//
    private float New_X;//新建机柜的x
    private float New_Y;//新建机柜的y


    IEnumerator WaitType(string url, string node_1, string node_2, string node_3)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());
        J_equiptypename = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_equiptypename[i] = jar[i][node_1].ToString();
            //print(J_Str_1[i] + node_1 + "---node_1---");
        }

        if (node_2 != "")
        {
            J_Equiptypeid = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_Equiptypeid[i] = jar[i][node_2].ToString();
                print(J_Equiptypeid[i] + node_2 + "---node_2---" + "----url     " + url);
            }
        }

        if (node_3 != "")
        {
            J_modelname = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_modelname[i] = jar[i][node_3].ToString();
                //print(J_Str_3[i] + node_3 + "---node_3---");
            }
        }

        J_wangxianBtn.GetComponent<DragImage>().J_EquipTypeId = J_Equiptypeid[0];
        J_wangxianBtn.GetComponent<DragImage>().J_name = J_equiptypename[0];
        J_jiaxiangBtn.GetComponent<DragImage>().J_EquipTypeId = J_Equiptypeid[1];
        J_jiaxiangBtn.GetComponent<DragImage>().J_name = J_equiptypename[1];
        J_fuwuqi01.GetComponent<DragImage>().J_EquipTypeId = J_Equiptypeid[2];
        J_fuwuqi01.GetComponent<DragImage>().J_name = J_equiptypename[2];
        J_fuwuqi02.GetComponent<DragImage>().J_EquipTypeId = J_Equiptypeid[3];
        J_fuwuqi02.GetComponent<DragImage>().J_name = J_equiptypename[3];
        //guangpei_12.GetComponent<DragImage>().J_EquipTypeId = J_Equiptypeid[4];
        //guangpei_12.GetComponent<DragImage>().J_name = J_equiptypename[4];
        guangpei_24.GetComponent<DragImage>().J_EquipTypeId = J_Equiptypeid[4];
        guangpei_24.GetComponent<DragImage>().J_name = J_equiptypename[4];

        for (int i = 0; i < J_Equiptypeid.Length; i++)
        {
            PlayerPrefs.SetString(J_equiptypename[i], J_Equiptypeid[i]);
            PlayerPrefs.SetString(J_Equiptypeid[i], J_equiptypename[i]);
        }
    }

    private List<string> layerMaskNames = new List<string>()
    {
        "Default",
        "Model",
        "House",
        "Ding",
        "JG",
        "Xian",
        "Door",
        "RayCastOut"
    };

    LayerMask clickLayerMask;
    void Awake()
    {
        J_camera = MyCamera.Instance.GetComponent<Camera>();
        J_CUrl = GameConst.ServerUrl;//  File.ReadAllText("D://Url.txt");
        J_IsThree = false;
        clickLayerMask = DataMgr.Instance.GetLayerMask();

        //if (PlayerPrefs.HasKey("线缆位置X"))
        //{
        //    Debug.Log("------生成线缆模型了------");
        //    J_Cable = Resources.Load<GameObject>(PlayerPrefs.GetString("线缆模型名称"));
        //    GameObject LS = Instantiate(J_Cable, new Vector3(PlayerPrefs.GetFloat("线缆位置X"), PlayerPrefs.GetFloat("线缆位置Y"), PlayerPrefs.GetFloat("线缆位置Z")), Quaternion.identity);
        //    LS.name = "LS";
        //    LS.transform.eulerAngles = new Vector3(PlayerPrefs.GetFloat("旋转位置X"), PlayerPrefs.GetFloat("旋转位置Y"), PlayerPrefs.GetFloat("旋转位置Z"));
        //    PlayerPrefs.DeleteKey("线缆位置X");
        //    PlayerPrefs.DeleteKey("线缆位置Y");
        //    PlayerPrefs.DeleteKey("线缆位置Z");
        //    PlayerPrefs.DeleteKey("旋转位置X");
        //    PlayerPrefs.DeleteKey("旋转位置Y");
        //    PlayerPrefs.DeleteKey("旋转位置Z");
        //}
        //else
        //{
        //    Debug.Log("------线缆模型不存在------");
        //}

        J_PortInfoPanel = transform.Find("PortInfoPanel").gameObject;
        J_PortNameText = J_PortInfoPanel.transform.Find("NameText").GetComponent<Text>();
        J_PortTypeText = J_PortInfoPanel.transform.Find("TypeText").GetComponent<Text>();
        J_PortStatusText = J_PortInfoPanel.transform.Find("StatusText").GetComponent<Text>();

        J_PortInfoPanel.SetActive(false);
        string typeurl = J_CUrl + "/APIS3d/device/api/web/equipmenttype/get-equipmenttype";
        StartCoroutine(WaitType(typeurl, "EquipTypeName", "ID", "ModelName"));

        J_OffPanel = transform.Find("OffPanel").gameObject;
        J_SurePanel = transform.Find("SurePanel").gameObject;
        J_GuidePanel = transform.Find("GuidePanel").gameObject;
        J_GuidePanel.SetActive(false);

        J_InfoPanel = transform.Find("InfoPanel").gameObject;
        J_SBInfoPanel = transform.Find("SBInfoPanel").gameObject;
        J_SBInfoPanel.SetActive(false);
        J_FreeCamera.gameObject.SetActive(false);

        J_Data = new Dictionary<string, string>();
        J_Data.Add("id", AreaTipCon.instance.J_floorid);
        //J_Data.Add("userName", PlayerPrefs.GetString("账号"));
        J_Data.Add("UserID", LoginCon.X_UserID);
        J_Data.Add("RolesID", LoginCon.X_RolesID);
        //http://3dmis.zcwjvr.com/switch/CabinetsSelect?id=3&userName=admin
        string url = "/APIS3d/device/api/web/cabinets/get-cabinets";//生产厂家，序列号，资产编号，购买时间，开始时间
        JsonContent.J_JsonContent.ContentJson_CabInfo(url, J_Data, "CabinetName", "ID", "Angle", "PosX", "PosY", "Manufacturer", "ManufactureSerial", "AssetsSerial", "BuyTime", "StartTime", "CabinetType","Serial");
        Invoke("GetCabinetInfo", 1.5f);

        if (transform.Find("LoadPanel") != null)
        {
            J_LoadPanel = transform.Find("LoadPanel").gameObject;
        }
        else
        {
            J_LoadPanel = Instantiate(Resources.Load<GameObject>("LoadPanel")).gameObject;
            J_LoadPanel.transform.SetParent(this.gameObject.transform);
        }
        J_LoadSlider = J_LoadPanel.transform.Find("LoadSlider").GetComponent<Slider>();
        J_LoadText = J_LoadPanel.transform.Find("LoadText").GetComponent<Text>();
        J_LoadPanel.SetActive(true);
        J_Text = J_LoadPanel.transform.Find("Text").GetComponent<Text>();
        J_Text.text = "正在进入" + AreaTipCon.instance.J_floorstr;
        if (AreaTipCon.instance.J_EquipID != "")
        {
            string EquipUrl = J_CUrl + "/APIS3d/device/api/web/ports/get-ports?id=" + AreaTipCon.instance.J_EquipID;
            StartCoroutine(WaitForEquipPort(EquipUrl, "portstatus", "portType", "PortName"));
        }

        string J_CableInfo = J_CUrl + "/APIS3d/device/api/web/cables/get-cables";
        StartCoroutine(GetCableInfo(J_CableInfo, "CableType", "ID", ""));
    }

    IEnumerator GetCableInfo(string url, string node_1, string node_2, string node_3)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        AreaTipCon.instance.J_CableInfo = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            AreaTipCon.instance.J_CableInfo[i] = jar[i][node_1].ToString();
            //print(J_Str_1[i] + node_1 + "---node_1---");
        }

        if (node_2 != "")
        {
            AreaTipCon.instance.J_CableId = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                AreaTipCon.instance.J_CableId[i] = jar[i][node_2].ToString();
            }
        }

        //if (node_3 != "")
        //{
        //    J_modelname = new string[jar.Count];
        //    for (int i = 0; i < jar.Count; i++)
        //    {
        //        J_modelname[i] = jar[i][node_3].ToString();
        //        //print(J_Str_3[i] + node_3 + "---node_3---");
        //    }
        //}

        AreaTipCon.instance.J_IsStart = true;
    }

    void UpdateCabPos()
    {

    }
    void GetCabinetInfo()
    {
        J_CabinetName = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_CabinetID = JsonContent.J_JsonContent.ReturnJsonData_2();
        J_Angle = JsonContent.J_JsonContent.ReturnJsonData_3();
        J_PosX = JsonContent.J_JsonContent.ReturnJsonData_4();
        J_PosY = JsonContent.J_JsonContent.ReturnJsonData_5();
        manufacturer = JsonContent.J_JsonContent.ReturnJsonData_6();//生产厂家
        manufactureserial = JsonContent.J_JsonContent.ReturnJsonData_7();//序列号
        assetsserial = JsonContent.J_JsonContent.ReturnJsonData_8();//资产编号
        buytime = JsonContent.J_JsonContent.ReturnJsonData_9();//购买时间
        starttime = JsonContent.J_JsonContent.ReturnJsonData_10();//开始时间
        J_CabinetType = JsonContent.J_JsonContent.ReturnJsonData_11();//机柜类型
        X_CurrSerialArr = JsonContent.J_JsonContent.ReturnJsonData_12();//所有机柜的对应位置

        if (J_CabinetName[0] == "对不起，您没有该机房管理的权限，无法进入")
        {
            StartCoroutine(WaitChangeScene());
            J_Tip.text = "对不起，您没有该机房管理的权限，无法进入";
            J_Tip.gameObject.SetActive(true);
        }

        J_creatmodel = Resources.Load<GameObject>("jigui01");
        J_creatmodel2 = Resources.Load<GameObject>("jigui02");
        print(J_CabinetName.Length + "-----J_CabinetName.Length-----wowowowowowowowwowo");
        if (J_CabinetID != null)
        {
            for (int i = 0; i < J_CabinetID.Length; i++)
            {
                //PlayerPrefs.SetString(J_CabinetName[i], J_CabinetName[i]);
                //PlayerPrefs.SetString(J_CabinetName[i] + "生产厂家", manufacturer[i]);
                //PlayerPrefs.SetString(J_CabinetName[i] + "序列号", manufactureserial[i]);
                //PlayerPrefs.SetString(J_CabinetName[i] + "编号", assetsserial[i]);
                //PlayerPrefs.SetString(J_CabinetName[i] + "购买", buytime[i]);
                //PlayerPrefs.SetString(J_CabinetName[i] + "上架", starttime[i]);
                //J_T.text = J_T.text + "/////////" + J_CabinetName[i] + "," + manufacturer[i] + "," + manufactureserial[i] + "," + assetsserial[i] + "," + buytime[i] + "," + starttime[i];
                print(J_CabinetName[i]);
                if (J_CabinetName[i] != "" )
                {
                    if (J_CabinetType[i] == "1200")
                    {
                        J_newObj = Instantiate(J_creatmodel);
                        J_newObj.name = J_CabinetName[i];
                    }
                    else
                    {
                        J_newObj = Instantiate(J_creatmodel2);
                        J_newObj.name = J_CabinetName[i];
                    }
                }

                if (i == 0)
                {
                    J_startpos = new Vector3(-1, 0, 1);
                    print(J_startpos);
                }
                print(AreaTipCon.instance.J_cabinetid + "----AreaTipCon.instance.J_cabinetid----");
                if (AreaTipCon.instance.J_cabinetid != "")
                {
                    print("---坐标要改变了---");
                    if (J_CabinetID[i] == AreaTipCon.instance.J_cabinetid)
                    {
                        print(J_PosX[i] + "--------133----------" + J_PosY[i]);
                        AreaTipCon.instance.J_Posx = float.Parse(J_PosX[i]);
                        AreaTipCon.instance.J_Posy = float.Parse(J_PosY[i]);
                        AreaTipCon.instance.J_Angle = float.Parse(J_Angle[i]);
                    }
                }

                if (J_newObj != null)
                {
                    //J_scenemanger.AddObj(J_newObj);
                    J_newObj.transform.SetParent(J_jifang.transform);
                }
                //if (J_PosX == null)
                //{
                //    SceneManager.LoadScene("jifang01");
                //}
                if (J_PosX.Length > i)
                {
                    if (J_PosX[i] != "" && J_PosY[i] != "")
                    {
                        print(J_PosX[i] + "------147------" + J_PosY[i]);
                        J_newObj.transform.localPosition = new Vector3(-float.Parse(J_PosX[i]), 0, float.Parse(J_PosY[i]));
                    }
                }
                if (J_Angle[i] != "")
                {
                    print(J_Angle[i]);
                    J_newObj.transform.eulerAngles = new Vector3(0, float.Parse(J_Angle[i]), 0);
                }
            }
        }

        string url = "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
        Debug.Log(url + "======---5464----======");
        JsonContent.J_JsonContent.ContentJson_1(url, "CabinetName", "CabinetPos", "EquipName", "EquipTypeID", "ID", "");
        print(AreaTipCon.instance.J_Posx + "-------------255----------" + AreaTipCon.instance.J_Posx);
        Invoke("WaitGetEquip", 1f);
    }

    public void UpdateData()
    {
        string url = "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
        JsonContent.J_JsonContent.ContentJson_1(url, "CabinetName", "CabinetPos", "EquipName", "EquipTypeID", "ID", "");
        print(AreaTipCon.instance.J_Posx + "-------------255----------" + AreaTipCon.instance.J_Posx);
        Invoke("WaitUpdateDate", 0.5f);
    }

    
   

    void WaitUpdateDate()
    {
        J_cabinetname = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_cabinetpos = JsonContent.J_JsonContent.ReturnJsonData_2();
        J_equipname = JsonContent.J_JsonContent.ReturnJsonData_3();
        J_equiptypeid = JsonContent.J_JsonContent.ReturnJsonData_4();
        J_equipid = JsonContent.J_JsonContent.ReturnJsonData_5();
        print("-----强刷数据了-----");
    }

    void WaitGetEquip()
    {
        J_cabinetname = JsonContent.J_JsonContent.ReturnJsonData_1();//设备所属机柜名称
        J_cabinetpos = JsonContent.J_JsonContent.ReturnJsonData_2();//设备所在位置
        J_equipname = JsonContent.J_JsonContent.ReturnJsonData_3();//设备名称
        J_equiptypeid = JsonContent.J_JsonContent.ReturnJsonData_4();//设备类型id
        J_equipid = JsonContent.J_JsonContent.ReturnJsonData_5();//设备id

        GameObject J_Db;
        GameObject J_Eq = null;

        for (int i = 0; i < J_cabinetname.Length; i++)
        {
            Debug.Log("机柜数组长度"+J_cabinetname.Length+"设备类型数组长度"+J_equiptypeid.Length);
            Debug.Log(J_cabinetname[i] + "  ---381---   " + J_cabinetpos[i]);
            Debug.Log(J_cabinetname[i] + "  /////   " + J_equiptypeid[i] + "  /////   " + J_Equiptypeid[0] + "  /////   " + J_Equiptypeid[1] + "  /////   " + J_Equiptypeid[2] + "  /////   " + J_Equiptypeid[3] + "  /////   " + J_equipname[i] + "   3333");
        }

        //int index;
        for (int i = 0; i < J_cabinetname.Length; i++)
        {
            //index = i + 1;
            if (J_cabinetname[i] != "")
            {
                print(J_cabinetname[i] + "/dangban/dangban" + J_cabinetpos[i]);
                J_Db = J_jifang.transform.Find(J_cabinetname[i] + "/dangban/dangban" + J_cabinetpos[i]).gameObject;
                J_Db.SetActive(true);
                Debug.Log(J_equiptypeid[i] + "  /////   " + J_Equiptypeid[0] + "  /////   " + J_Equiptypeid[1] + "  /////   " + J_Equiptypeid[2] + "  /////   " + J_Equiptypeid[3] + "  /////   " + J_equipname[i]);
                if (J_equiptypeid[i] == J_Equiptypeid[0])
                {
                    print(J_modelname[0] + "----J_modelname[0]----");
                    J_Eq = Instantiate(Resources.Load<GameObject>(J_modelname[0]));
                }
                if (J_equiptypeid[i] == J_Equiptypeid[1])
                {
                    J_Eq = Instantiate(Resources.Load<GameObject>(J_modelname[1]));
                }
                if (J_equiptypeid[i] == J_Equiptypeid[2])
                {
                    J_Eq = Instantiate(Resources.Load<GameObject>(J_modelname[2]));
                }
                if (J_equiptypeid[i] == J_Equiptypeid[3])
                {
                    J_Eq = Instantiate(Resources.Load<GameObject>(J_modelname[3]));
                }
                if (J_equiptypeid[i] == J_Equiptypeid[4])
                {
                    Debug.Log(J_modelname[4]);
                    J_Eq = Instantiate(Resources.Load<GameObject>(J_modelname[4]));
                }
                //此处给每个新生成的设备添加CableCon脚本，用来检测已连接的线缆，进行线缆生成
                J_Eq.AddComponent<CableCon>().J_EquipID = J_equipid[i];
                J_Eq.transform.position = J_Db.transform.position;
                J_Eq.transform.rotation = J_Db.transform.rotation;
                if (J_equiptypeid[i] == J_Equiptypeid[1])
                {
                    J_Eq.transform.eulerAngles = J_Eq.transform.eulerAngles ;
                }
                else
                {
                    J_Eq.transform.eulerAngles = J_Eq.transform.eulerAngles + new Vector3(0, 180, 0);
                }
                J_Eq.transform.SetParent(J_Db.transform.parent.parent);
                J_Eq.name = J_equipname[i];
            }

        }

        if (AreaTipCon.instance.J_IsChose)
        {
            AreaTipCon.instance.J_IsChose = false;
            print(AreaTipCon.instance.J_CabinetName + "/" + AreaTipCon.instance.J_EquipName);
            J_equip = J_jifang.transform.Find(AreaTipCon.instance.J_CabinetName + "/" + AreaTipCon.instance.J_EquipName).gameObject;

            //J_camera.GetComponent<FollowPlayer>().enabled = false;
            J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
            J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
            J_camera.GetComponent<FollowPlayer>().Target = J_equip;
            //J_Camera.transform.position = J_hit.collider.gameObject.transform.parent.position - J_hit.collider.gameObject.transform.parent.forward * 0.5f;
            J_camera.transform.position = J_equip.transform.position + J_equip.transform.parent.forward * 0.5f;
            J_camera.GetComponent<FollowPlayer>().enabled = true;
            J_IsThree = true;
        }

        if (PlayerPrefs.HasKey("线缆位置X"))
        {
            Debug.Log("------生成线缆模型了------");
            J_Cable = Resources.Load<GameObject>(PlayerPrefs.GetString("线缆模型名称"));
            GameObject J_Port = J_jifang.transform.Find(PlayerPrefs.GetString("端口")).gameObject;
            //GameObject LS = Instantiate(J_Cable, new Vector3(PlayerPrefs.GetFloat("线缆位置X"), PlayerPrefs.GetFloat("线缆位置Y"), PlayerPrefs.GetFloat("线缆位置Z")), Quaternion.identity);
            GameObject LS = Instantiate(J_Cable, J_Port.transform.position, Quaternion.identity);
            LS.name = "LS";
            LS.transform.eulerAngles = new Vector3(PlayerPrefs.GetFloat("旋转位置X"), PlayerPrefs.GetFloat("旋转位置Y"), PlayerPrefs.GetFloat("旋转位置Z"));
            PlayerPrefs.DeleteKey("线缆位置X");
            PlayerPrefs.DeleteKey("线缆位置Y");
            PlayerPrefs.DeleteKey("线缆位置Z");
            PlayerPrefs.DeleteKey("旋转位置X");
            PlayerPrefs.DeleteKey("旋转位置Y");
            PlayerPrefs.DeleteKey("旋转位置Z");
        }
    }

    void WaitGetCable()
    {

    }

    void Start()
    {

        isReturn = false;
        Refresh = transform.Find("Refresh").GetComponent<Button>();
        J_ProText = transform.Find("Panel/ProBtn/Text").GetComponent<Text>();
        J_AreaText = transform.Find("Panel/AreaBtn/Text").GetComponent<Text>();
        J_FloorText = transform.Find("Panel/FloorBtn/Text").GetComponent<Text>();
        J_RoomText = transform.Find("Panel/RoomBtn/Text").GetComponent<Text>();
        J_RoomText.text = AreaTipCon.instance.J_floorstr;
        J_ProText.text = AreaTipCon.instance.J_prostr;
        J_AreaText.text = AreaTipCon.instance.J_areastr;
        J_FloorText.text = AreaTipCon.instance.J_tierstr;

        J_NameText = transform.Find("InfoPanel/NameText").GetComponent<Text>();
        J_PlantText = transform.Find("InfoPanel/PlantText").GetComponent<Text>();
        J_NumText = transform.Find("InfoPanel/NumText").GetComponent<Text>();
        J_PropertyText = transform.Find("InfoPanel/PropertyText").GetComponent<Text>();
        J_BuyTimeText = transform.Find("InfoPanel/BuyTimeText").GetComponent<Text>();
        J_TimeText = transform.Find("InfoPanel/TimeText").GetComponent<Text>();

        J_NameInput = transform.Find("InfoPanel/NameInput").GetComponent<InputField>();
        J_PlantInput = transform.Find("InfoPanel/PlantInput").GetComponent<InputField>();
        J_NumInput = transform.Find("InfoPanel/NumInput").GetComponent<InputField>();
        J_PropertInput = transform.Find("InfoPanel/PropertyInput").GetComponent<InputField>();
        J_BuyTimeInput = transform.Find("InfoPanel/BuyTimeInput").GetComponent<InputField>();
        J_TimeInput = transform.Find("InfoPanel/TimeInput").GetComponent<InputField>();

        J_NameInput.text = J_NameText.text;
        J_PlantInput.text = J_PlantText.text;
        J_NumInput.text = J_NumText.text;
        J_PropertInput.text = J_PropertyText.text;
        J_BuyTimeInput.text = J_BuyTimeText.text;
        J_TimeInput.text = J_TimeText.text;

        J_NameInput.gameObject.SetActive(false);
        J_PlantInput.gameObject.SetActive(false);
        J_NumInput.gameObject.SetActive(false);
        J_PropertInput.gameObject.SetActive(false);
        J_BuyTimeInput.gameObject.SetActive(false);
        J_TimeInput.gameObject.SetActive(false);
        J_uitip = Resources.Load<GameObject>("UITip");
        J_areatip = GameObject.Find("AreaTip").GetComponent<AreaTipCon>();
        J_pos = J_jifang.transform.Find("Pos").gameObject;
        J_scenemanger = GameObject.Find("SceneManger").GetComponent<SceneManger>();
        //J_creatpanel = transform.Find("SaidPanel 1/CreatBtn/CreatPanel").gameObject;
        J_modelpanel = transform.Find("ModelPanel").gameObject;
        if (!isFirst)
        {
            J_rotatepanel = transform.Find("EditorPanel/RotateBtn/RotatePanel").gameObject;
        }
        else
        {
            J_rotatepanel = transform.Find("EditorPanel/RotateBtn/Panel").gameObject;
        }
        J_seafield = transform.Find("SaidPanel/SeaField").GetComponent<InputField>();
        J_panel = transform.Find("InfoPanel").gameObject;
        J_rotatepanel.SetActive(true);

        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
        J_OffPanel.SetActive(false);
        J_rotatepanel.SetActive(false);
        J_rotatefield = transform.Find("EditorPanel/RotateBtn/RotatePanel/RotateField").GetComponent<InputField>();

        J_SurePanel.SetActive(false);
        SbPanel = transform.Find("ModelPanel/X_SbPanel").gameObject;
        DlPanel = transform.Find("ModelPanel/X_DlPanel").gameObject;
        SbPanel.SetActive(false);
        DlPanel.SetActive(false);
        J_text = transform.Find("Tip").gameObject;
        J_player = MyCamera.Instance.gameObject;// GameObject.Find("(Singleton)RTEditor.RuntimeEditorApplication/(Singleton)RTEditor.EditorCamera");
        J_cameraPos = GameObject.Find("renwu/Player/Position");
        //J_pospanel = transform.Find("PosPanel").gameObject;
        //J_mpospanel = transform.Find("mPosPanel").gameObject;
        //J_typefield = transform.Find("PosPanel/TypeField").GetComponent<InputField>();
        //J_xfield = transform.Find("PosPanel/XField").GetComponent<InputField>();
        //J_yfield = transform.Find("PosPanel/YField").GetComponent<InputField>();
        //J_zfield = transform.Find("PosPanel/ZField").GetComponent<InputField>();
        //J_numfield = transform.Find("PosPanel/NumField").GetComponent<InputField>();
        //J_mtypefield = transform.Find("mPosPanel/TypeField").GetComponent<InputField>();
        //J_jiguiname = transform.Find("mPosPanel/XField").GetComponent<InputField>();
        //J_u = transform.Find("mPosPanel/YField").GetComponent<InputField>();
        //J_pospanel.SetActive(false);
        J_text.SetActive(false);
        J_modelPanel.SetActive(false);
        J_rotatepanel.SetActive(false);
        J_panel.SetActive(false);
        //J_creatpanel.SetActive(false);
        J_rect.SetActive(false);
        J_scrollbar.SetActive(false);
        //J_mpospanel.SetActive(false);
        J_modelpanel.SetActive(false);
        //J_player.GetComponent<CameraMoveCon>().enabled = false;


        X_Data1 = new Dictionary<string, string>();
        X_Data1.Add("RoomID", AreaTipCon.instance.J_floorid);

        JsonContent.J_JsonContent.ContentJson_5("/APIS/device/api/web/cabinets/room-position", X_Data1, "Serial");
        X_CabinetSerial = JsonContent.J_JsonContent.ReturnJsonData_5_1();

        X_Data = new Dictionary<string, string>();
        X_Data.Add("id", AreaTipCon.instance.J_floorid);
        JsonContent.J_JsonContent.ContentJson_4_1("/APIS/device/api/web/switchrooms/get-switchrooms-byid", X_Data, "RoomType");
        X_RoomType = JsonContent.J_JsonContent.ReturnJsonData_4_1();

    }
    
    private Transform J_P;
    private Vector3 a = new Vector3(0, 0, 0);
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            GameObject obj = Instantiate(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            obj.transform.position = new Vector3(-a.x, 0, a.y);

        }

        if (J_isFreeView)
        {
            return;
        }

        Ray J_ray = J_camera.ScreenPointToRay(Input.mousePosition);
        //if (Physics.Raycast(J_ray, out J_hit))
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        if (EventSystem.current.IsPointerOverGameObject())
        //        {
        //            print("鼠标点到UI上了");
        //            return;
        //        }

        //        if (J_hit.collider.gameObject.tag == "D")
        //        {
        //            //J_E.SetActive(false);
        //            J_T.SetActive(false);

        //        }
        //        else
        //        {
        //            J_T.SetActive(true);
        //        }
        //    }
        //    if (Input.GetMouseButtonUp(0))
        //    {
        //        if (J_hit.collider.gameObject.tag == "D")
        //        {
        //            //J_E.SetActive(false);
        //            J_T.SetActive(false);

        //        }
        //        else
        //        {
        //            J_T.SetActive(true);
        //        }
        //    }
        //}

        J_ClickTime += Time.deltaTime;

        J_LoadTime += Time.deltaTime;
        if (J_LoadTime <= 1.25f)
        {
            J_LoadPanel.SetActive(true);
            J_LoadSlider.value = 0.8f * J_LoadTime;
            string J_LoadNum = (80 * J_LoadTime).ToString();
            if (J_LoadNum.Length > 4)
            {
                J_LoadText.text = J_LoadNum.Substring(0, 4) + "%";
            }
            else
            {
                J_LoadText.text = J_LoadNum + "%";
            }
        }
        else if (J_LoadTime > 1.25f)
        {
            J_LoadPanel.SetActive(false);
        }

        if (J_iscreat)
        {
            J_iscreat = false;
        }

        if (J_ISShow)
        {
            J_InfoPanel.SetActive(false);
        }
        if (Physics.Raycast(J_ray, out J_hit, 1000f, DataMgr.Instance.SelectedLayerMask.value))
            //if (Physics.Raycast(J_ray, out J_hit))
        {
            //if (EventSystem.current.IsPointerOverGameObject())
            //{
            //    print("鼠标点到UI上了");
            //    return;
            //}
            if (Input.GetMouseButtonDown(0) && J_IsNew)
            {
                GameObject clickedBtn = EventSystem.current.currentSelectedGameObject;

                if (clickedBtn != null && (clickedBtn.name == "InfoPanel" || clickedBtn.transform.parent.name == "InfoPanel"))
                {
                    print("这个是正确的");
                }
                else
                {
                    print("点错东西了");
                    J_Tip.text = "请先保存或关闭信息面板";
                    J_Tip.gameObject.SetActive(true);
                    StartCoroutine(WaitCloseTipPanel());
                }
                return;
            }

            if (J_IsNew || J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_IsNew)
            {
                return;
            }

            if (J_hit.collider.gameObject.tag == "Port" && J_hit.collider.gameObject.name != "Area")
            {
                J_Port = J_hit.collider.gameObject.name;
                if (J_portname != null)
                {
                    //print(J_portstatus[int.Parse(J_hit.collider.gameObject.name) - 1] + "----------" + J_porttype[int.Parse(J_hit.collider.gameObject.name) - 1]);
                    J_PortNameText.text = J_hit.transform.parent.parent.name + "-" + J_hit.collider.gameObject.name;
                    J_PortStatusText.text = J_portstatus[int.Parse(J_hit.collider.gameObject.name) - 1];
                    J_PortTypeText.text = J_porttype[int.Parse(J_hit.collider.gameObject.name) - 1];
                    J_PortInfoPanel.SetActive(true);
                }
                else
                {
                    print("数组目前为空");
                    J_PortInfoPanel.SetActive(false);
                }
            }
            else
            {
                J_PortInfoPanel.SetActive(false);
                //print("没有选中端口====" + J_hit.collider.gameObject.tag);
            }
            if (J_hit.collider.gameObject.tag == "jigui" || J_hit.collider.gameObject.name == "jiguimen")
            {
                if (Input.GetMouseButtonUp(0))
                {
                    if (EventSystem.current.IsPointerOverGameObject())
                    {
                        print("鼠标点到UI上了");
                        return;
                    }

                    if (J_hit.collider.gameObject.name == "jiguimen")
                    {
                        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_Cabinet = J_hit.collider.transform.parent.gameObject;
                    }
                    else
                    {
                        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_Cabinet = J_hit.collider.gameObject;
                    }
                }
            }
            if ((J_hit.collider.gameObject.tag == "Model" || J_hit.collider.gameObject.tag == "jigui" || J_hit.collider.gameObject.name == "jiguimen" || J_hit.collider.gameObject.tag == "Port") && !J_IsNew)
            {
                if (Input.GetMouseButtonDown(0) && J_isEditor)
                {
                    if (EventSystem.current.IsPointerOverGameObject())
                    {
                        print("鼠标点到UI上了");
                        return;
                    }

                    J_mouseModel = J_hit.collider.transform.gameObject;
                    if (J_mouseModel.tag == "jigui")
                    {
                        J_OffPanel.SetActive(false);
                        for (int i = 0; i < J_CabinetName.Length; i++)
                        {
                            if (J_CabinetName[i] == J_mouseModel.name)
                            {
                                J_jiguiID = J_CabinetID[i];
                                X_CurrCabinetSerial = X_CurrSerialArr[i];
                            }
                        }
                    }
                    if (J_mouseModel.name == "jiguimen")
                    {
                        J_OffPanel.SetActive(false);
                        for (int i = 0; i < J_CabinetName.Length; i++)
                        {
                            if (J_CabinetName[i] == J_mouseModel.transform.parent.name)
                            {
                                J_jiguiID = J_CabinetID[i];
                                X_CurrCabinetSerial = X_CurrSerialArr[i];
                            }
                        }
                    }
                    print(J_mouseModel.name + "----J_mouseModel.name----");
                    //J_modelPanel.SetActive(true);
                }

                if (J_hit.collider.gameObject.name == "SB")
                {
                    J_OffPanel.SetActive(false);
                    J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_hit.collider.transform.parent.gameObject;
                }

                if (J_hit.collider.gameObject.tag == "Model")
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        J_OffPanel.SetActive(false);
                        if (EventSystem.current.IsPointerOverGameObject())
                        {
                            print("鼠标点到UI上了");
                            return;
                        }

                        J_InfoPanel.SetActive(false);
                        J_SBInfoPanel.SetActive(false);
                        if (J_hit.collider.gameObject.name == "SB")
                        {
                            J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_hit.collider.transform.parent.gameObject;
                        }
                        else
                        {
                            J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_hit.collider.gameObject;
                        }
                        J_SBInfoPanel.SetActive(true);
                    }
                }
                if (J_hit.collider.gameObject.tag == "Port")
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        if (EventSystem.current.IsPointerOverGameObject())
                        {
                            print("鼠标点到UI上了");
                            return;
                        }

                        J_InfoPanel.SetActive(false);
                        J_SBInfoPanel.SetActive(false);
                        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_hit.collider.transform.parent.parent.gameObject;
                        J_SBInfoPanel.SetActive(true);
                    }
                }
            }
            if (J_hit.collider.gameObject.name == "jiguimen" && !J_IsNew)
            {
                if (Input.GetMouseButtonUp(0) && J_isEditor)
                {
                    if (EventSystem.current.IsPointerOverGameObject())
                    {
                        print("鼠标点到UI上了");
                        return;
                    }

                    J_mouseModel = J_hit.collider.transform.parent.gameObject;
                    if (J_mouseModel.tag == "jigui")
                    {
                        for (int i = 0; i < J_CabinetName.Length; i++)
                        {
                            if (J_CabinetName[i] == J_mouseModel.name)
                            {
                                //J_EquipID = J_equipid[i];
                                J_Cabinetid = J_CabinetID[i];
                                X_CurrCabinetSerial = X_CurrSerialArr[i];
                            }
                        }
                    }
                    print(J_mouseModel.name + "----J_mouseModel.name----");
                    //J_modelPanel.SetActive(true);
                }
            }
            if (Input.GetMouseButtonDown(2))
            {
                Debug.LogErrorFormat("open door :{0}, layer :{1}", J_hit.collider.gameObject.name, J_hit.collider.gameObject.tag, J_hit.collider.gameObject);
                if (J_hit.collider.gameObject.name == "jiguimen")
                {
                    //if (J_FirstDoor == null)
                    //{
                    //    J_FirstDoor = J_hit.collider.transform.parent.gameObject;
                    //}

                    if (EventSystem.current.IsPointerOverGameObject())
                    {
                        print("鼠标点到UI上了");
                        return;
                    }
                    //J_hit.collider.gameObject.AddComponent<Rigidbody>().useGravity = false;

                    if (J_isopen1)
                    {
                        //J_hit.collider.transform.parent.GetComponent<BoxCollider>().enabled = true;
                        J_hit.collider.gameObject.GetComponent<Animator>().SetInteger("Start", 0);
                        J_isopen1 = false;
                        J_FirstDoor = null;
                    }
                    else
                    {
                        //J_hit.collider.transform.parent.GetComponent<BoxCollider>().enabled = false;
                        J_hit.collider.gameObject.GetComponent<Animator>().SetInteger("Start", 1);
                        J_isopen1 = true;
                    }

                    //if (J_hit.collider.transform.parent.gameObject == J_FirstDoor)
                    //{
                    //    if (J_isopen1)
                    //    {
                    //        J_hit.collider.gameObject.GetComponent<Animator>().SetInteger("Start", 0);
                    //        J_isopen1 = false;
                    //        J_FirstDoor = null;
                    //    }
                    //    else
                    //    {
                    //        J_hit.collider.gameObject.GetComponent<Animator>().SetInteger("Start", 1);
                    //        J_isopen1 = true;
                    //    }
                    //}
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    print("鼠标点到UI上了");
                    return;
                }
                print(J_hit.collider.gameObject.tag + "====847====" + J_hit.collider.gameObject.name);

                if (J_hit.collider.gameObject.tag == "jigui" || J_hit.collider.gameObject.name == "jiguimen")
                {
                   
                    J_OffPanel.SetActive(false);
                    if (EventSystem.current.IsPointerOverGameObject())
                    {
                        print("鼠标点到UI上了");
                        return;
                    }
                    DataMgr.Instance.PrintDataFormat("射线碰到的对象是=====" + J_hit.collider.gameObject.name);
                    if (J_hit.collider.gameObject.name == "jiguimen")
                    {
                        PlayerMoves.Instance.SetPlayerPos(J_hit.collider.transform.parent);
                        J_NameText.text = J_hit.collider.transform.parent.gameObject.name;
                        J_NameInput.text = J_hit.collider.transform.parent.gameObject.name;
                        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_Cabinet = J_hit.collider.transform.parent.gameObject;
                        J_ClickObj = J_hit.collider.transform.parent.gameObject;

                        for (int i = 0; i < J_CabinetName.Length; i++)
                        {
                            if (J_CabinetName[i] == J_hit.collider.transform.parent.gameObject.name)
                            {
                                J_Cabinetid = J_CabinetID[i];
                                X_CurrCabinetSerial = X_CurrSerialArr[i];
                            }
                        }
                    }
                    else
                    {
                        PlayerMoves.Instance.SetPlayerPos(J_hit.collider.transform);
                        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_Cabinet = J_hit.collider.gameObject;
                        J_NameText.text = J_hit.collider.gameObject.name;
                        J_NameInput.text = J_hit.collider.gameObject.name;

                        J_ClickObj = J_hit.collider.gameObject;

                        for (int i = 0; i < J_CabinetName.Length; i++)
                        {
                            if (J_CabinetName[i] == J_hit.collider.gameObject.name)
                            {
                                J_Cabinetid = J_CabinetID[i];
                                X_CurrCabinetSerial = X_CurrSerialArr[i];
                            }
                        }
                    }

                    for (int i = 0; i < J_CabinetName.Length; i++)
                    {
                        print(J_CabinetName[i] + "==794==" + J_NameText.text);
                        if (J_CabinetName[i] == J_NameText.text)
                        {
                            J_PlantText.text = manufacturer[i];//生产厂家
                            J_NumText.text = manufactureserial[i];//序列号
                            J_PropertyText.text = assetsserial[i];//资产编号
                            J_BuyTimeText.text = buytime[i];//购买时间
                            J_TimeText.text = starttime[i];//开始时间


                            J_PlantInput.text = J_PlantText.text;
                            J_NumInput.text = J_NumText.text;
                            J_PropertInput.text = J_PropertyText.text;
                            J_BuyTimeInput.text = J_BuyTimeText.text;
                            J_TimeInput.text = J_TimeText.text;
                            print(J_PlantText.text + "--803--" + manufacturer[i]);
                            print(J_NumText.text + "--804--" + manufactureserial[i]);
                            print(J_PropertyText.text + "--805--" + assetsserial[i]);
                            print(J_BuyTimeText.text + "--806--" + buytime[i]);
                            print(J_TimeText.text + "--807--" + starttime[i]);
                        }
                    }

                    J_panel.SetActive(true);
                    J_SBInfoPanel.SetActive(false);
                }
                if (J_hit.collider.gameObject.tag == "Model" || J_hit.collider.gameObject.name == "SB")
                {
                    J_OffPanel.SetActive(false);
                    J_InfoPanel.SetActive(false);
                    J_SBInfoPanel.SetActive(false);
                    if (J_hit.collider.gameObject.name == "SB")
                    {
                        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_hit.collider.transform.parent.gameObject;
                    }
                    else
                    {
                        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = J_hit.collider.gameObject;
                    }
                    J_SBInfoPanel.SetActive(true);

                    J_ClickModel = J_hit.collider.gameObject;
                    J_ClickTime = 0;
                    if (J_isEditor)
                    {
                        J_mouseModel = J_hit.collider.transform.gameObject;
                        if (J_mouseModel.tag == "Model")
                        {
                            for (int i = 0; i < J_equipname.Length; i++)
                            {
                                print(J_equipname[i] + "=====832=====" + J_mouseModel.name);
                                if (J_equipname[i] == J_mouseModel.name)
                                {
                                    J_EquipID = J_equipid[i];
                                    print(J_EquipID + "=======836=======" + J_equipid[i]);
                                }
                            }
                        }
                        print(J_mouseModel.name + "----J_mouseModel.name----");
                        //J_modelPanel.SetActive(true);
                    }
                    if (J_hit.collider.gameObject.name == "SB")
                    {
                        J_ClickModel = J_hit.collider.transform.parent.gameObject;
                    }
                }
                if (J_hit.collider.gameObject.tag == "Xian")
                {
                    J_InfoPanel.SetActive(false);
                    J_SBInfoPanel.SetActive(false);

                    J_mouseModel = J_hit.collider.transform.gameObject;
                    if (J_mouseModel.tag == "Xian")
                    {
                        J_mouseModel.transform.parent.parent.parent.GetComponent<BoxCollider>().enabled = false;
                        J_Des = J_mouseModel.transform.parent.parent.parent.gameObject;
                        J_PortName = J_mouseModel.transform.parent.name;
                        J_OffPanel.SetActive(true);
                    }
                }
            }
            if (Input.GetMouseButtonDown(0) && J_ClickTime < 1)
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    print("鼠标点到UI上了");
                    return;
                }
                print(J_ClickTime + "----J_ClickTime----2222222");

                if (J_ClickModel == J_hit.collider.gameObject)
                {
                    //J_camera.GetComponent<FollowPlayer>().enabled = false;
                    J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                    J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                    J_camera.GetComponent<FollowPlayer>().Target = J_hit.collider.gameObject;
                    J_camera.transform.position = J_hit.collider.transform.transform.position + J_hit.collider.transform.parent.forward * 0.5f;
                    J_camera.GetComponent<FollowPlayer>().enabled = true;
                    J_IsThree = true;
                    //切换视野遍历场景中的设备信息
                    string url = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
                    StartCoroutine(WaitForEquipJsonData(url, "CabinetName", "CabinetPos", "EquipName", "EquipTypeID", "ID"));
                }
                if (J_hit.collider.gameObject.name == "SB")
                {
                    if (J_ClickModel == J_hit.collider.transform.parent.gameObject)
                    {
                        //J_camera.GetComponent<FollowPlayer>().enabled = false;
                        J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                        J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                        J_camera.GetComponent<FollowPlayer>().Target = J_hit.collider.gameObject;
                        J_camera.transform.position = J_ClickModel.transform.position + J_ClickModel.transform.parent.forward * 0.5f;
                        J_camera.GetComponent<FollowPlayer>().enabled = true;
                        J_IsThree = true;
                        //切换视野遍历场景中的设备信息
                        string url = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
                        StartCoroutine(WaitForEquipJsonData(url, "CabinetName", "CabinetPos", "EquipName", "EquipTypeID", "ID"));
                    }
                }
            }
        }

        #region  层搜索功能
        //LayerMask mask = 1 << LayerMask.NameToLayer("Model");
        //RaycastHit _hit;
        //if (Input.GetMouseButtonUp(0))
        //{
        //    if (EventSystem.current.IsPointerOverGameObject())
        //    {
        //        print("鼠标点到UI上了");
        //        return;
        //    }
        //    if (Physics.Raycast(J_ray, out _hit, 1000f, mask.value))
        //    {
        //        J_InfoPanel.SetActive(false);
        //        J_SBInfoPanel.SetActive(false);
        //        J_SBInfoPanel.GetComponent<SBInfoPanelCon>().J_RayEquip = _hit.collider.gameObject;
        //        J_SBInfoPanel.SetActive(true);

        //        J_ClickModel = _hit.collider.gameObject;
        //        J_ClickTime = 0;
        //        if (J_isEditor)
        //        {
        //            J_mouseModel = _hit.collider.transform.gameObject;
        //            if (J_mouseModel.tag == "Model")
        //            {
        //                for (int i = 0; i < J_equipname.Length; i++)
        //                {
        //                    if (J_equipname[i] == J_mouseModel.name)
        //                    {
        //                        J_EquipID = J_equipid[i];
        //                    }
        //                }
        //            }
        //            print(J_mouseModel.name + "----J_mouseModel.name----");
        //            //J_modelPanel.SetActive(true);
        //        }
        //    }
        //}

        //if (Input.GetMouseButtonDown(0) && J_ClickTime < 2)
        //{
        //    if (EventSystem.current.IsPointerOverGameObject())
        //    {
        //        print("鼠标点到UI上了");
        //        return;
        //    }
        //    print(J_ClickTime + "----J_ClickTime----2222222");
        //    if (Physics.Raycast(J_ray, out _hit, 1000f, mask.value))
        //    {
        //        if (J_ClickModel == _hit.collider.gameObject)
        //        {
        //            J_camera.GetComponent<FollowPlayer>().enabled = false;
        //            J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
        //            J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
        //            J_camera.GetComponent<FollowPlayer>().Target = _hit.collider.gameObject;
        //            J_camera.transform.position = _hit.collider.transform.transform.position + _hit.collider.transform.parent.forward * 0.5f;
        //            J_camera.GetComponent<FollowPlayer>().enabled = true;
        //            //切换视野遍历场景中的设备信息
        //            string url = J_CUrl+"switch/equipMentSelect?id=" + AreaTipCon.instance.J_floorid;
        //            StartCoroutine(WaitForEquipJsonData(url, "cabinetname", "cabinetpos", "equipname", "equiptypeid", "id"));
        //        }
        //    }
        //}

        //LayerMask Jmask = 1 << LayerMask.NameToLayer("Xian");
        //RaycastHit X_hit;
        //if (Input.GetMouseButtonUp(0))
        //{
        //    if (EventSystem.current.IsPointerOverGameObject())
        //    {
        //        print("鼠标点到UI上了");
        //        return;
        //    }
        //    if (Physics.Raycast(J_ray, out X_hit, 1000f, Jmask.value))
        //    {
        //        J_InfoPanel.SetActive(false);
        //        J_SBInfoPanel.SetActive(false);

        //        J_mouseModel = X_hit.collider.transform.gameObject;
        //        if (J_mouseModel.tag == "Xian")
        //        {
        //            J_mouseModel.transform.parent.parent.parent.GetComponent<BoxCollider>().enabled = false;
        //            J_Des = J_mouseModel.transform.parent.parent.parent.gameObject;
        //            J_PortName = J_mouseModel.transform.parent.name;
        //            J_OffPanel.SetActive(true);
        //        }
        //    }
        //}
        #endregion
    }

    public void UpdateEquipInfo()
    {
        //切换视野遍历场景中的设备信息
        string url = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
        StartCoroutine(WaitForEquipJsonData(url, "CabinetName", "CabinetPos", "EquipName", "EquipTypeID", "ID"));
    }

    IEnumerator WaitForEquipJsonData(string url, string node_1, string node_2, string node_3, string node_4, string node_5)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());

        J_cabinetname = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_cabinetname[i] = jar[i][node_1].ToString();
        }

        if (node_2 != "")
        {
            J_cabinetpos = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_cabinetpos[i] = jar[i][node_2].ToString();
            }
        }

        if (node_3 != "")
        {
            J_equipname = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_equipname[i] = jar[i][node_3].ToString();
            }
        }

        if (node_4 != "")
        {
            J_equiptypeid = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_equiptypeid[i] = jar[i][node_4].ToString();
            }
        }

        if (node_5 != "")
        {
            J_equipid = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_equipid[i] = jar[i][node_5].ToString();
            }
        }

        for (int i = 0; i < J_equipname.Length; i++)
        {
            print(J_equipname[i] + "  ====927=====  " + J_ClickModel.name);
            if (J_equipname[i] == J_ClickModel.name)
            {
                string EquipUrl = J_CUrl + "/APIS3d/device/api/web/ports/get-ports?id=" + J_equipid[i];
                StartCoroutine(WaitForEquipPort(EquipUrl, "portstatus", "portType", "PortName"));
                print("----------这里执行了930--------" + J_equipid[i]);
                J_Id = i;
                //string EquipUrl = J_CUrl+"equipment/GetPortInfo?id=" + J_equipid[i];
                //StartCoroutine(WaitForPortJsonData(EquipUrl, "portname", "portstatus", "porttype"));
            }
        }
    }

    IEnumerator WaitForEquipPort(string url, string node_1, string node_2, string node_3)
    {
        WWW www = new WWW(url);
        yield return www;
        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());

        J_portstatus = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_portstatus[i] = jar[i][node_1].ToString();
        }

        J_porttype = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_porttype[i] = jar[i][node_2].ToString();
        }

        J_portname = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_portname[i] = jar[i][node_3].ToString();
        }

        for (int i = 0; i < J_portstatus.Length; i++)
        {
            print("端口状态==" + J_portstatus[i] + "  端口类型==" + J_porttype[i] + "  端口名称==" + J_portname[i]);
        }
        //string EquipUrl = J_CUrl+"equipment/GetPortInfo?id=" + J_portid[int.Parse(J_Port) - 1];
        //StartCoroutine(WaitForPortJsonData(EquipUrl, "portname", "portstatus", "porttype"));
    }

    IEnumerator WaitForPortJsonData(string url, string node_1, string node_2, string node_3)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());

        J_portname = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_portname[i] = jar[i][node_1].ToString();
        }

        if (node_2 != "")
        {
            J_portstatus = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_portstatus[i] = jar[i][node_2].ToString();
            }
        }

        if (node_3 != "")
        {
            J_porttype = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_porttype[i] = jar[i][node_3].ToString();
            }
        }

        print("----------这里执行了974--------" + J_portname[0] + "------" + J_portstatus[0] + "-------" + J_porttype[0]);
    }

    void OnClickBtn(Button btn)
    {
        Vector3 tmpCam = J_player.transform.position;
        //暂时关闭
        //if (btn.name != "xianBtn" && btn.name != "xianBtn (1)")
        //{

        //}
        switch (btn.name)
        {
            case "CIBtn":
                J_GuidePanel.SetActive(false);
                break;
            case "GuideBtn":
                if (J_GuidePanel.activeInHierarchy)
                {
                    J_GuidePanel.SetActive(false);
                }
                else
                {
                    J_GuidePanel.SetActive(true);
                }
                break;
            #region    解决bug----进入某机房后，填写完关键字还未搜索便会弹出搜索结果列表
            case "SeaBtn_1":
            case "SeaBtn_2":
            case "SeaBtn_3":
                if (J_seafield.text != "")
                {
                    J_rect.SetActive(true);
                    J_scrollbar.SetActive(true);
                }
                break;
            #endregion

            case "FreeViewBtn":
                //切换为自由视角

                if (PlayerPrefs.HasKey("线缆位置X"))
                {
                    PlayerPrefs.DeleteKey("线缆位置X");
                    PlayerPrefs.DeleteKey("线缆位置Y");
                    PlayerPrefs.DeleteKey("线缆位置Z");
                    PlayerPrefs.DeleteKey("旋转位置X");
                    PlayerPrefs.DeleteKey("旋转位置Y");
                    PlayerPrefs.DeleteKey("旋转位置Z");
                }
                if (GameObject.Find("LS") != null)
                {
                    Destroy(GameObject.Find("LS"));
                }

                if (PlayerPrefs.HasKey("连接端口信息"))
                {
                    PlayerPrefs.DeleteKey("连接端口信息");
                }

                if (!AreaTipCon.instance.J_IsFirst)
                {
                    J_Tip.gameObject.SetActive(true);
                    J_Tip.text = "取消链接";
                    StartCoroutine(WaitCloseTipPanel());
                }

                AreaTipCon.instance.J_IsFirst = true;

                J_Ding.GetComponent<MeshRenderer>().enabled = false;
                J_player.GetComponent<DingCon>().J_IsThree = false;
                J_isFreeView = true;
                J_FreeCamera.gameObject.SetActive(true);
                //J_player.GetComponent<CameraMoveCon>().enabled = true;
                J_xian.GetComponent<DragImage>().J_isFreeView = true;
                J_player.GetComponent<FollowPlayer>().enabled = false;
                J_renwu.SetActive(false);
                J_player.SetActive(false);
                Debug.Log("鸟瞰");
                Debug.Log("视野切换为鸟瞰图" + J_player.transform.position);
                break;
            case "FirstSaidBtn":
                //切换为第一人称视野
                if (PlayerPrefs.HasKey("线缆位置X"))
                {
                    PlayerPrefs.DeleteKey("线缆位置X");
                    PlayerPrefs.DeleteKey("线缆位置Y");
                    PlayerPrefs.DeleteKey("线缆位置Z");
                    PlayerPrefs.DeleteKey("旋转位置X");
                    PlayerPrefs.DeleteKey("旋转位置Y");
                    PlayerPrefs.DeleteKey("旋转位置Z");
                }
                if (GameObject.Find("LS") != null)
                {
                    Destroy(GameObject.Find("LS"));
                }

                if (!AreaTipCon.instance.J_IsFirst)
                {
                    J_Tip.gameObject.SetActive(true);
                    J_Tip.text = "取消链接";
                    StartCoroutine(WaitCloseTipPanel());
                }

                AreaTipCon.instance.J_IsFirst = true;

                if (PlayerPrefs.HasKey("连接端口信息"))
                {
                    PlayerPrefs.DeleteKey("连接端口信息");
                }
                print("视野切换为第一人称");
                J_player.SetActive(true);
                J_IsThree = false;
                J_player.GetComponent<DingCon>().J_IsThree = true;
                J_Ding.GetComponent<MeshRenderer>().enabled = true;
                J_renwu.SetActive(true);
                if (J_isFreeView)
                {
                    J_isFreeView = false;
                    //J_renwu.transform.position = new Vector3(J_player.transform.position.x, J_renwu.transform.position.y, J_player.transform.position.z);
                    //J_renwu.transform.position = new Vector3(tmpCam.x, J_renwu.transform.position.y, tmpCam.z);
                    Debug.Log("视野切换为第一人称" + J_player.transform.position);
                }
                //J_player.GetComponent<CameraMoveCon>().enabled = false;
                J_xian.GetComponent<DragImage>().J_isFreeView = false;
                J_player.GetComponent<FollowPlayer>().enabled = false;
                J_player.GetComponent<FollowPlayer>().MinDistance = 0;
                J_player.GetComponent<FollowPlayer>().MaxDistance = 0;
                J_player.GetComponent<FollowPlayer>().Target = null;
                J_player.GetComponent<FollowPlayer>().enabled = true;
                J_player.GetComponent<FollowPlayer>().J_IsThree = false;
                J_FreeCamera.gameObject.SetActive(false);
                break;
            case "ThirdSaidBtn":
                //切换为第三人称视野

                if (PlayerPrefs.HasKey("线缆位置X"))
                {
                    PlayerPrefs.DeleteKey("线缆位置X");
                    PlayerPrefs.DeleteKey("线缆位置Y");
                    PlayerPrefs.DeleteKey("线缆位置Z");
                    PlayerPrefs.DeleteKey("旋转位置X");
                    PlayerPrefs.DeleteKey("旋转位置Y");
                    PlayerPrefs.DeleteKey("旋转位置Z");
                }
                if (GameObject.Find("LS") != null)
                {
                    Destroy(GameObject.Find("LS"));
                }

                if (!AreaTipCon.instance.J_IsFirst)
                {
                    J_Tip.gameObject.SetActive(true);
                    J_Tip.text = "取消链接";
                    StartCoroutine(WaitCloseTipPanel());
                }

                AreaTipCon.instance.J_IsFirst = true;

                if (PlayerPrefs.HasKey("连接端口信息"))
                {
                    PlayerPrefs.DeleteKey("连接端口信息");
                }
                print("视野切换为第三人称");
                J_IsThree = false;
                J_player.SetActive(true);
                J_player.GetComponent<DingCon>().J_IsThree = true;
                J_Ding.GetComponent<MeshRenderer>().enabled = true;
                J_renwu.SetActive(true);
                if (J_isFreeView)
                {
                    J_isFreeView = false;
                    //J_renwu.transform.position = new Vector3(J_player.transform.position.x, J_renwu.transform.position.y, J_player.transform.position.z);
                    //J_renwu.transform.position = new Vector3(tmpCam.x, J_renwu.transform.position.y, tmpCam.z);
                    Debug.Log("视野切换为第三人称" + J_player.transform.position);
                }
                //J_player.GetComponent<CameraMoveCon>().enabled = false;
                J_xian.GetComponent<DragImage>().J_isFreeView = false;
                J_player.GetComponent<FollowPlayer>().enabled = false;
                J_player.GetComponent<FollowPlayer>().MinDistance = 1.5f;
                J_player.GetComponent<FollowPlayer>().MaxDistance = 10;
                J_player.GetComponent<FollowPlayer>().Target = null;
                J_player.GetComponent<FollowPlayer>().enabled = true;
                J_player.GetComponent<FollowPlayer>().J_IsThree = true;
                J_FreeCamera.gameObject.SetActive(false);
                break;
            case "BackBtn":
                print("正在点击返回按钮");
                SceneManager.LoadScene("Computer_1");
                J_areatip.J_Show = "不显示";
                break;
            case "SaveBtn":
                print("正在点击机房场景保存按钮");
                //此处调用机房场景保存接口

                break;
            case "MoveBtn":
                //移动
                if (J_modelPanel != null)
                {
                    J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Translation);
                    J_modelPanel.SetActive(false);
                }
                break;
            case "RotateBtn":
                //旋转
                if (J_modelPanel != null)
                {
                    //J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Rotation);
                    //J_modelPanel.SetActive(false);
                    J_rotatepanel.SetActive(true);
                }
                break;

            #region  旋转方案一：
            case "SureRot":
                if (J_rotatefield.text != "")
                {
                    print("模型开始旋转");
                    float rotate = float.Parse(J_rotatefield.text);
                    J_mouseModel.transform.eulerAngles = new Vector3(0, rotate, 0);
                }
                J_rotatepanel.SetActive(false);
                J_modelPanel.SetActive(false);
                break;
            case "FreeRot":
                print("模型自由旋转");
                //J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Rotation);
                J_rotatepanel.SetActive(false);
                J_modelPanel.SetActive(false);
                break;
            #endregion

            case "ScaleBtn":
                //缩放
                if (J_modelPanel != null)
                {
                    J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Scale);
                    J_modelPanel.SetActive(false);
                }
                break;
            case "DeleBtn":
                //删除
                if (J_mouseModel != null)
                {
                    //Destroy(J_mouseModel);

                    J_modelPanel.SetActive(false);
                    J_isEditor = false;
                    if (J_mouseModel.tag == "jigui")
                    {
                        string url = "/APIS3d/device/api/web/cabinets/del-cabinets?id=" + J_jiguiID + "&Serial=" + X_CurrCabinetSerial + "&SwitchRoomID=" + AreaTipCon.instance.J_floorid;
                        Debug.Log("删除的机柜URL=====" + url + "   当前机柜序号：" + X_CurrCabinetSerial + "   当前房间ID：" + AreaTipCon.instance.J_floorid);
                        JsonContent.J_JsonContent.SaveDataToServer_1(url);
                        //关闭机柜信息面板
                        J_panel.SetActive(false);
                        print("----进行了机柜删除----" + J_jiguiID);
                        Invoke("DeleEquip", 0.5f);
                        //J_camera.GetComponent<FollowPlayer>().enabled = false;
                        //J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                        //J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                        //J_camera.GetComponent<FollowPlayer>().Target = null;
                        //J_camera.GetComponent<FollowPlayer>().enabled = true;
                        if (J_IsThree)
                        {
                            J_IsThree = false;
                            //J_camera.GetComponent<FollowPlayer>().enabled = false;
                            J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                            J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                            J_camera.GetComponent<FollowPlayer>().Target = null;
                            J_camera.GetComponent<FollowPlayer>().enabled = true;
                        }
                    }
                    if (J_mouseModel.tag == "Model")
                    {
                        string url = "/APIS3d/device/api/web/equipments/del-equipments?id=" + J_EquipID;
                        Debug.Log("删除的设备URL=====" + url);
                        JsonContent.J_JsonContent.SaveDataToServer_1(url);
                        //关闭设备信息面板
                        J_SBInfoPanel.SetActive(false);
                        Invoke("DeleEquip", 0.5f);
                        print("----进行了设备删除----" + J_EquipID);
                        if (J_IsThree)
                        {
                            J_IsThree = false;
                            //J_camera.GetComponent<FollowPlayer>().enabled = false;
                            J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                            J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                            J_camera.GetComponent<FollowPlayer>().Target = null;
                            J_camera.GetComponent<FollowPlayer>().enabled = true;
                        }
                    }

                    if (J_mouseModel.tag == "Xian")
                    {
                        J_SurePanel.SetActive(true);
                    }
                    if (J_mouseModel.tag != "Xian")
                    {
                        J_mouseModel.transform.position = new Vector3(10000, 10000, 10000);
                        J_mouseModel.transform.gameObject.SetActive(false);
                        J_mouseModel.transform.SetParent(J_rubbish);
                        J_mouseModel = null;
                    }
                }
                break;
            case "COffBtn":
                J_OffPanel.SetActive(false);
                break;
            case "PathBtn":
                J_SurePanel.SetActive(false);
                int J_index = int.Parse(J_PortName) - 1;
                J_mouseModel.transform.parent.parent.parent.GetComponent<CableCon>().PortPath(J_index);
                Application.OpenURL("http://cams.zcwjvr.com:8100/view/fulllink.html?id="+AreaTipCon.instance.J_port+"&room="+AreaTipCon.instance.X_SwitchRoomID+"&type=2"+"&build="+AreaTipCon.instance.X_BuindID+"&floor="+AreaTipCon.instance.J_FloorId);
                print("正在点击端口全链路按钮! 当前端口是："+ AreaTipCon.instance.J_port+"     当前房间id："+ AreaTipCon.instance.X_SwitchRoomID+"      当前建筑id："+ AreaTipCon.instance.X_BuindID+"      当前楼层id是："+ AreaTipCon.instance.J_FloorId);
                break;
            case "OffBtn":
                J_OffPanel.SetActive(false);
                J_SurePanel.SetActive(true);
                break;
            case "CBTN":
                J_SurePanel.SetActive(false);
                int index = int.Parse(J_PortName) - 1;
                J_mouseModel.transform.parent.parent.parent.GetComponent<CableCon>().DeleCableInfo(index);
                J_mouseModel.transform.position = new Vector3(10000, 10000, 10000);
                J_mouseModel.transform.gameObject.SetActive(false);
                J_mouseModel.transform.SetParent(J_rubbish);
                J_mouseModel = null;
                PlayerPrefs.DeleteKey("连接端口信息");
                //J_camera.GetComponent<FollowPlayer>().enabled = false;
                J_camera.GetComponent<FollowPlayer>().MinDistance = 0.5f;
                J_camera.GetComponent<FollowPlayer>().MaxDistance = 10;
                J_camera.GetComponent<FollowPlayer>().Target = null;
                J_camera.GetComponent<FollowPlayer>().enabled = true;
                //SceneManager.LoadScene("jifang01");
                Invoke("DeleEquip", 0.5f);
                print("----进行了端口断开----" + J_jiguiID);
                break;
            case "CLBTN":
                J_SurePanel.SetActive(false);
                break;
            case "SbBtn":
                if (SbPanel.activeInHierarchy)
                {
                    SbPanel.SetActive(false);
                }
                else
                {
                    SbPanel.SetActive(true);
                }
                break;
            case "DlBtn":
                if (DlPanel.activeInHierarchy)
                {
                    DlPanel.SetActive(false);
                }
                else
                {
                    DlPanel.SetActive(true);
                }
                break;
            case "EditBtn":
                btn.transform.parent.gameObject.SetActive(false);
                break;
            case "SeaveBtn":

                print("正在点击机柜信息保存按钮");
                J_NameInput.gameObject.SetActive(false);
                J_PlantInput.gameObject.SetActive(false);
                J_NumInput.gameObject.SetActive(false);
                J_PropertInput.gameObject.SetActive(false);
                J_BuyTimeInput.gameObject.SetActive(false);
                J_TimeInput.gameObject.SetActive(false);

                if (J_NameInput.text != "")
                {
                    J_NameText.text = J_NameInput.text;
                    J_ClickObj.name = J_NameInput.text;
                }
                if (J_PlantInput.text != "")
                {
                    J_PlantText.text = J_PlantInput.text;
                }
                if (J_NumInput.text != "")
                {
                    J_NumText.text = J_NumInput.text;
                }
                if (J_PropertInput.text != "")
                {
                    J_PropertyText.text = J_PropertInput.text;
                }
                if (J_BuyTimeInput.text != "")
                {
                    J_BuyTimeText.text = J_BuyTimeInput.text;
                }
                if (J_TimeInput.text != "")
                {
                    J_TimeText.text = J_TimeInput.text;
                }

                J_NameText.gameObject.SetActive(true);
                J_PlantText.gameObject.SetActive(true);
                J_NumText.gameObject.SetActive(true);
                J_PropertyText.gameObject.SetActive(true);
                J_BuyTimeText.gameObject.SetActive(true);
                J_TimeText.gameObject.SetActive(true);
                J_Data = new Dictionary<string, string>();
                print(J_ClickObj.name);

                //wwwform保存数据
                //此处将坐标强转为正整数
                float J_PosX = -J_ClickObj.transform.localPosition.x;
                float J_PosY = J_ClickObj.transform.localPosition.z;//机柜的Z坐标
                float J_Angle = J_ClickObj.transform.eulerAngles.y;
                //if (J_PosX.ToString().Length > 4)
                //{
                //    J_PosX = float.Parse(J_PosX.ToString().Substring(0, 4));
                //}
                //if (J_PosY.ToString().Length > 4)
                //{
                //    J_PosY = float.Parse(J_PosY.ToString().Substring(0, 4));
                //}
                //if (J_Angle.ToString().Length > 4)
                //{
                //    J_Angle = float.Parse(J_Angle.ToString().Substring(0, 4));
                //}


                X_Data1 = new Dictionary<string, string>();
                X_Data1.Add("RoomID", AreaTipCon.instance.J_floorid);
                
                JsonContent.J_JsonContent.ContentJson_5("/APIS/device/api/web/cabinets/room-position", X_Data1, "Serial");
                X_CabinetSerial = JsonContent.J_JsonContent.ReturnJsonData_5_1();

                //X_CabinetSerial[0] 机柜位置序号
                X_Data = new Dictionary<string, string>();
                X_Data.Add("id", AreaTipCon.instance.J_floorid);
                JsonContent.J_JsonContent.ContentJson_4_1("/APIS/device/api/web/switchrooms/get-switchrooms-byid", X_Data, "RoomType");
                X_RoomType = JsonContent.J_JsonContent.ReturnJsonData_4_1();

                if (!J_IsNew)
                {
                    J_Data.Add("id", J_Cabinetid);
                }
                //暂时关闭
                //if (J_IsNew)
                //{
                //    J_Data.Add("CabinetSum", AreaTipCon.instance.J_CabinetNum);
                //}

                //switch (X_RoomType[0])
                //{
                //    case "1":
                //        X_RoomSize = "1";//7*3改成1
                //        break;
                //    case "2":
                //        X_RoomSize = "2";//17*11改成2
                //        break;
                //    case "3":
                //        X_RoomSize = "3";//35*18改成3
                //        break;
                //}
                J_Data = new Dictionary<string, string>();
                J_Data.Add("Serial", X_CabinetSerial[0]);
                J_Data.Add("RoomSize", X_RoomType);
                J_Data.Add("RoomType", X_RoomType);
               
                Debug.Log("当前新建机柜的机房ID是：  " + AreaTipCon.instance.J_floorid);


                J_Data.Add("SwitchRoomID", AreaTipCon.instance.J_floorid);
                J_Data.Add("CabinetName", J_ClickObj.name);
                J_Data.Add("ModelName", "机柜_1");
                //J_Data.Add("IconName", "jigui");
                //J_Data.Add("PosX", J_PosX.ToString());
                //J_Data.Add("PosY", J_PosY.ToString());
                J_Data.Add("Angle", J_Angle.ToString());
                //J_Data.Add("IconName", "123");
                if (J_ClickObj.transform.Find("Type").tag == "jigui01")
                {
                    J_Data.Add("CabinetType", "1200");
                }
                else
                {
                    J_Data.Add("CabinetType", "600");
                }
                J_Data.Add("StartTime", J_TimeInput.text);
                J_Data.Add("BuyTime", J_BuyTimeInput.text);
                J_Data.Add("MakeTime", "3");
                J_Data.Add("Manufacturer", J_PlantInput.text);
                J_Data.Add("AssetsSerial", J_PropertInput.text);
                J_Data.Add("ManufactureSerial", J_NumInput.text);
                //
                J_NameInput.text = "";
                J_PlantInput.text = "";
                J_NumInput.text = "";
                J_PropertInput.text = "";
                J_BuyTimeInput.text = "";
                J_TimeInput.text = "";


                if (J_IsNew)
                {
                    //机柜新增
                    JsonContent.J_JsonContent.SaveDataToServer("/APIS3d/device/api/web/cabinets/save-cabinets", J_Data);
                    foreach (KeyValuePair<string, string> kvp in J_Data)
                    {
                        Debug.Log("键" + kvp.Key + "    值" + kvp.Value);
                    }
                    //try
                    //{
                    //    JsonContent.J_JsonContent.SaveDataToServer("/APIS3d/device/api/web/cabinets/save-cabinets", J_Data);
                    //    foreach (KeyValuePair<string, string> kvp in J_Data)
                    //    {
                    //        Debug.Log("键" + kvp.Key + "    值" + kvp.Value);
                    //    }
                    //}
                    //catch (System.Exception)
                    //{

                    //    Debug.Log("报错重试...");
                    //}
                    //finally
                    //{
                    //    ShowSavaInfo();
                    //}

                    Invoke("ShowSavaInfo", 1.5f);
                    J_IsShow = true;

                    GameObject obj = transform.Find("/Tfather/jifang/" + J_ClickObj.name).gameObject;
                    New_X = float.Parse(DragImage.CabinetPosX);
                    New_Y = float.Parse(DragImage.CabinetPosY);
                    obj.transform.localPosition = new Vector3(-New_X, 0, New_Y);

                }
                else
                {
                    //机柜保存
                    if (J_LastName != J_NameText.text || J_LastPlant != J_PlantText.text || J_LastNum != J_NumText.text || J_LastProperty != J_PropertyText.text || J_LastBuyTime != J_BuyTimeText.text || J_LastTime != J_TimeText.text)
                    {
                        J_LastName = J_NameText.text;
                        J_LastPlant = J_PlantText.text;
                        J_LastNum = J_NumText.text;
                        J_LastProperty = J_PropertyText.text;
                        J_LastBuyTime = J_BuyTimeText.text;
                        J_LastTime = J_TimeText.text;

                        JsonContent.J_JsonContent.SaveDataToServer("/APIS3d/device/api/web/cabinets/save-cabinets", J_Data);
                        Invoke("ShowSavaInfo", 0.5f);
                        print(J_LastName + "---" + J_LastPlant + "---" + J_LastNum + "---" + J_LastProperty + "---" + J_LastBuyTime + "---" + J_LastTime);
                        J_Data = new Dictionary<string, string>();
                        J_Data.Add("id", AreaTipCon.instance.J_floorid);
                        //J_Data.Add("userName", PlayerPrefs.GetString("账号"));
                        J_Data.Add("UserID", LoginCon.X_UserID);
                        J_Data.Add("RolesID", LoginCon.X_RolesID);
                        J_ISShow = true;
                        string url = "/APIS3d/device/api/web/cabinets/get-cabinets";//生产厂家，序列号，资产编号，购买时间，开始时间
                        JsonContent.J_JsonContent.ContentJson_4(url, J_Data, "CabinetName", "ID", "Angle", "PosX", "PosY", "Manufacturer", "ManufactureSerial", "AssetsSerial", "BuyTime", "StartTime", "CabinetType","Serial");
                        Invoke("UpdateCabinetData", 0.5f);
                    }
                }
                break;
            case "CancelBtn":
                J_modelPanel.SetActive(false);
                break;
            case "CloseBtn":
                if (J_IsNew)
                {
                    J_IsNew = false;
                    Destroy(J_ClickObj);
                }
                btn.transform.parent.gameObject.SetActive(false);
                break;
            case "AddBtn":
                if (J_modelpanel.activeInHierarchy)
                {
                    J_modelpanel.SetActive(false);
                }
                else
                {
                    J_modelpanel.SetActive(true);
                }
                break;
            case "EdBtn":
                //机柜信息编辑按钮
                if (J_modelPanel.activeInHierarchy)
                {
                    J_modelPanel.SetActive(false);
                    J_isEditor = false;
                }
                else
                {
                    J_modelPanel.SetActive(true);
                    J_isEditor = true;
                }
                break;
            case "NameBtn":
                J_NameText.gameObject.SetActive(false);
                J_NameInput.text = J_NameText.text;
                J_NameInput.gameObject.SetActive(true);
                break;
            case "PlantBtn":
                J_PlantText.gameObject.SetActive(false);
                J_PlantInput.text = J_PlantText.text;
                J_PlantInput.gameObject.SetActive(true);
                break;
            case "NumBtn":
                J_NumText.gameObject.SetActive(false);
                J_NumInput.text = J_NumText.text;
                J_NumInput.gameObject.SetActive(true);
                break;
            case "PropertyBtn":
                J_PropertyText.gameObject.SetActive(false);
                J_PropertInput.text = J_PropertyText.text;
                J_PropertInput.gameObject.SetActive(true);
                break;
            case "BuyTimeBtn":
                J_BuyTimeText.gameObject.SetActive(false);
                J_BuyTimeInput.text = J_BuyTimeText.text;
                J_BuyTimeInput.gameObject.SetActive(true);
                break;
            case "TimeBtn":
                J_TimeText.gameObject.SetActive(false);
                J_TimeInput.text = J_TimeText.text;
                J_TimeInput.gameObject.SetActive(true);
                break;

            #region  废弃的搜索功能
            //case "SeaBtn":
            //    J_isstart = true;
            //    J_isstop = true;
            //    if (J_seafield.text != "")
            //    {
            //        if (GameObject.Find(J_jifang.name + "/" + J_seafield.text) == null)
            //        {
            //            J_text.GetComponent<Text>().text = "搜索的设备不存在，请重新填写";
            //            J_text.SetActive(true);
            //            StartCoroutine(WaitCloseTipPanel());
            //            return;
            //        }
            //        J_seaobj = J_jifang.transform.Find(J_seafield.text).gameObject;
            //        J_player.SetActive(false);
            //        J_renwu.transform.position = J_seaobj.transform.position + Vector3.forward * 0.7f;
            //        J_player.SetActive(true);
            //        J_seafield.text = "";
            //    }
            //    break;
            #endregion

            case "ProBtn":
                AreaTipCon.instance.J_IsFirst = true;
                if (PlayerPrefs.HasKey("线缆位置X"))
                {
                    PlayerPrefs.DeleteKey("线缆位置X");
                    PlayerPrefs.DeleteKey("线缆位置Y");
                    PlayerPrefs.DeleteKey("线缆位置Z");
                    PlayerPrefs.DeleteKey("旋转位置X");
                    PlayerPrefs.DeleteKey("旋转位置Y");
                    PlayerPrefs.DeleteKey("旋转位置Z");
                }
                if (PlayerPrefs.HasKey("连接端口信息"))
                {
                    PlayerPrefs.DeleteKey("连接端口信息");
                }
                SceneManager.LoadScene("AreaScene");
                break;
            case "AreaBtn":
                AreaTipCon.instance.J_IsFirst = true;
                if (PlayerPrefs.HasKey("线缆位置X"))
                {
                    PlayerPrefs.DeleteKey("线缆位置X");
                    PlayerPrefs.DeleteKey("线缆位置Y");
                    PlayerPrefs.DeleteKey("线缆位置Z");
                    PlayerPrefs.DeleteKey("旋转位置X");
                    PlayerPrefs.DeleteKey("旋转位置Y");
                    PlayerPrefs.DeleteKey("旋转位置Z");
                }
                if (PlayerPrefs.HasKey("连接端口信息"))
                {
                    PlayerPrefs.DeleteKey("连接端口信息");
                }
                SceneManager.LoadScene("Computer_1");
                break;
            case "FloorBtn":
                AreaTipCon.instance.J_IsFirst = true;
                if (PlayerPrefs.HasKey("线缆位置X"))
                {
                    PlayerPrefs.DeleteKey("线缆位置X");
                    PlayerPrefs.DeleteKey("线缆位置Y");
                    PlayerPrefs.DeleteKey("线缆位置Z");
                    PlayerPrefs.DeleteKey("旋转位置X");
                    PlayerPrefs.DeleteKey("旋转位置Y");
                    PlayerPrefs.DeleteKey("旋转位置Z");
                }
                if (PlayerPrefs.HasKey("连接端口信息"))
                {
                    PlayerPrefs.DeleteKey("连接端口信息");
                }
                //AreaTipCon.instance.J_IsFloor = true;
                //isReturn = true;
                SceneManager.LoadScene("Computer_1");
                
                break;
            case "SynBtn":
                AreaTipCon.instance.J_IsFirst = true;
                if (PlayerPrefs.HasKey("线缆位置X"))
                {
                    PlayerPrefs.DeleteKey("线缆位置X");
                    PlayerPrefs.DeleteKey("线缆位置Y");
                    PlayerPrefs.DeleteKey("线缆位置Z");
                    PlayerPrefs.DeleteKey("旋转位置X");
                    PlayerPrefs.DeleteKey("旋转位置Y");
                    PlayerPrefs.DeleteKey("旋转位置Z");
                }
                if (PlayerPrefs.HasKey("连接端口信息"))
                {
                    PlayerPrefs.DeleteKey("连接端口信息");
                }
                SceneManager.LoadScene("ProjectScene");
                break;
            case "Refresh":
                if (AreaTipCon.instance.X_RoomType == "3")
                {
                    SceneManager.LoadScene("jifang01");
                }
                if (AreaTipCon.instance.X_RoomType == "2")
                {
                    SceneManager.LoadScene("jifang02");
                }
                if (AreaTipCon.instance.X_RoomType == "1")
                {
                    SceneManager.LoadScene("jifang03");
                }
                break;
        }
    }

    private void DeleEquip()
    {
        Debug.Log("----在此处调用了删除设备方法----");
        //CableCon.J_cable.OffCable();
        //CableCon.J_cable.J_IsUpdate = true;

        GameObject[] J_Model = GameObject.FindGameObjectsWithTag("Model");
        foreach (GameObject J_Item in J_Model)
        {
            J_Item.GetComponent<CableCon>().OffCable();
        }
    }


    

    void UpdateCabinetData()
    {
        J_CabinetName = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_CabinetID = JsonContent.J_JsonContent.ReturnJsonData_2();
        J_Angle = JsonContent.J_JsonContent.ReturnJsonData_3();
        J_PosX = JsonContent.J_JsonContent.ReturnJsonData_4();
        J_PosY = JsonContent.J_JsonContent.ReturnJsonData_5();
        manufacturer = JsonContent.J_JsonContent.ReturnJsonData_6();//生产厂家
        manufactureserial = JsonContent.J_JsonContent.ReturnJsonData_7();//序列号
        assetsserial = JsonContent.J_JsonContent.ReturnJsonData_8();//资产编号
        buytime = JsonContent.J_JsonContent.ReturnJsonData_9();//购买时间
        starttime = JsonContent.J_JsonContent.ReturnJsonData_10();//开始时间
        J_CabinetType = JsonContent.J_JsonContent.ReturnJsonData_11();//机柜类型
        X_CurrSerialArr = JsonContent.J_JsonContent.ReturnJsonData_12();//所有机柜的对应位置
        J_ISShow = false;
    }

    //定时关闭提示面板
    IEnumerator WaitCloseTipPanel()
    {
        yield return new WaitForSeconds(1);
        J_text.SetActive(false);
    }

    public void WaitClose()
    {
        StartCoroutine(WaitClosePanel());
    }

    IEnumerator WaitClosePanel()
    {
        yield return new WaitForSeconds(1);
        J_Tip.gameObject.SetActive(false);
    }

    IEnumerator WaitChangeScene()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Computer_1");
    }

    void ShowSavaInfo()
    {
        string SaveData = JsonContent.J_JsonContent.ReturnSaveData();
        if (SaveData == "")
        {
            if (J_IsShow)
            {
                J_IsShow = false;
                //J_Tip.text = "保存成功";
                //J_Tip.gameObject.SetActive(true);
                //this.transform.root.gameObject.GetComponent<CreatModelCon>().WaitClose();
                //gameObject.SetActive(false);
                J_panel.SetActive(false);
                J_IsNew = false;
                J_Tip.text = "机柜新建成功";
                J_Tip.gameObject.SetActive(true);
                StartCoroutine(WaitClosePanel());

                J_Data = new Dictionary<string, string>();
                J_Data.Add("id", AreaTipCon.instance.J_floorid);
                //J_Data.Add("userName", PlayerPrefs.GetString("账号"));
                J_Data.Add("UserID", LoginCon.X_UserID);
                J_Data.Add("RolesID", LoginCon.X_RolesID);
                J_ISShow = true;
                string url = "/APIS3d/device/api/web/cabinets/get-cabinets";//生产厂家，序列号，资产编号，购买时间，开始时间
                JsonContent.J_JsonContent.ContentJson_4(url, J_Data, "CabinetName", "ID", "Angle", "PosX", "PosY", "Manufacturer", "ManufactureSerial", "AssetsSerial", "BuyTime", "StartTime", "CabinetType","Serial");
                Invoke("UpdateCabinetData", 0.5f);
            
                print("机柜新建成功------------1632");
            }
        }
        else
        {
            SaveData = "";
            J_InfoPanel.SetActive(true);
            J_Tip.text = "名称重复，请重新操作";
            J_Tip.gameObject.SetActive(true);
            J_panel.SetActive(true);
            StartCoroutine(WaitClosePanel());
            if (J_IsShow)
            {
                J_IsShow = false;
                J_IsNew = true;
            }
            print("机柜名称重复------------1645");
        }
    }

    /// <summary>
    /// 关闭软件运行时，清除本地第一次线缆的连接信息
    /// </summary>
    private void OnDestroy()
    {
        //if (PlayerPrefs.HasKey("线缆位置X"))
        //{
        //    PlayerPrefs.DeleteKey("线缆位置X");
        //    PlayerPrefs.DeleteKey("线缆位置Y");
        //    PlayerPrefs.DeleteKey("线缆位置Z");
        //    PlayerPrefs.DeleteKey("旋转位置X");
        //    PlayerPrefs.DeleteKey("旋转位置Y");
        //    PlayerPrefs.DeleteKey("旋转位置Z");
        //}
    }

    /// <summary>
    /// 鸟瞰模式下  路径规划的方法
    /// </summary>
    private LineRenderer line;
    private Vector3[] pos;
    private void PathPlan()
    {

        //两种方法：一种直接划线并导航，另一种先画点，按Enter弹出窗口，确定后连接成线。   待定。
        line = this.gameObject.AddComponent<LineRenderer>();
        line.material = new Material(Shader.Find("Particles/Additive"));
        line.SetVertexCount(2);
        line.SetColors(Color.red, Color.red);
        line.SetWidth(0.1f, 0.1f);
        line.SetPositions(pos);

        if (J_isFreeView == true)
        {
            if (Input.GetMouseButtonUp(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitinfo;
                if (Physics.Raycast(ray,out hitinfo))
                {
                    Debug.Log("射线检测到了东西");
                }


            }
        }
    }



    [SerializeField]

    private LineRenderer lineRenderer;

    private bool firstMouseDown = false;//第一次点击

    private bool mouseDown = false; //一直点击

    /// <summary>
    /// 放在Update里的方法
    /// </summary>
    private void InUpdate()

    {

        if (Input.GetMouseButtonDown(0))
        {

            firstMouseDown = true;

            mouseDown = true;

        }

        if (Input.GetMouseButtonUp(0))
        {

            mouseDown = false;

        }

        OnDrawLine(); //调用画线方法

        firstMouseDown = false;

    }

    private Vector3[] positions = new Vector3[10];//所有坐标（10个点）

    private int posCount = 0;//当前第几个坐标

    private Vector3 head;//头坐标

    private Vector3 last;//上一次头坐标

    private void OnDrawLine()
    {

        if (firstMouseDown)
        {

            posCount = 0;

            head = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            last = head;

        }

        if (mouseDown)

        {

            head = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Vector3.Distance(head, last) > 0.01f)

            {

                SavePosition(head);

                posCount++;

            }

            OnRayCast(head);

            last = head;

        }

        else
        {

            this.positions = new Vector3[10];

        }

        ChangePositions(positions);

    }

    private void SavePosition(Vector3 pos)
    {

        pos.z = 0;

        if (posCount <= 9)

        {

            for (int i = posCount; i < 10; i++)

            {

                positions[i] = pos;

            }

        }

        else
        {

            for (int i = 0; i < 9; i++)
            {

                positions[i] = positions[i + 1];

            }

            positions[9] = pos;

        }

    }

    private void ChangePositions(Vector3[] positions)
    {

        lineRenderer.SetPositions(positions);

    }

    private void OnRayCast(Vector3 worldPos)
    {

        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);

        Ray ray = Camera.main.ScreenPointToRay(screenPos);

        RaycastHit[] hits = Physics.RaycastAll(ray);

        for (int i = 0; i < hits.Length; i++)
        {

            //Destroy(hits[i].collider.gameObject);

            hits[i].collider.gameObject.SendMessage("OnCut", SendMessageOptions.DontRequireReceiver);

        }

    }


}
