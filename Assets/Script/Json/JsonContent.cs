﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json.Converters;

public class JsonContent : MonoBehaviour
{
    private string url_1= "http://cams.zcwjvr.com:8080/";
    private string[] J_Str_1;
    private string[] J_Str_2;
    private string[] J_Str_3;
    private string[] J_Str_4;
    private string[] J_Str_5;
    private string[] J_Str_6;
    private string[] J_Str_7;
    private string[] J_Str_8;
    private string[] J_Str_9;
    private string[] J_Str_10;
    private string[] J_Str_11;
    private string[] J_Str_12;
    private string[] J_Strs;
    private string J_SaveReturn;
    private string X_Str_1;
    private string[] X_Str_2;

    public static JsonContent J_JsonContent;

    void Awake()
    {
        url_1 = GameConst.ServerUrl;// File.ReadAllText("D://Url.txt");

        Screen.SetResolution(1920, 925, false);
        if (J_JsonContent == null)
        {
            J_JsonContent = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void ContentJson_1(string url_2, string node_1, string node_2, string node_3, string node_4, string node_5, string node_6)
    {
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_1(url, node_1, node_2, node_3, node_4, node_5, node_6));
        Debug.Log("1");
    }

    public void ContentJson_2(string url_2, string node, string field_1, string field_2, int index)
    {
        Debug.Log("2");
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_2(url, node, field_1, field_2, index));
    }

    public void ContentJson_4
        (string url_2, Dictionary<string, string> post, string node_1, string node_2, string node_3, string node_4, string node_5, string node_6, string node_7, string node_8, string node_9, string node_10, string node_11,string node_12)
    {
        Debug.Log("3");
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_4(url, post, node_1, node_2, node_3, node_4, node_5, node_6, node_7, node_8, node_9, node_10, node_11,node_12));
    }

    public void ContentJson_5(string url_2, Dictionary<string, string> post, string node_1)
    {
        Debug.Log("4");
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_5(url, post, node_1));
    }

    private bool isTest = false;
    IEnumerator WaitForJsonData_1(string url, string node_1, string node_2, string node_3, string node_4, string node_5, string node_6)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        if (isTest)
        {
            string test = DataMgr.Instance.TestNode.ToString();
            jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(test);
        }

        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());
        J_Str_1 = new string[jar.Count];
        List<string> strList = new List<string>();

        for (int i = 0; i < jar.Count; i++)
        {
            J_Str_1[i] = jar[i][node_1].ToString();
        }

       

        if (node_2 != "")
        {
            J_Str_2 = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_Str_2[i] = jar[i][node_2].ToString();
            }
        }


        if (node_3 != "")
        {
            J_Str_3 = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_Str_3[i] = jar[i][node_3].ToString();
                print(J_Str_3[i] + node_3 + "---node_3---");
            }
        }

        if (node_4 != "")
        {
            J_Str_4 = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_Str_4[i] = jar[i][node_4].ToString();
                //print(J_Str_4[i] + node_4 + "---node_4---");
            }
        }

        if (node_5 != "")
        {
            J_Str_5 = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_Str_5[i] = jar[i][node_5].ToString();
                //print(J_Str_5[i] + node_5 + "---node_3---");
            }
        }

        if (node_6 != "")
        {
            J_Str_6 = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_Str_6[i] = jar[i][node_5].ToString();
                //print(J_Str_5[i] + node_5 + "---node_3---");
            }
        }
        strList.Add(string.Format("{0}=>{1}", node_1, string.Join("|", J_Str_1)));
        strList.Add(string.Format("{0}=>{1}", node_2, string.Join("|", J_Str_2)));
        strList.Add(string.Format("{0}=>{1}", node_3, string.Join("|", J_Str_3)));
        strList.Add(string.Format("{0}=>{1}", node_4, string.Join("|", J_Str_4)));
        strList.Add(string.Format("{0}=>{1}", node_5, string.Join("|", J_Str_5)));
        strList.Add(string.Format("{0}=>{1}", node_6, string.Join("|", J_Str_6)));

        DataMgr.Instance.PrintDataArr(url, jar.ToString());
        DataMgr.Instance.PrintDataArr(url, strList.ToArray());
        Debug.Log("4");
    }

    IEnumerator WaitForJsonData_4
        (string url, Dictionary<string, string> post, string node_1, string node_2, string node_3, string node_4, string node_5, string node_6, string node_7, string node_8, string node_9, string node_10, string node_11,string node_12)
    {
        Debug.Log("执行了Json_4");
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);

        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        Debug.LogError(url+"    ||||||||    "+www.text);
        if (jo["code"].ToString() == "0")
        {
            Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
            print(jar.ToString());
            J_Str_1 = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                print(jar.Count + "-------------------");
                J_Str_1[i] = jar[i][node_1].ToString();
                print(J_Str_1[i] + node_1 + "---node_1---");
            }

            if (node_2 != "")
            {
                J_Str_2 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_2[i] = jar[i][node_2].ToString();
                    print(J_Str_2[i] + node_2 + "---node_2---" + "----url     " + url);
                }
            }

            if (node_3 != "")
            {
                J_Str_3 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_3[i] = jar[i][node_3].ToString();
                    print(J_Str_3[i] + node_3 + "---node_3---");
                }
            }

            if (node_4 != "")
            {
                J_Str_4 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_4[i] = jar[i][node_4].ToString();
                    print(J_Str_4[i] + node_4 + "---node_4---");
                }
            }

            if (node_5 != "")
            {
                J_Str_5 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_5[i] = jar[i][node_5].ToString();
                    print(J_Str_5[i] + node_5 + "---node_3---");
                }
            }
            if (node_6 != "")
            {
                J_Str_6 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_6[i] = jar[i][node_6].ToString();
                    print(J_Str_6[i] + node_6 + "---node_3---");
                }
            }
            if (node_7 != "")
            {
                J_Str_7 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_7[i] = jar[i][node_7].ToString();
                    print(J_Str_7[i] + node_7 + "---node_3---");
                }
            }
            if (node_8 != "")
            {
                J_Str_8 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_8[i] = jar[i][node_8].ToString();
                    print(J_Str_8[i] + node_8 + "---node_3---");
                }
            }
            if (node_9 != "")
            {
                J_Str_9 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_9[i] = jar[i][node_9].ToString();
                    print(J_Str_9[i] + node_9 + "---node_3---");
                }
            }
            if (node_10 != "")
            {
                J_Str_10 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_10[i] = jar[i][node_10].ToString();
                    print(J_Str_10[i] + node_10 + "---node_3---");
                }
            }

            if (node_11 != "")
            {
                J_Str_11 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_11[i] = jar[i][node_11].ToString();
                }
            }
            if (node_12 != "")
            {
                J_Str_12 = new string[jar.Count];
                for (int i = 0; i < jar.Count; i++)
                {
                    J_Str_12[i] = jar[i][node_12].ToString();
                }
            }
        }
        else
        {
            if (jo["code"].ToString() == "")
            {
                J_Str_1 = new string[1];
                J_Str_1[0] = "对不起，您没有该机房管理的权限，无法进入";
            }
        }
        Debug.Log("5");
    }

    IEnumerator WaitForJsonData_2(string url, string node, string field_1, string field_2, int index)
    {
        WWW www = new WWW(url);
        yield return www;
        Debug.Log("nihao2 " + url);

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo[field_1].ToString());
        print(jar.Count + "----jar.Count----");
        J_Strs = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            Newtonsoft.Json.Linq.JToken newJar = Newtonsoft.Json.Linq.JToken.Parse(jar[i][field_2].ToString());
            J_Strs[i] = newJar[node].ToString();
            print(J_Strs[i] + node + "---node---");
        }
    }

    IEnumerator WaitForJsonData_3(string url, string node, string field_1, string field_2, int index)
    {
        WWW www = new WWW(url);
        yield return www;
        Debug.Log("nihao3 " + url);

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo[field_1].ToString());
        print(jar[1].ToString());
        Newtonsoft.Json.Linq.JArray newJar = Newtonsoft.Json.Linq.JArray.Parse(jar[index][field_2].ToString());
        J_Strs = new string[newJar.Count];
        for (int i = 0; i < newJar.Count; i++)
        {
            J_Strs[i] = newJar[i][node].ToString();
            print(J_Strs[i]);
        }
    }

    public string[] ReturnJsonData_1()
    {
        Debug.Log(J_Str_1+"J_str_1");
        return J_Str_1;
    }

    public string[] ReturnJsonData_2()
    {
        Debug.Log(J_Str_2 + "J_Str_2");
        return J_Str_2;
    }

    public string[] ReturnJsonData_3()
    {
        Debug.Log(J_Str_3 + "J_Str_3");
        return J_Str_3;
    }

    public string[] ReturnJsonData_4()
    {
        Debug.Log(J_Str_4 + "J_Str_4");
        return J_Str_4;
    }

    public string[] ReturnJsonData_5()
    {
        Debug.Log(J_Str_5 + "J_Str_5");
        return J_Str_5;
    }

    public string[] ReturnJsonData_6()
    {
        return J_Str_6;
    }

    public string[] ReturnJsonData_7()
    {
        return J_Str_7;
    }

    public string[] ReturnJsonData_8()
    {

        return J_Str_8;
    }
    public string[] ReturnJsonData_9()
    {
        return J_Str_9;
    }

    public string[] ReturnJsonData_10()
    {
        return J_Str_10;
    }

    public string[] ReturnJsonData_11()
    {
        return J_Str_11;
    }
    public string[] ReturnJsonData_12()
    {
        return J_Str_12;
    }

    public string[] ReturnJsonDatas()
    {
        return J_Strs;
    }

    public string ReturnSaveData()
    {
        return J_SaveReturn;
    }

    public void SaveDataToServer(string url_2, Dictionary<string, string> post)
    {
        string url = url_1 + url_2;
        StartCoroutine(PostDataToServer(url, post));
    }

    IEnumerator PostDataToServer(string url, Dictionary<string, string> post)
    {
        print(url + "post:       "+post);
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {

            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
            Debug.Log("Key:   " + saveElectric.Key + "    Value:    " + saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);
        Debug.Log("url 地址==" + www.url);
        yield return www;
        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        Debug.Log(jo.ToString() + "===数据库返回的结果");
        if (jo["code"].ToString() == "0")
        {
            J_SaveReturn = "";
        }
        if (jo["code"].ToString() == "-1")
        {
            J_SaveReturn = "名称重复";
        }
        print(www.text);
    }

    public void SaveDataToServer_1(string url_2)
    {
        string url = url_1 + url_2;
        StartCoroutine(PostDataToServer_1(url));
    }

    IEnumerator PostDataToServer_1(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        print(www.text.ToString());
        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(jo["code"].ToString());
        //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["code"].ToString());
        //print(jar[0].ToString());
        //Newtonsoft.Json.Linq.JArray newJar = Newtonsoft.Json.Linq.JArray.Parse(jar[index][field_2].ToString());
        //J_Strs = new string[newJar.Count];
        //for (int i = 0; i < newJar.Count; i++)
        //{
        //    J_Strs[i] = newJar[i][node].ToString();
        //    print(J_Strs[i]);
        //}
    }




    public void ContentJson_4_1(string url_2, Dictionary<string, string> post, string node_1)
    {
        Debug.Log("4_1");
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_4_1(url, post, node_1));
    }

    IEnumerator WaitForJsonData_4_1(string url, Dictionary<string, string> post, string node_1 )
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);
        yield return www;


        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text+"4_1");
        if (jo["code"].ToString() == "0")
        {
            
            

            print(jo["data"][node_1].ToString());

            X_Str_1 = jo["data"][node_1].ToString();
            //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
            
            

            
        }
        else
        {
            if (jo["code"].ToString() == "-1")
            {
                
                X_Str_1 = "对不起，您没有该机房管理的权限，无法进入";
            }
        }
        Debug.Log("5");
    }


    public string ReturnJsonData_4_1()
    {
        Debug.Log(X_Str_1 + "X_str_1");
        return X_Str_1;
    }



    IEnumerator WaitForJsonData_5(string url, Dictionary<string, string> post, string node_1)
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);

        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        if (jo["code"].ToString() == "0")
        {
            Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
            print(jar.ToString());
            X_Str_2 = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                print(jar.Count + "-------------------");
                X_Str_2[i] = jar[i][node_1].ToString();
                print(X_Str_2[i] + node_1 + "---node_1---");
            }
        }
        else
        {
            if (jo["code"].ToString() == "-1")
            {
                Debug.Log("获取Serial失败..");
            }
        }
        yield return new WaitForSeconds(1f);
    }

    public string[] ReturnJsonData_5_1()
    {
        return X_Str_2;
    }


    public void ContentJson_CabInfo
        (string url_2, Dictionary<string, string> post, string node_1, string node_2, string node_3, string node_4, string node_5, string node_6, string node_7, string node_8, string node_9, string node_10, string node_11, string node_12)
    {
        Debug.Log("3");
        string url = url_1 + url_2;
        StartCoroutine(WaitForJsonData_4(url, post, node_1, node_2, node_3, node_4, node_5, node_6, node_7, node_8, node_9, node_10, node_11, node_12));
    }



}
