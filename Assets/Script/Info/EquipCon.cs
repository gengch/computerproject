﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipCon : MonoBehaviour
{
    private Vector3 J_StartPos;
    private Vector3 J_NowPos;
    private RaycastHit J_hit;
    private Camera J_Camera;

    void Start()
    {
        J_StartPos = this.gameObject.transform.position;
        J_Camera = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();
    }

    void Update()
    {
        J_NowPos = this.gameObject.transform.position;
        if (Input.GetMouseButtonUp(0) && J_StartPos != J_NowPos)
        {
            Ray J_ray = J_Camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(J_ray, out J_hit))
            {
                this.gameObject.transform.position = J_StartPos;
                if (J_hit.collider.gameObject.tag == "jigui" || J_hit.collider.gameObject.name == "jiguimen")
                {

                }
                else
                {
                    
                }
            }
            J_NowPos = J_StartPos;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "dangban")
        {

        }
    }
}
