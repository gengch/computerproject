﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UITipCon : MonoBehaviour
{
    private Camera J_camera;

    private void Start()
    {
        J_camera = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();
        transform.parent.GetComponent<Canvas>().worldCamera = J_camera;
        GetComponent<Button>().onClick.AddListener(OnClickBtn);
    }

    private void OnClickBtn()
    {
        print("---执行了---" + GetComponentInChildren<Text>().text);
    }
}
