﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChosePanelCon : MonoBehaviour
{
    private GameObject searchbg;//生成的每一行的显示物体 
    private GameObject gridnameshow;
    public GameObject J_Girl1;
    public GameObject J_Girl2;
    public GameObject J_Girl3;

    private string[] switchroomname;//机房名称
    private string[] id;//机房id
    private string[] floorareaid;//楼层id
    private string[] J_RoomType;//房间类型
    private string J_Roomtype;//选择的房间类型

    private string[] J_CabinetID;//机柜ID
    private string[] J_CabinetName;//机柜名称
    private string[] J_Angle;//机柜旋转角度
    private string[] J_PosX;//机柜X坐标
    private string[] J_PosY;//机柜Y坐标

    private string[] J_EquipName;//设备名称
    private string J_equipname;
    private string J_cabinetname;
    private string roomid;
    private string cabinetid;
    private string[] J_EquipID;
    private GameObject J_Panel;
    private GameObject J_Panel1;
    private GameObject J_Panel2;
    private Dictionary<string, string> J_Data;
    private GameObject J_Tip;

    private string J_FloorStr;
    private string J_FloorID;
    private float J_Posx;
    private float J_Posy;
    private string J_EquipId;

    private void Awake()
    {
        J_Tip = transform.root.Find("Tip").gameObject;
        J_Panel = transform.Find("RoomPanel").gameObject;
        J_Panel1 = transform.Find("CabinetPanel").gameObject;
        J_Panel2 = transform.Find("EquipPanel").gameObject;

        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }

    void OnEnable()
    {
        string gridpath = "RB";//生成列表的路径  
        gridnameshow = Resources.Load(gridpath, typeof(GameObject)) as GameObject;//加载生成的子物体  

        JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/switchrooms/searche-switchrooms", "SwitchRoomName", "ID", "FloorAreaID", "RoomType", "", "");
        //JsonContent.J_JsonContent.ContentJson_1("/switch/switchRoomSelect?id=" + AreaTipCon.instance.J_FloorID, "switchroomname", "id", "floorareaid", "", "", "");
        //Grid的长度随着生成物体个数变化  
        J_Girl1.GetComponent<RectTransform>().sizeDelta = new Vector2(J_Girl1.GetComponent<RectTransform>().sizeDelta.x, 0);
        // 清空grid里的所有东西  
        List<Transform> lst = new List<Transform>();
        foreach (Transform child in J_Girl1.transform)
        {
            lst.Add(child);
            //Debug.Log(child.gameObject.name);
        }
        for (int i = 0; i < lst.Count; i++)
        {
            Destroy(lst[i].gameObject);
        }

        Invoke("WaitRoomData", 0.5f);
    }

    void AddLiatent()
    {
        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                print(J_tempbtn.name + "---------J_tempbtn.name--------");
                if (J_tempbtn.name != "RoomXB" && J_tempbtn.name != "CabinetXB" && J_tempbtn.name != "EquipXB" && J_tempbtn.name != "Go")
                {
                    OnClickBtn(J_tempbtn);
                }
            });
        }
    }

    private void OnClickBtn(GameObject go)
    {
        //go.transform.parent.gameObject.SetActive(false);
        //DragImage.J_DragImage.J_IsFirst = false;
        //SceneManager.LoadScene("jifang01");
        //print("-------164------" + this.gameObject.name + "-----" + this.name);
        //// 清空grid里的所有东西  
        //List<Transform> lst = new List<Transform>();
        //foreach (Transform child in J_Girl1.transform)
        //{
        //    lst.Add(child);
        //    Debug.Log(child.gameObject.name);
        //}
        //for (int i = 0; i < lst.Count; i++)
        //{
        //    Destroy(lst[i].gameObject);
        //}
    }

    //机房数据
    void WaitRoomData()
    {
        switchroomname = JsonContent.J_JsonContent.ReturnJsonData_1();
        id = JsonContent.J_JsonContent.ReturnJsonData_2();
        floorareaid = JsonContent.J_JsonContent.ReturnJsonData_3();
        J_RoomType = JsonContent.J_JsonContent.ReturnJsonData_4();
        // 清空grid里的所有东西  
        List<Transform> lst = new List<Transform>();
        foreach (Transform child in J_Girl1.transform)
        {
            lst.Add(child);
            Debug.Log(child.gameObject.name);
        }
        for (int i = 0; i < lst.Count; i++)
        {
            Destroy(lst[i].gameObject);
        }
        for (int i = 0; i < switchroomname.Length; i++)
        {
            if (i == 0)
            {
                Generatenamegrids(switchroomname[i], id[i], true);//生成列表   
            }
            else
            {
                Generatenamegrids(switchroomname[i], id[i], false);//生成列表   
            }
            //此处生成按钮等== SeaField.text
            print(switchroomname[i] + "----switchroomname[i]----");
        }
        AddLiatent();
        //string url = "/switch/CabinetsSelect?id=" + AreaTipCon.instance.J_floorid;
    }

    private void Generatenamegrids(string thename, string id, bool J_IsLast)
    {
        //生成record的物体、  
        searchbg = Instantiate(gridnameshow, this.transform.position, Quaternion.identity) as GameObject;
        searchbg.name = "R";
        searchbg.transform.SetParent(J_Girl1.transform);
        searchbg.transform.localScale = new Vector3(1, 1, 1);
        searchbg.transform.Find("positontext").GetComponent<Text>().text = thename;
        searchbg.transform.Find("Text").GetComponent<Text>().text = id;

        //本grid长度加60  
        J_Girl1.GetComponent<RectTransform>().sizeDelta = new Vector2(J_Girl1.GetComponent<RectTransform>().sizeDelta.x,
            J_Girl1.GetComponent<RectTransform>().sizeDelta.y +
            J_Girl1.GetComponent<GridLayoutGroup>().cellSize.y +
            J_Girl1.GetComponent<GridLayoutGroup>().spacing.y);
        J_Girl1.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
        print("重新生成列表了");
    }

    private void Generatenamegrids1(string thename, string id, bool J_IsLast)
    {
        print(thename + "------" + id);
        //生成record的物体、  
        searchbg = Instantiate(gridnameshow, this.transform.position, Quaternion.identity) as GameObject;
        searchbg.name = "C";
        searchbg.transform.SetParent(J_Girl2.transform);
        searchbg.transform.localScale = new Vector3(1, 1, 1);
        searchbg.transform.Find("positontext").GetComponent<Text>().text = thename;
        searchbg.transform.Find("Text").GetComponent<Text>().text = id;

        //本grid长度加60  
        J_Girl2.GetComponent<RectTransform>().sizeDelta = new Vector2(J_Girl2.GetComponent<RectTransform>().sizeDelta.x,
            J_Girl2.GetComponent<RectTransform>().sizeDelta.y +
            J_Girl2.GetComponent<GridLayoutGroup>().cellSize.y +
            J_Girl2.GetComponent<GridLayoutGroup>().spacing.y);
        J_Girl2.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
    }

    private void Generatenamegrids2(string thename, string id, bool J_IsLast)
    {
        //生成record的物体、  
        searchbg = Instantiate(gridnameshow, this.transform.position, Quaternion.identity) as GameObject;
        searchbg.name = "E";
        searchbg.transform.SetParent(J_Girl3.transform);
        searchbg.transform.localScale = new Vector3(1, 1, 1);
        searchbg.transform.Find("positontext").GetComponent<Text>().text = thename;
        searchbg.transform.Find("Text").GetComponent<Text>().text = id;

        //本grid长度加60  
        J_Girl3.GetComponent<RectTransform>().sizeDelta = new Vector2(J_Girl3.GetComponent<RectTransform>().sizeDelta.x,
            J_Girl3.GetComponent<RectTransform>().sizeDelta.y +
            J_Girl3.GetComponent<GridLayoutGroup>().cellSize.y +
            J_Girl3.GetComponent<GridLayoutGroup>().spacing.y);
        J_Girl3.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
    }

    private void Update()
    {
        J_Girl1.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().direction = Scrollbar.Direction.BottomToTop;
        J_Girl2.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().direction = Scrollbar.Direction.BottomToTop;
        J_Girl3.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().direction = Scrollbar.Direction.BottomToTop;
    }

    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            case "RoomXB":
                print(btn.name + "-----RoomXB-----" + J_Panel.gameObject.name);
                if (J_Panel.activeInHierarchy)
                {
                    J_Panel.SetActive(false);
                }
                else
                {
                    J_Panel.SetActive(true);
                }
                J_Panel1.SetActive(false);
                J_Panel2.SetActive(false);
                J_Girl1.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
                J_Girl1.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().direction = Scrollbar.Direction.BottomToTop;
                Invoke("Change", 0.1f);
                AddLiatent();
                break;
            case "CabinetXB":
                print(btn.name + "-----CabinetXB-----");
                if (J_Panel1.activeInHierarchy)
                {
                    J_Panel1.SetActive(false);
                }
                else
                {
                    J_Panel1.SetActive(true);
                }
                J_Panel.SetActive(false);
                J_Panel2.SetActive(false);
                AddLiatent();
                J_Girl3.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
                J_Girl3.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().direction = Scrollbar.Direction.BottomToTop;
                Invoke("Change", 0.1f);
                break;
            case "EquipXB":
                print(btn.name + "-----EquipXB-----");
                if (J_Panel2.activeInHierarchy)
                {
                    J_Panel2.SetActive(false);
                }
                else
                {
                    J_Panel2.SetActive(true);
                }
                J_Panel.SetActive(false);
                J_Panel1.SetActive(false);
                AddLiatent();
                J_Girl2.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
                J_Girl2.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().direction = Scrollbar.Direction.BottomToTop;
                Invoke("Change", 0.1f);
                break;
            case "Go":
                if (transform.Find("Room/Text").GetComponent<Text>().text == "机房名称" || transform.Find("Cabinet/Text").GetComponent<Text>().text == "机柜名称" || transform.Find("Equip/Text").GetComponent<Text>().text == "设备名称")
                {
                    J_Tip.GetComponent<Text>().text = "请完善选择信息";
                    J_Tip.gameObject.SetActive(true);
                    StartCoroutine(WaitCloseTipPanel());
                }
                else
                {
                    btn.transform.parent.gameObject.SetActive(false);
                    DragImage.J_DragImage.J_IsFirst = false;
                    AreaTipCon.instance.J_IsChose = true;
                    AreaTipCon.instance.J_EquipName = J_equipname;
                    AreaTipCon.instance.J_CabinetName = J_cabinetname;

                    AreaTipCon.instance.J_floorstr = J_FloorStr;
                    AreaTipCon.instance.J_floorid = J_FloorID;
                    AreaTipCon.instance.J_Posx = J_Posx;
                    AreaTipCon.instance.J_Posy = J_Posy;
                    AreaTipCon.instance.J_EquipID = J_EquipId;

                    transform.root.GetComponent<CreatModelCon>().J_IsThree = false;

                    if (J_Roomtype == "3")
                    {
                        SceneManager.LoadScene("jifang01");
                    }
                    if (J_Roomtype == "2")
                    {
                        SceneManager.LoadScene("jifang02");
                    }
                    if (J_Roomtype == "1")
                    {
                        SceneManager.LoadScene("jifang03");
                    }
                    print("-------164------" + this.gameObject.name + "-----" + this.name);
                    // 清空grid里的所有东西  
                    List<Transform> lst = new List<Transform>();
                    foreach (Transform child in J_Girl1.transform)
                    {
                        lst.Add(child);
                        Debug.Log(child.gameObject.name);
                    }
                    for (int i = 0; i < lst.Count; i++)
                    {
                        Destroy(lst[i].gameObject);
                    }
                }
                break;
            case "R":
                btn.transform.parent.parent.gameObject.SetActive(false);
                transform.Find("Room/Text").GetComponent<Text>().text = btn.transform.Find("positontext").GetComponent<Text>().text;
                transform.Find("Cabinet/Text").GetComponent<Text>().text = "机柜名称";
                transform.Find("Equip/Text").GetComponent<Text>().text = "设备名称";
                for (int i = 0; i < switchroomname.Length; i++)
                {
                    if (btn.transform.Find("positontext").GetComponent<Text>().text == switchroomname[i] && btn.transform.Find("Text").GetComponent<Text>().text == id[i])
                    {
                        //AreaTipCon.instance.J_floorstr = btn.transform.Find("positontext").GetComponent<Text>().text;
                        //AreaTipCon.instance.J_floorid = id[i];
                        J_FloorStr = btn.transform.Find("positontext").GetComponent<Text>().text;
                        J_FloorID = id[i];
                        J_Roomtype = J_RoomType[i];
                        roomid = id[i];
                        J_Data = new Dictionary<string, string>();

                        J_Data.Add("id", roomid);
                        //J_Data.Add("userName", PlayerPrefs.GetString("账号"));
                        J_Data.Add("UserID", LoginCon.X_UserID);
                        J_Data.Add("RolesID", LoginCon.X_RolesID);
                        string url = "/APIS3d/device/api/web/cabinets/get-cabinets";
                        JsonContent.J_JsonContent.ContentJson_4(url, J_Data, "CabinetName", "ID", "Angle", "PosX", "PosY", "", "", "", "", "", "","");
                        Invoke("WaitContentCabient", 0.5f);
                    }
                }
                break;
            case "C":
                btn.transform.parent.parent.gameObject.SetActive(false);
                transform.Find("Cabinet/Text").GetComponent<Text>().text = btn.transform.Find("positontext").GetComponent<Text>().text;
                J_cabinetname = btn.transform.Find("positontext").GetComponent<Text>().text;
                transform.Find("Equip/Text").GetComponent<Text>().text = "设备名称";
                for (int i = 0; i < J_CabinetName.Length; i++)
                {
                    if (btn.transform.Find("positontext").GetComponent<Text>().text == J_CabinetName[i])
                    {
                        //AreaTipCon.instance.J_Posx = float.Parse(J_PosX[i]);
                        //AreaTipCon.instance.J_Posy = float.Parse(J_PosY[i]);
                        J_Posx = float.Parse(J_PosX[i]);
                        J_Posy = float.Parse(J_PosY[i]);

                        Debug.Log(J_PosX[i] + "------传入机柜位置了------316------" + J_PosY[i]);
                        cabinetid = J_CabinetID[i];
                        string url = "/APIS3d/device/api/web/equipments/get-equipments?id=" + cabinetid;
                        //string url = "/switch/equipMentAll?id=1";//数据库内数据不完善，测试用
                        JsonContent.J_JsonContent.ContentJson_1(url, "EquipName", "ID", "", "", "", "");
                        Invoke("WaitContentEquip", 0.5f);
                    }
                }
                break;
            case "E":
                btn.transform.parent.parent.gameObject.SetActive(false);
                transform.Find("Equip/Text").GetComponent<Text>().text = btn.transform.Find("positontext").GetComponent<Text>().text;
                J_equipname = btn.transform.Find("positontext").GetComponent<Text>().text;
                for (int i = 0; i < J_EquipName.Length; i++)
                {
                    if (J_EquipName[i] == J_equipname)
                    {
                        //AreaTipCon.instance.J_EquipID = J_EquipID[i];
                        J_EquipId = J_EquipID[i];
                    }
                }
                break;
        }
    }

    void Change()
    {
        J_Girl1.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
        J_Girl2.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
        J_Girl3.transform.parent.Find("Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
    }

    //机柜数据
    void WaitContentCabient()
    {
        J_CabinetName = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_CabinetID = JsonContent.J_JsonContent.ReturnJsonData_2();
        J_Angle = JsonContent.J_JsonContent.ReturnJsonData_3();
        J_PosX = JsonContent.J_JsonContent.ReturnJsonData_4();
        J_PosY = JsonContent.J_JsonContent.ReturnJsonData_5();
        // 清空grid里的所有东西  
        List<Transform> lst = new List<Transform>();
        foreach (Transform child in J_Girl2.transform)
        {
            lst.Add(child);
            Debug.Log(child.gameObject.name);
        }
        for (int i = 0; i < lst.Count; i++)
        {
            Destroy(lst[i].gameObject);
        }
        for (int i = 0; i < J_CabinetName.Length; i++)
        {
            if (i == 0)
            {
                Generatenamegrids1(J_CabinetName[i], J_CabinetID[i], true);//生成列表   
            }
            else
            {
                Generatenamegrids1(J_CabinetName[i], J_CabinetID[i], false);//生成列表   
            }
            //此处生成按钮等== SeaField.text
            print(J_CabinetName[i] + "----J_CabinetName[i]----" + J_CabinetName.Length);
        }
        AddLiatent();
    }

    //设备数据
    void WaitContentEquip()
    {
        J_EquipName = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_EquipID = JsonContent.J_JsonContent.ReturnJsonData_2();
        // 清空grid里的所有东西  
        List<Transform> lst = new List<Transform>();
        foreach (Transform child in J_Girl3.transform)
        {
            lst.Add(child);
            Debug.Log(child.gameObject.name);
        }
        for (int i = 0; i < lst.Count; i++)
        {
            Destroy(lst[i].gameObject);
        }
        for (int i = 0; i < J_EquipName.Length; i++)
        {
            if (i == 0)
            {
                Generatenamegrids2(J_EquipName[i], "", true);//生成列表   
            }
            else
            {
                Generatenamegrids2(J_EquipName[i], "", false);//生成列表   
            }
            //此处生成按钮等== SeaField.text
            print(J_EquipName[i] + "----switchroomname[i]----");
        }
        AddLiatent();
    }

    IEnumerator WaitCloseTipPanel()
    {
        yield return new WaitForSeconds(1);
        J_Tip.SetActive(false);
    }
}
