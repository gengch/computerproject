﻿using HighlightingSystem;
using UnityEngine;


public class HighlightObject : MonoBehaviour
{
    protected Highlighter h;
    public Camera J_camera;
    public enum HighlightType
    {
        Once,//变化一次
        Constant,//持续
        Flash//闪烁
    }

    public Color highlightColor = Color.red;

    void Awake()
    {
        h = gameObject.AddComponent<Highlighter>();
        J_camera = MyCamera.Instance.GetComponent<Camera>();// GameObject.Find("(Singleton)RTEditor.RuntimeEditorApplication/(Singleton)RTEditor.EditorCamera").GetComponent<Camera>();
        //J_camera = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();
        //J_camera = GameObject.Find("renwu/PlayerCamera").GetComponent<Camera>();
        if (J_camera.GetComponent<HighlightingRenderer>() == null)
        {
            J_camera.gameObject.AddComponent<HighlightingRenderer>();
        }
    }

    private void Start()
    {
        //J_camera = GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();
        ////J_camera = GameObject.Find("renwu/PlayerCamera").GetComponent<Camera>();
        //if (J_camera.GetComponent<HighlightingRenderer>() == null)
        //{
        //    J_camera.gameObject.AddComponent<HighlightingRenderer>();
        //}
    }

    /// <summary>
    /// API:修改物体是否开启高亮
    /// </summary>
    /// <param name="showHightlight">是否开启高亮</param>
    public void SwithHightlight(bool showHightlight)
    {
        SwithHightlight(showHightlight, HighlightType.Constant);
    }

    /// <summary>
    /// API:修改物体是否开启高亮
    /// </summary>
    /// <param name="showHightlight">是否开启高亮</param>
    /// <param name="type">高亮类型</param>
    public void SwithHightlight(bool showHightlight, HighlightType type)
    {
        switch (type)
        {
            case HighlightType.Once:
                if (showHightlight)
                    h.ConstantOnImmediate(highlightColor);
                else
                    h.ConstantOffImmediate();
                break;

            case HighlightType.Constant:
                if (showHightlight)
                    h.ConstantOn(highlightColor);
                else
                    h.ConstantOff();
                break;

            case HighlightType.Flash:
                if (showHightlight)
                    h.FlashingOn(highlightColor, new Color(0, 0, 0, 0));
                else
                    h.FlashingOff();
                break;
        }
    }
}
