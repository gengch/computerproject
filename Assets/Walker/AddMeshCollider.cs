﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMeshCollider : MonoBehaviour
{
    [ContextMenu("AddCollider")]
    private void Test()
    {
        MeshRenderer[] meshRenderers = gameObject.GetComponentsInChildren<MeshRenderer>();

        foreach(MeshRenderer meshRenderer in meshRenderers)
        {
            GameObject go = meshRenderer.gameObject;
            Collider[] colliders = go.GetComponents<Collider>();
            if (colliders.Length > 1)
            {
                MeshCollider meshCollider = go.GetComponent<MeshCollider>();
                DestroyImmediate(meshCollider);
            }
        }
    }
}
