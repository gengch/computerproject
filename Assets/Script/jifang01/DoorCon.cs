﻿using UnityEngine;

public class DoorCon : MonoBehaviour
{
    private RaycastHit J_hit;
    private Animator J_animator;
    private Camera J_camera;

    private bool J_isopen;

    void Start()
    {
        J_animator = GetComponent<Animator>();
        J_camera = MyCamera.Instance.GetComponent<Camera>();// GameObject.Find("(Singleton)RTEditor.RuntimeEditorApplication/(Singleton)RTEditor.EditorCamera").GetComponent<Camera>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(2))
        {
            Ray J_ray = J_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(J_ray, out J_hit, 1000, DataMgr.Instance.SelectedLayerMask.value))
            {
                Debug.LogError("hit : " + J_hit.collider.gameObject.name, J_hit.collider.gameObject);
                if (J_hit.collider.gameObject.tag == "Door")
                {
                    int index = int.Parse(J_hit.collider.gameObject.name.Substring(4, 1));
                    if (J_isopen)
                    {
                        if (index == 1 || index == 2)
                        {
                            CloseDoor(2);
                            J_isopen = false;
                        }
                        else if (index == 3 || index == 4)
                        {
                            CloseDoor(4);
                            J_isopen = false;
                        }
                        else if (index == 5 || index == 6)
                        {
                            CloseDoor(6);
                            J_isopen = false;
                        }
                    }
                    else
                    {
                        if (index == 1 || index == 2)
                        {
                            OpenDoor(1);
                            J_isopen = true;
                        }
                        else if (index == 3 || index == 4)
                        {
                            OpenDoor(3);
                            J_isopen = true;
                        }
                        else if (index == 5 || index == 6)
                        {
                            OpenDoor(5);
                            J_isopen = true;
                        }
                    }
                }
            }
        }
    }

    //开门
    void OpenDoor(int index)
    {
        J_animator.SetInteger("MenCon", index);
    }

    //关门
    void CloseDoor(int index)
    {
        J_animator.SetInteger("MenCon", index);
    }
}
