﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/// <summary>
/// 机柜搜索脚本
/// </summary>
public class SearchCabinet : MonoBehaviour
{
    private string[] switchroomname;//机房名称
    private string[] roomtype;//机房类型
    private string[] id;//机房id
    private string[] floorareaid;//楼层id
    private string[] posx;//机房id
    private string[] posy;//楼层id

    private GameObject gridnameshow;

    public Button searchbutton;
    private string inputtext = "";
    private GameObject searchbg;//生成的每一行的显示物体  

    public GameObject J_camera;
    public GameObject J_player;
    public GameObject J_scrollbar;

    private Text J_NumText;//搜索个数

    private int J_index;

    private Dictionary<string, string> J_Data;

    public GameObject J_rect;
    public GameObject J_Girl;

    private void Start()
    {
        J_NumText = transform.Find("SearchPanel/NumText").GetComponent<Text>();
        J_NumText.gameObject.SetActive(false);

        string gridpath = "findnamegridcontent";//生成列表的路径  
        gridnameshow = Resources.Load(gridpath, typeof(GameObject)) as GameObject;//加载生成的子物体  

        //初始化查找按钮  
        searchbutton.onClick.AddListener(Findbutton);
        print(searchbutton.gameObject.activeInHierarchy + "-----------------");
    }

    /// <summary>  
    /// 查找方法触发  
    /// </summary>  
    public void Findbutton()
    {
        J_rect.SetActive(true);
        J_scrollbar.SetActive(true);

        J_scrollbar.GetComponent<Scrollbar>().value = 0;
        Debug.Log("SerachCabinet----FindButton");
        inputtext = GameObject.Find("Canvas").transform.Find("SaidPanel/SeaField/Text").GetComponent<Text>().text;
        //数据库完成后，添加需要的字段名
        J_Data = new Dictionary<string, string>();
        J_Data.Add("keyword", inputtext);

        JsonContent.J_JsonContent.ContentJson_4("/APIS3d/device/api/web/cabinets/searche-cabinets", J_Data, "CabinetName", "", "SwitchRoomID", "PosX", "PosY","","","","","","","");
        Invoke("WaitData", 0.5f);

        //Grid的长度随着生成物体个数变化  
        J_Girl.GetComponent<RectTransform>().sizeDelta = new Vector2(this.gameObject.GetComponent<RectTransform>().sizeDelta.x, 0);


        // 清空grid里的所有东西  
        List<Transform> lst = new List<Transform>();
        foreach (Transform child in J_Girl.transform)
        {
            lst.Add(child);
            Debug.Log(child.gameObject.name);
        }
        for (int i = 0; i < lst.Count; i++)
        {
            Destroy(lst[i].gameObject);
        }

        Invoke("WaitAddListener", 0.5f);
    }

    void WaitAddListener()
    {
        //对新生成的按钮添加监听
        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }


    /// <summary>  
    /// 将查找文字与库里的数据对比，然后生成列表  
    /// </summary>  
    void WaitData()
    {
        J_index = 0;//每次查询之前进行清零处理。
        switchroomname = JsonContent.J_JsonContent.ReturnJsonData_1();
        //roomtype = JsonContent.J_JsonContent.ReturnJsonData_2();
        id = JsonContent.J_JsonContent.ReturnJsonData_3();
        posx = JsonContent.J_JsonContent.ReturnJsonData_4();
        posy = JsonContent.J_JsonContent.ReturnJsonData_5();


        for (int i = 0; i < switchroomname.Length; i++)
        {
            if (inputtext != "" && switchroomname[i].Contains(inputtext))
            {
                Generatenamegrids(switchroomname[i], id[i], "机柜");//生成列表  
                J_index++;
            }
            if (inputtext == "")
            {
                Generatenamegrids(switchroomname[i], id[i], "机柜");//生成列表  
                J_index++;
            }
        }
        Debug.LogWarning("索引个数" + J_index.ToString());
        J_NumText.text = J_index.ToString();
        J_NumText.gameObject.SetActive(true);
        Invoke("Change", 0.5f);
    }

    /// <summary>
    /// 对新生成的按钮添加监听
    /// </summary>
    /// <param name="btn"></param>
    private void OnClickBtn(Button btn)
    {
        if (btn.name == "CB")
        {
            btn.transform.parent.gameObject.SetActive(false);
            J_scrollbar.SetActive(false);
        }

        if (btn.name == "CheckBtn")
        {
            GameObject.Find("Canvas").transform.Find("SaidPanel/SeaField").GetComponent<InputField>().text = "";
            //J_Girl.transform.parent.parent.gameObject.SetActive(false);
            //J_scrollbar.SetActive(false);
            if (btn.transform.parent.Find("positontext").GetComponent<Text>().text != SceneManager.GetActiveScene().name)
            {
                for (int i = 0; i < switchroomname.Length; i++)
                {
                    if (btn.transform.parent.Find("positontext").GetComponent<Text>().text == switchroomname[i] && btn.transform.parent.Find("Text").GetComponent<Text>().text == id[i])
                    {
                        print(posx.Length + "------" + i);
                        print(posy.Length + "-----");
                        AreaTipCon.instance.J_Posx = float.Parse(posx[i]);
                        AreaTipCon.instance.J_Posy = float.Parse(posy[i]);

                        //AreaTipCon.instance.J_floorstr = switchroomname[i];
                        AreaTipCon.instance.J_floorid = id[i];
                        SceneManager.LoadScene("jifang01");

                        //根据机房类型选择机房型号
                        //切换之后给出搜索内筒的坐标，人物根据坐标切换初始位置
                        //if (roomtype[i] == "机房")
                        //{
                        //    AreaTipCon.instance.J_floorstr = switchroomname[i];
                        //    AreaTipCon.instance.J_floorid = id[i];
                        //    SceneManager.LoadScene("jifang01");
                        //}
                        //if (roomtype[i] == "配电间")
                        //{
                        //    AreaTipCon.instance.J_floorstr = switchroomname[i];
                        //    AreaTipCon.instance.J_floorid = id[i];
                        //    SceneManager.LoadScene("jifang02");
                        //}
                        //if (roomtype[i] == "电间")
                        //{
                        //    AreaTipCon.instance.J_floorstr = switchroomname[i];
                        //    AreaTipCon.instance.J_floorid = id[i];
                        //    SceneManager.LoadScene("jifang03");
                        //}
                    }
                }
            }

            // 清空grid里的所有东西  
            List<Transform> lst = new List<Transform>();
            foreach (Transform child in J_Girl.transform)
            {
                lst.Add(child);
                Debug.Log(child.gameObject.name);
            }
            for (int i = 0; i < lst.Count; i++)
            {
                Destroy(lst[i].gameObject);
            }

            J_scrollbar.SetActive(false);
            J_Girl.transform.parent.parent.gameObject.SetActive(false);
        }
    }


    /// <summary>  
    /// 生成整个gird子物体  
    /// </summary>  
    private void Generatenamegrids(string thename, string id, string typeName)
    {
        //生成record的物体、  
        searchbg = Instantiate(gridnameshow, this.transform.position, Quaternion.identity) as GameObject;
        searchbg.transform.SetParent(J_Girl.transform);
        searchbg.transform.localScale = new Vector3(1, 1, 1);
        searchbg.transform.Find("positontext").GetComponent<Text>().text = thename;
        searchbg.transform.Find("Text").GetComponent<Text>().text = id;
        searchbg.transform.Find("TypeText").GetComponent<Text>().text = typeName;

        //本grid长度加60  
        J_Girl.GetComponent<RectTransform>().sizeDelta = new Vector2(J_Girl.GetComponent<RectTransform>().sizeDelta.x,
            J_Girl.GetComponent<RectTransform>().sizeDelta.y +
            J_Girl.GetComponent<GridLayoutGroup>().cellSize.y +
            J_Girl.GetComponent<GridLayoutGroup>().spacing.y);
        J_scrollbar.GetComponent<Scrollbar>().value = 1;
    }

    void Change()
    {
        J_scrollbar.GetComponent<Scrollbar>().value = 1;
    }
}
