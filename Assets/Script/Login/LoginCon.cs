﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;

public class LoginCon : MonoBehaviour
{
    private string J_CUrl ;
    private bool J_isRemember;
    private Toggle J_Toggle;
    private InputField J_AccountField;
    private InputField J_PassWordField;
    private GameObject J_Image;
    private float J_RotateSpeed = 50;
    private string J_Str;

    public static string  X_UserID;

    public static string X_RolesID;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("连接端口信息"))
        {
            PlayerPrefs.DeleteKey("连接端口信息");
        }

        if (PlayerPrefs.HasKey("线缆位置X"))
        {
            PlayerPrefs.DeleteKey("线缆位置X");
            PlayerPrefs.DeleteKey("线缆位置Y");
            PlayerPrefs.DeleteKey("线缆位置Z");
            PlayerPrefs.DeleteKey("旋转位置X");
            PlayerPrefs.DeleteKey("旋转位置Y");
            PlayerPrefs.DeleteKey("旋转位置Z");
        }

        if (PlayerPrefs.HasKey("连接端口信息"))
        {
            PlayerPrefs.DeleteKey("连接端口信息");
        }

        J_CUrl = GameConst.ServerUrl;//  File.ReadAllText("D://Url.txt");
    }

    void Start()
    {
        J_Image = transform.Find("Image").gameObject;
        J_AccountField = transform.Find("AccountField").GetComponent<InputField>();
        J_PassWordField = transform.Find("PassWordField").GetComponent<InputField>();
        if (PlayerPrefs.HasKey("Account"))
        {
            J_AccountField.text = PlayerPrefs.GetString("Account");
        }
        if (PlayerPrefs.HasKey("PassWord"))
        {
            J_PassWordField.text = PlayerPrefs.GetString("PassWord");
        }
        J_Toggle = transform.Find("Toggle").GetComponent<Toggle>();
        if (PlayerPrefs.HasKey("Remenber"))
        {
            if (PlayerPrefs.GetString("Remenber") == "true")
            {
                J_Toggle.isOn = true;
                J_isRemember = true;
            }
        }
        J_Toggle.onValueChanged.AddListener((bool value) => OnToggleClick(value));
        transform.Find("LoginBtn").GetComponent<Button>().onClick.AddListener(delegate () { OnClickBtn(); });
    }

    void Update()
    {
        J_Image.transform.Rotate(Vector3.forward * Time.deltaTime * J_RotateSpeed);
    }

    void OnToggleClick(bool value)
    {
        if (value)
        {
            J_isRemember = true;
        }
        else
        {
            J_isRemember = false;
        }
    }

    void OnClickBtn()
    {
        //此处先进行连接数据库
        string url = J_CUrl + "/APIS3d/device/api/web/login/login?UserName=" + J_AccountField.text + "&PassWord=" + J_PassWordField.text;
        //JsonContent.J_JsonContent.ContentJson_1(url, "登录成功", "", "");
        //
        //SceneManager.LoadScene("ProjectScene");
        Debug.Log(url);
        StartCoroutine(WaitForJsonData_1(url,"ID", "RolesID"));
    }
    IEnumerator WaitForJsonData_1(string url,string node_1,string node_2)
    {
        WWW www = new WWW(url);
        yield return www;
        Debug.Log(www.text);
        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        J_Str = jo["code"].ToString();
        //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        X_UserID= jo["data"][node_1].ToString();
        X_RolesID = jo["data"][node_2].ToString();
        print(jo["code"].ToString());
        StartCoroutine(WaitForLogin());
        print("---------------------");
        //Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["code"].ToString());
        //print(jar.ToString());
    }
    IEnumerator WaitForLogin()
    {
        yield return new WaitForSeconds(0.5f);
        //进行比较之后，若账号密码正确则保存密码和登录
        //保存账号密码之前删除以前的全部记录

        if (J_Str == "0")
        {
            //PlayerPrefs.DeleteAll();
            if (J_isRemember)
            {
                PlayerPrefs.SetString("Account", J_AccountField.text);
                PlayerPrefs.SetString("PassWord", J_PassWordField.text);
                PlayerPrefs.SetString("Remenber", "true");
            }
            PlayerPrefs.SetString("账号", J_AccountField.text);
            SceneManager.LoadScene("ProjectScene");
        }
    }
}
