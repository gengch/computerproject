﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ImageCom : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private GameObject J_Image;

    void Start()
    {
        J_Image = transform.Find("Image").gameObject;
        J_Image.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        J_Image.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        J_Image.SetActive(false);
    }
}
