﻿using UnityEngine;

public class CombineMeshe : MonoBehaviour
{
    //注：this.gameObject必须有MeshFilter和MeshRender组件
    void Start()
    {
        if (GetComponent<MeshFilter>()==null|| GetComponent<MeshRenderer>() == null)
        {
            print(this.gameObject.name+"缺少MeshFilter组件或者缺少MeshRender组件，不能进行网格合并！！！");
            return;
        }
        //获取自身和所有子物体上的MeshFilter组件
        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        //生成数组
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];

        //获取自身和所有子物体中所有MeshRenderer组件
        MeshRenderer[] meshRenderer = GetComponentsInChildren<MeshRenderer>();
        //生成材质球数组
        Material[] mats = new Material[meshRenderer.Length];                    

        //遍历数组
        for (int i = 0; i < meshFilters.Length; i++)
        {
            //获取材质球列表
            mats[i] = meshRenderer[i].sharedMaterial;                           

            combine[i].mesh = meshFilters[i].sharedMesh;
            //矩阵(Matrix)自身空间坐标的点转换成世界空间坐标的点
            combine[i].transform = transform.worldToLocalMatrix * meshFilters[i].transform.localToWorldMatrix;

            //销毁除this.gameObject之外的所有物体
            if (meshFilters[i].gameObject.name != gameObject.name)
            {
                Destroy(meshFilters[i].gameObject);
            }
        }
        //为新的整体新建一个mesh 
        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        //合并Mesh. 第二个false参数, 表示并不合并为一个网格, 而是一个子网格列表 
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine, false);
        //为合并后的新Mesh指定材质 
        transform.GetComponent<MeshRenderer>().sharedMaterials = mats;         

        transform.gameObject.SetActive(true);
    }
}
