﻿using UnityEngine;

public class AreaTipCon : MonoBehaviour
{
    [HideInInspector]
    public string X_UserID;
    [HideInInspector]
    public string J_FloorId;//选择的楼层ID
    [HideInInspector]
    public GameObject X_Building;//选择的建筑
    [HideInInspector]
    public int X_BuindID;//选择的建筑ID
    [HideInInspector]
    public string X_SwitchRoomID;//所选房间类型
    [HideInInspector]
    public string X_RoomType;
    [HideInInspector]
    public string J_prostr;//选择的项目名称
    [HideInInspector]
    public string J_proNum;//选择的项目ID
    [HideInInspector]
    public string J_areastr;//选择的区域名称
    [HideInInspector]
    public string J_floorstr;//选择的机房名称
    [HideInInspector]
    public string J_floorid;//选择的机房id
    [HideInInspector]
    public string J_tierstr;//选择的楼层层数
    [HideInInspector]
    public string J_Show;//需要显示的物体名称
    [HideInInspector]
    public string J_AreaId;//选择的区域ID
    [HideInInspector]
    public Vector3 StartPos;//开始时人物坐标
    [HideInInspector]
    public Quaternion StartRot;//开始时人物旋转角度
    public GameObject J_player;//任务
    public static AreaTipCon instance;
    [HideInInspector]
    public string J_ImageUrl;//选择的项目对应的图片
    [HideInInspector]
    public float J_Posx;//机柜的x坐标
    [HideInInspector]
    public float J_Posy;//机柜的y坐标
    [HideInInspector]
    public float J_Angle;//机柜的旋转坐标
    [HideInInspector]
    public string J_cabinetid;//选择的机柜id
    [HideInInspector]
    public string J_StartPortCabinetId;//起始机柜
    [HideInInspector]
    public string J_EndPortCabinetId;//目标机柜
    [HideInInspector]
    public string J_StartPortID;//起始设备端口
    [HideInInspector]
    public string J_EndPortID;//目标设备端口
    [HideInInspector]
    public string J_EndProtType;
    [HideInInspector]
    public string J_CabelID;//线缆id
    [HideInInspector]
    public bool J_IsFirst = true;//是否为第一次连接
    [HideInInspector]
    public string userName;//用户名
    [HideInInspector]
    public bool J_IsChose;//是否开始选择
    [HideInInspector]
    public string J_EquipName;//选中的设备名称
    [HideInInspector]
    public string J_CabinetName;//选中的机柜名称
    [HideInInspector]
    public GameObject J_port;//选中的端口
    [HideInInspector]
    public bool J_IsFloor;//是否显示楼层
    [HideInInspector]
    public string J_FloorUrl;
    [HideInInspector]
    public int J_Floor;
    [HideInInspector]
    public string J_FimagenUrl;
    [HideInInspector]
    public string J_EquipID;
    [HideInInspector]
    public string J_CabinetNum;
    [HideInInspector]
    public string[] J_CableInfo;//线缆类型
    [HideInInspector]
    public string[] J_CableId;
    [HideInInspector]
    public bool J_IsStart;
    [HideInInspector]
    public string[] J_CableType;
    [HideInInspector]
    public string J_FloorID;

    void Awake()
    {
        StartPos = J_player.transform.position;
        StartRot = J_player.transform.rotation;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
