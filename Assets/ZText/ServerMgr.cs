﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerMgr : MonoBehaviour
{

    #region 委托定义
    /// <summary>
    /// 向服务器请求资源回调
    /// </summary>
    /// <param name="wwwObj">返回的www对象</param>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    public delegate void GetCompleteDelegate(WWW wwwObj, object param01, object param02);
    /// <summary>
    /// 想服务器请求资源失败回调
    /// </summary>
    /// <param name="wwwObj">返回的www对象</param>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    public delegate void GetErrorDelegate(WWW wwwObj, object param01, object param02);
    /// <summary>
    /// 向服务器提交数据回调
    /// </summary>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    public delegate void PostCompleteDelegate(object param01, object param02);
    /// <summary>
    /// 向服务器提交数据失败回调
    /// </summary>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    public delegate void PostErrorDelegate(object param01, object param02);
    #endregion

    #region 单例
    private static ServerMgr _instance;
    public static ServerMgr Instance
    {
        get { return _instance; }
    }
    #endregion

    #region 脚本声明周期
    private void Awake()
    {
        _instance = this;
    }
    #endregion

    #region 向服务器请求数据
    /// <summary>
    /// 向服务器请求数据
    /// </summary>
    /// <param name="url">请求地址</param>
    /// <param name="post">请求参数</param>
    /// <param name="callBack">请求成功的回调函数</param>
    /// <param name="errorCallBack">请求失败的回调函数</param>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    public void GetDataFromServer(string url, Dictionary<string, object> post, GetCompleteDelegate callBack, GetErrorDelegate errorCallBack = null, object param01 = null, object param02 = null)
    {
        StartCoroutine(LinkServer(url, post, callBack, errorCallBack, param01, param02));
    }
    /// <summary>
    /// 向服务器请求数据
    /// </summary>
    /// <param name="url">请求地址</param>
    /// <param name="post">请求参数</param>
    /// <param name="callBack">请求成功回调函数</param>
    /// <param name="errorCallBack">请求失败回调函数</param>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    /// <returns></returns>
    private IEnumerator LinkServer(string url, Dictionary<string, object> post, GetCompleteDelegate callBack = null, GetErrorDelegate errorCallBack = null, object param01 = null, object param02 = null)
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, object> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            if (callBack != null)
                ((GetCompleteDelegate)callBack).Invoke(www, param01, param02);
        }
        else
        {
            if (errorCallBack != null)
                ((GetErrorDelegate)errorCallBack).Invoke(www, param01, param02);
            Debug.LogError("请求数据 ：" + url + "出错!");
        }
    }
    #endregion

    #region 保存数据到服务器
    /// <summary>
    /// 保存数据到服务器
    /// </summary>
    /// <param name="url">保存的地址</param>
    /// <param name="post">请求数据列表</param>
    /// <param name="callBack">请求成功回调</param>
    /// <param name="errorCallBack">请求失败回调</param>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    public void SaveDataToServer(string url, Dictionary<string, object> post, PostCompleteDelegate callBack, PostErrorDelegate errorCallBack, object param01 = null, object param02 = null)
    {
        StartCoroutine(PostDataToServer(url, post, callBack, errorCallBack, param01, param02));
    }

    /// <summary>
    /// 保存数据到服务器
    /// </summary>
    /// <param name="url">保存的地址</param>
    /// <param name="post">请求数据列表</param>
    /// <param name="callBack">请求成功回调</param>
    /// <param name="errorCallBack">请求失败回调</param>
    /// <param name="param01">自定义参数01</param>
    /// <param name="param02">自定义参数02</param>
    /// <returns></returns>
    private IEnumerator PostDataToServer(string url, Dictionary<string, object> post, PostCompleteDelegate callBack, PostErrorDelegate errorCallBack, object param01 = null, object param02 = null)
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, object> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);
        yield return www;
        if (string.IsNullOrEmpty(www.error)) //没有错误
        {
            if (callBack != null)
                ((PostCompleteDelegate)callBack).Invoke(param01, param02);
        }
        else //有错误
        {
            if (errorCallBack != null)
                ((PostErrorDelegate)errorCallBack).Invoke(param01, param02);
        }
    }
    #endregion
}
