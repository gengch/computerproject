﻿using UnityEngine;
using UnityEngine.UI;

public class CameraMoveCon : MonoBehaviour
{
    void Start()
    {
        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }

    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            case "FDBtn":
                transform.Translate(Vector3.forward * 0.5f);
                break;
            case "SXBtn":
                transform.Translate(Vector3.back * 0.5f);
                break;
            case "YXBtn":
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y - 15, transform.eulerAngles.z);
                break;
            case "ZXBtn":
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + 15, transform.eulerAngles.z);
                break;
        }
    }
    void LateUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    transform.Translate(Vector3.forward * 0.5f);
        //}
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    transform.Translate(Vector3.back * 0.5f);
        //}
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + 15, transform.eulerAngles.z);
        //}
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y - 15, transform.eulerAngles.z);
        //}
    }
}
