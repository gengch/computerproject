﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CableCon : MonoBehaviour
{
    private string J_CUrl = "http://3dmis.zcwjvr.com/";
    [HideInInspector]
    public string J_EquipID;//当前设备ID

    private string[] J_portid;//端口id
    private string[] J_portstatus;//端口状态
    private string[] J_porttype;//端口类型
    private int J_Index;//名称
    private GameObject J_CableObj;//网线预制件1
    private GameObject J_CableObj1;//网线预制件2
    private GameObject J_Port;//端口
    private GameObject J_Cable;//新生成的网线
    private GameObject J_PortId;//端口ID
    private GameObject J_Canvas;
    private GameObject J_Tip;
    private bool J_IsContinue;
    private string J_Name;

    [HideInInspector]
    public bool J_IsUpdate;
    public static CableCon J_cable;

    private void Awake()
    {
        J_CUrl = GameConst.ServerUrl;// File.ReadAllText("D://Url.txt");
    }

    void Start()
    {
        J_Name = this.gameObject.name;
        J_cable = this;
        J_Canvas = GameObject.Find("Canvas");
        J_Tip = J_Canvas.transform.Find("Tip").gameObject;
        J_CableObj = Resources.Load<GameObject>("xian");
        J_CableObj1 = Resources.Load<GameObject>("xian 1");

    }

    private void OnEnable()
    {
        Invoke("ContentUrl", 0.2f);
    }

    void ContentUrl()
    {
        if (transform.Find("SB") != null)
        {
            string url1 = J_CUrl + "/APIS3d/device/api/web/ports/get-ports?id=" + J_EquipID;
            Debug.Log(url1 + "--------57--------");
            StartCoroutine(WaitGetCableInfo(url1, "ID", "portstatus", "portType"));
            J_IsContinue = true;
        }
    }

    void Update()
    {
        if (this.J_IsUpdate /*&& J_IsUpdate*/)
        {
            this.J_IsUpdate = false;
            string url1 = J_CUrl + "/APIS3d/device/api/web/ports/get-ports?id=" + J_EquipID;
            StartCoroutine(WaitGetCableInfo1(url1, "ID", "portstatus", "portType"));
        }
    }

    /// <summary>
    /// 断开连接，删除设备时刷新数据库
    /// </summary>
    public void OffCable()
    {
        if (this.gameObject.activeInHierarchy)
        {
            string url1 = J_CUrl + "/APIS3d/device/api/web/ports/get-ports?id=" + J_EquipID;
            StartCoroutine(WaitGetCableInfo1(url1, "ID", "portstatus", "portType"));
        }
    }

    IEnumerator WaitGetCableInfo(string url, string node_1, string node_2, string node_3)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print("线缆信息：   "+jar.ToString());
        J_portid = new string[jar.Count];

        for (int i = 0; i < jar.Count; i++)
        {
            J_portid[i] = jar[i][node_1].ToString();
            //print(J_EquiName[i] + node_1 + "---node_1---");
        }


        J_portstatus = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_portstatus[i] = jar[i][node_2].ToString();
            //print(J_EquiID[i] + node_2 + "---node_2---" + "----url     " + url);
        }


        J_porttype = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_porttype[i] = jar[i][node_3].ToString();
            Debug.Log(J_porttype[i] + "-----104-----" + jar.Contains(node_3));
        }
        //if (AreaTipCon.instance.J_CableType == null)
        //{
        //    AreaTipCon.instance.J_CableType = new string[jar.Count];
        //    for (int i = 0; i < jar.Count; i++)
        //    {
        //        AreaTipCon.instance.J_CableType[i] = jar[i][node_3].ToString();
        //        Debug.Log(AreaTipCon.instance.J_CableType[i] + "-----104-----" + jar.Contains(node_3));
        //    }
        //}


        for (int i = 0; i < J_portid.Length; i++)
        {
            if (J_portstatus[i] == "已连接")
            {
                J_Index = i + 1;

                J_Port = transform.Find("SB/" + J_Index).gameObject;
                //判定是否已经有线缆，若没有线缆则进行生成
                if (J_Port.transform.childCount == 0)
                {
                    Debug.Log(AreaTipCon.instance.J_CableInfo[0] + "-----117-----" + AreaTipCon.instance.J_CableInfo[1] + "-----" + J_porttype);
                    if (J_porttype[i] == AreaTipCon.instance.J_CableInfo[0])
                    {
                        J_Cable = Instantiate(J_CableObj);
                        J_Cable.transform.position = J_Port.transform.position;
                        J_Cable.transform.SetParent(J_Port.transform);
                    }
                    if (J_porttype[i] == AreaTipCon.instance.J_CableInfo[1])
                    {
                        J_Cable = Instantiate(J_CableObj1);
                        J_Cable.transform.position = J_Port.transform.position;
                        J_Cable.transform.SetParent(J_Port.transform);
                    }
                }
            }
        }
    }

    public void DeleCableInfo(int index)
    {
        string url = "/APIS3d/device/api/web/portslink/del-portslink?id=" + J_portid[index];
        print(url);
        JsonContent.J_JsonContent.SaveDataToServer_1(url);
        J_Tip.SetActive(true);
        J_Tip.GetComponent<Text>().text = "断开连接成功";
        StartCoroutine(WaitCloseTip());
    }

    public void PortPath(int index)
    {
        print(J_portid[index] + "----静待端口全链路的形成----");
    }

    IEnumerator WaitGetCableInfo1(string url, string node_1, string node_2, string node_3)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());
        J_portid = new string[jar.Count];

        for (int i = 0; i < jar.Count; i++)
        {
            J_portid[i] = jar[i][node_1].ToString();
            //print(J_EquiName[i] + node_1 + "---node_1---");
        }

        if (node_2 != "")
        {
            J_portstatus = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_portstatus[i] = jar[i][node_2].ToString();
                //print(J_EquiID[i] + node_2 + "---node_2---" + "----url     " + url);
            }
        }

        if (node_3 != "")
        {
            J_porttype = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_porttype[i] = jar[i][node_3].ToString();
                //print(J_CabinetId[i] + node_2 + "---node_2---" + "----url     " + url);
            }
        }

        for (int i = 0; i < J_portid.Length; i++)
        {
            if (J_portstatus[i] == "已连接")
            {
                J_Index = i + 1;

                J_Port = transform.Find("SB/" + J_Index).gameObject;
                //判定是否已经有线缆，若没有线缆则进行生成
                if (J_Port.transform.childCount == 0)
                {
                    Debug.Log(J_Port.name + "------");
                    Debug.Log(AreaTipCon.instance.J_CableInfo[0] + "-----215-----" + AreaTipCon.instance.J_CableInfo[1] + "-----" + J_porttype[i] + "----" + J_portid[i]);
                    if (J_porttype[i] == AreaTipCon.instance.J_CableInfo[0])
                    {
                        J_Cable = Instantiate(J_CableObj);
                        J_Cable.transform.position = J_Port.transform.position;
                        J_Cable.transform.SetParent(J_Port.transform);
                    }
                    if (J_porttype[i] == AreaTipCon.instance.J_CableInfo[1])
                    {
                        J_Cable = Instantiate(J_CableObj1);
                        J_Cable.transform.position = J_Port.transform.position;
                        J_Cable.transform.SetParent(J_Port.transform);
                    }
                }
            }

            if (J_portstatus[i] == "未连接")
            {
                J_Index = i + 1;

                J_Port = transform.Find("SB/" + J_Index).gameObject;
                //判定是否已经有线缆，若有线缆则进行删除
                if (J_Port.transform.childCount != 0)
                {
                    Debug.Log(J_Port.transform.GetChild(0).name + "--------------201");
                    Destroy(J_Port.transform.GetChild(0).gameObject);
                }
            }
        }
    }

    IEnumerator WaitCloseTip()
    {
        yield return new WaitForSeconds(1);
        J_Tip.SetActive(false);
    }
}
