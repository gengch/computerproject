﻿using RTEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LitJson;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class TriggerLightCon : MonoBehaviour
{
    
    private string J_CUrl = "http://3dmis.zcwjvr.com/";
    private GameObject J_tippanel;
    private Text J_floortext;
    private Text J_OfficeText;
    private Text J_tiertext;
    private Text J_nettext;
    private Text J_teltext;
    private bool J_isclose;
    private int J_index;
    private RaycastHit J_hit;
    private GameObject J_firstlightobject;
    private GameObject J_lastlightobject;
    private GameObject J_nowlightobject;
    private GameObject J_house;
    private GameObject J_house2;
    private GameObject J_floor;
    private GameObject J_house1;
    private GameObject J_mouseModel;//选中的模型
    private GameObject J_uifloor;
    //private GameObject J_changji;
    private Camera J_Camera;

    public InputField J_rotatefield;
    //public GameObject J_rotatepanel;
    //public GameObject J_modelPanel;//控制模型移动、旋转的面板
    public Camera J_camera;
    public Camera J_newcamrea;
    public GameObject J_birdcamera;
    public GameObject J_player;
    public GameObject J_renwu;
    public GameObject J_cameraPos;
    public EditorGizmoSystem J_editorsystem;
    public Transform J_rubbish;
    public InputField J_floorfield;
    public InputField J_tierfield;
    public InputField J_seafield;
    public GameObject J_rect;
    public GameObject J_scrollbar;
    //public AreaTipCon J_areatip;
    //public Text J_NumText;
    /****************待连接数据库后，此数据由数据库提供*********************/

    //第一栋楼房坐标
    public float J_bojx;
    public float J_bojy;
    public float J_bojz;
    public int J_hight;

    //平面图
    public GameObject J_plan1;
    public GameObject J_plan2;
    public GameObject J_plan3;
    public GameObject J_plan4;

    //机房个数
    public int J_houseindex;

    public Text J_protext;
    public Text J_areatext;
    public Text J_changetext;
    public Text J_Floortext;
    //测试用面板
    public GameObject Panel;
    /**********************************************************************/
    private GameObject J_housebtn;//按钮预制件
    private GameObject J_creathousebtn;//新创建的机房按钮
    private Transform J_enterpanel;//进入区域面板
    private GameObject J_enterbtn;//进入区域按钮
    private GameObject J_changjing;//地形预制件
    private GameObject J_IntoBtn;//进入区域按钮
    private int J_terrainindex;
    private int J_Index;
    private GameObject J_uifirst;
    private string[] J_BuildId;//建筑ID
    private string[] J_Str;//场景内建筑名称
    private float J_ClickTime;//鼠标双击楼层时的时间间隔
    private string[] J_FloorNum;//每栋建筑的楼层数量
    private string J_LastName;//鼠标射线上次选中的楼层名称
    private string[] J_fimagename;//楼层平面图地址
    private int J_FloorIndex;//当前选中的楼层层号
    private string[] J_GetFloorID;//楼层ID
    private string[] J_RoomName;//机房名称
    private string[] J_RoomID;//机房ID
    private GameObject[] J_Buttons;
    private float J_LoadTime;
    private GameObject J_LoadPanel;
    private Slider J_LoadSlider;
    private Text J_LoadText;
    private WWW www;
    private bool J_IsDestroy;
    private GameObject J_Terrain;
    private GameObject J_Last;
    private float J_UrlTimer;
    private bool J_IsFloor;
    public Button J_AreaBtn;
    private GameObject J_GuidePanel;

    private string[] J_RoomType;
    private string[] J_RoomNum;
    private int J_B;
    private int J_W;
    private int J_D;
    private Text J_Text;
    private Button Refresh;

    private GameObject BuildPanel;
    private GameObject CheckBuilding_Btn;//点击选择建筑按钮
    private Transform ChoseBuilding_Panel;//选择建筑下拉面板
    private GameObject EnterBuilding_Btn;//确定跳转建筑按钮
    private GameObject[] buildingChose_Btn;//建筑下拉菜单按钮
    private GameObject J_creatbuildingbtn;//新创建的建筑按钮

    private GameObject FloorPanel;
    private GameObject CheckFloor_Btn;//点击选择楼层按钮
    private Transform ChoseFloor_Panel;//选择楼层下拉面板
    private GameObject EnterFloor_Btn;//确定跳转楼层按钮
    private GameObject[] floorChose_Btn;//楼层下拉菜单按钮
    private GameObject J_creatfloorbtn;//新创建的楼层按钮

    private string[] floorName;//楼层名称

    private string FloorUIName;//通过按钮进入楼层时，楼层ui显示内容

    private string[] X_RoomType;//

    public static bool isAllRefresh;

    public delegate void D();

    private void Awake()
    {
        J_CUrl = GameConst.ServerUrl;//  File.ReadAllText("D://Url.txt");
    }

    private void Start()
    {
        Debug.LogError("Init Prefabs");
        isAllRefresh = true;
        Refresh = transform.Find("Refresh").GetComponent<Button>();
        FloorPanel = transform.Find("ChosePanel_Floor").gameObject;
        CheckFloor_Btn = transform.Find("ChosePanel_Floor/ChoseFloor").gameObject;
        ChoseFloor_Panel = transform.Find("ChosePanel_Floor/ChoseFloor/EnterPanel");
        EnterFloor_Btn = transform.Find("ChosePanel_Floor/Turn_Floor").gameObject;
        FloorPanel.SetActive(true);

        BuildPanel = transform.Find("ChosePanel").gameObject;
        CheckBuilding_Btn = transform.Find("ChosePanel/ChoseBuilding").gameObject;
        ChoseBuilding_Panel = transform.Find("ChosePanel/ChoseBuilding/EnterPanel");
        EnterBuilding_Btn = transform.Find("ChosePanel/Turn_Building").gameObject;
        BuildPanel.SetActive(true);

        J_GuidePanel = transform.Find("GuidePanel").gameObject;
        J_GuidePanel.SetActive(false);
        J_Last = GameObject.Find("changjing/Last");
        J_LoadPanel = transform.Find("LoadPanel").gameObject;
        J_LoadSlider = J_LoadPanel.transform.Find("LoadSlider").GetComponent<Slider>();
        J_LoadText = J_LoadPanel.transform.Find("LoadText").GetComponent<Text>();
        J_Text = J_LoadPanel.transform.Find("Text").GetComponent<Text>();
        J_Text.text = "正在进入" + AreaTipCon.instance.J_areastr;

        J_renwu.SetActive(false);
        J_Floortext.transform.parent.gameObject.SetActive(false);
        J_areatext.text = AreaTipCon.instance.J_areastr;
        J_protext.text = AreaTipCon.instance.J_prostr;
        J_Camera = J_camera;
        J_rect.SetActive(false);
        J_scrollbar.SetActive(false);
        J_changjing = Resources.Load<GameObject>("changjing");
        J_housebtn = Resources.Load<GameObject>("AreaBtn");
        J_uifloor = Resources.Load<GameObject>("UIFloor");
        J_uifirst = Resources.Load<GameObject>("UIFirst");
        J_enterpanel = transform.Find("Plan/EnterBtn_1/EnterPanel");
        J_enterbtn = transform.Find("Plan/EnterBtn_1").gameObject;
        J_IntoBtn = transform.Find("Plan/IntoBtn").gameObject;
        Panel.SetActive(false);


        //创建场景中选择建筑按钮
        buildingChose_Btn = new GameObject[10];
        for (int i = 0; i < buildingChose_Btn.Length; i++)
        {
            int index = i + 1;
            J_creatbuildingbtn = Instantiate(J_housebtn);
            J_creatbuildingbtn.name = "Building_" + index;
            J_creatbuildingbtn.transform.SetParent(ChoseBuilding_Panel);
            buildingChose_Btn[i] = J_creatbuildingbtn;
            buildingChose_Btn[i].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                this.Onclick_Building(index-1);
            });
        }

        for (int i = 0; i < buildingChose_Btn.Length; i++)
        {
            buildingChose_Btn[i].SetActive(false);
        }

        //创建场景中楼层选择按钮
        floorChose_Btn = new GameObject[10];
        for (int i = 0; i < floorChose_Btn.Length; i++)
        {
            int index = i + 1;
            J_creatfloorbtn = Instantiate(J_housebtn);
            J_creatfloorbtn.name = "Floor_" + index;
            J_creatfloorbtn.transform.SetParent(ChoseFloor_Panel);
            floorChose_Btn[i] = J_creatfloorbtn;
            floorChose_Btn[i].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                this.Onclick_Floor(index - 1);
            });
        }



        for (int i = 0; i < floorChose_Btn.Length; i++)
        {
            floorChose_Btn[i].SetActive(false);
        }

        //创建机房内按钮
        J_Buttons = new GameObject[10];//初始化,之后改成从数据库获取机房数量
        for (int i = 0; i < 10; i++)
        {
            int index = i + 1;
            J_creathousebtn = Instantiate(J_housebtn);
            J_creathousebtn.name = "House_" + index;
            //J_creathousebtn.transform.Find("Text").GetComponent<Text>().text = "机房" + index;
            J_creathousebtn.transform.SetParent(J_enterpanel);
            J_Buttons[i] = J_creathousebtn;
            J_Buttons[i].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                this.Onclick_Room(index - 1);
            });

        }

        //J_changji = Resources.Load<GameObject>("changjing 1");
        J_tippanel = transform.Find("TipPanel").gameObject;
        J_OfficeText = transform.Find("TipPanel/OfficeText").GetComponent<Text>();
        J_floortext = transform.Find("TipPanel/FloorText").GetComponent<Text>();
        J_tiertext = transform.Find("TipPanel/TierText").GetComponent<Text>();
        J_nettext = transform.Find("TipPanel/NetText").GetComponent<Text>();
        J_teltext = transform.Find("TipPanel/TelText").GetComponent<Text>();
        
        J_house = Resources.Load<GameObject>("Floor");
        J_house2 = Resources.Load<GameObject>("Floor1");

        //J_rotatepanel.SetActive(true);


        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            print(J_tempbtn.name + "------159-------" + this.name);
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }


        for (int i = 0; i < J_Buttons.Length; i++)
        {
            J_Buttons[i].SetActive(false);
        }
        //J_rotatepanel.SetActive(false);
        //隐藏楼层平面图
        J_plan1.SetActive(false);
        J_plan2.SetActive(false);
        J_plan3.SetActive(false);
        J_plan4.SetActive(false);
        J_enterpanel.gameObject.SetActive(false);
        ChoseBuilding_Panel.gameObject.SetActive(false);
        J_enterbtn.SetActive(false);
        J_IntoBtn.SetActive(false);
        //J_modelPanel.SetActive(false);
        J_tippanel.SetActive(false);

        print(AreaTipCon.instance.J_AreaId);
        JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/building/get-building?id=" + AreaTipCon.instance.J_AreaId, "Building", "ID", "Num", "", "", "");
        //JsonContent.J_JsonContent.ContentJson_1("/building/findAreasIdAll?id=" + AreaTipCon.instance.J_AreaId, "floorareasnum");
        //JsonContent.J_JsonContent.ContentJson_2("/APIS3d/device/api/web/building/get-building?id=" + AreaTipCon.instance.J_AreaId, "", "data", "Num", 3);
        Invoke("StartInfo", 1.5f);



        if (AreaTipCon.instance.J_IsFloor)
        {
            AreaTipCon.instance.J_IsFloor = false;
            //J_plan1.SetActive(true);

            J_rect.SetActive(false);
            J_scrollbar.SetActive(false);

            print("正在点击楼层");
            string url_ = PlayerPrefs.GetString("楼层");
            print(url_ + "------209url_------");
            //PlayerPrefs.SetString("楼层", url);
            //JsonContent.J_JsonContent.ContentJson_1("http://3dmis.zcwjvr.com" + AreaTipCon.instance.J_FloorUrl, "switchroomname", "id", "", "", "", "");
            StartCoroutine(WaitForJsonData_1(J_CUrl + AreaTipCon.instance.J_FloorUrl, "SwitchRoomName", "ID"));
            print(AreaTipCon.instance.J_FloorUrl + "-----212-----");
            //Invoke("GetRoomInfo", 0.8f);

        }
    }

    void Onclick_Building(int index)
    {
        Debug.Log(index);
        Debug.Log(buildingChose_Btn[index].transform.name);
        CheckBuilding_Btn.transform.Find("Text").GetComponent<Text>().text = buildingChose_Btn[index].transform.Find("Text").GetComponent<Text>().text;
        ChoseBuilding_Panel.gameObject.SetActive(false);
    }

    void Onclick_Floor(int index)
    {
        Debug.Log("楼层ID是：  " + J_GetFloorID[index] + "  ||   ");
        AreaTipCon.instance.J_FloorId = J_GetFloorID[index];
        Debug.Log(index);
        Debug.Log(floorChose_Btn[index].transform.name);
        CheckFloor_Btn.transform.Find("Text").GetComponent<Text>().text = floorChose_Btn[index].transform.Find("Text").GetComponent<Text>().text;
        FloorUIName = floorChose_Btn[index].transform.Find("Text").GetComponent<Text>().text;
        ChoseFloor_Panel.gameObject.SetActive(false);

        JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/floorareas/get-floorareas/?buildingId=" + AreaTipCon.instance.X_BuindID, "fileid", "ID", "", "", "", "");
        Invoke("FimageName", 0.5f);
    }

    void Onclick_Room(int index)
    {
        //Debug.Log("AreaTipCon.instance.J_FloorId======" + AreaTipCon.instance.J_FloorId);
        //JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/switchrooms/get-switchrooms?id="+ AreaTipCon.instance.J_FloorId, "SwitchRoomName", "ID", "RoomType", "", "", "");
        //Invoke("GetRoomInfo", 0.5f);
        J_changetext.text = J_Buttons[index].transform.Find("Text").GetComponent<Text>().text;
        J_Buttons[index].transform.parent.gameObject.SetActive(false);
        AreaTipCon.instance.J_floorstr = J_Buttons[index].transform.Find("Text").GetComponent<Text>().text;
        AreaTipCon.instance.J_floorid = J_RoomID[index];
        AreaTipCon.instance.X_RoomType = X_RoomType[index];
    }

    IEnumerator WaitForJsonData_1(string url, string node_1, string node_2)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString()+"新的");
        J_RoomName = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_RoomName[i] = jar[i][node_1].ToString();
        }

        if (node_2 != "")
        {
            J_RoomID = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_RoomID[i] = jar[i][node_2].ToString();
            }
        }

        for (int i = 0; i < J_RoomName.Length; i++)
        {
            print(J_Buttons.Length + "-----681-----" + i + "---" + J_RoomName.Length);
            J_Buttons[i].SetActive(true);
            //确保不出现乱码
            char[] ch = J_RoomName[i].ToCharArray();
            if (J_RoomName[i] != null)
            {
                if (ch[0] >= 0x4E00 && ch[0] <= 0x9FA5)
                {

                }
                else
                {
                    //SceneManager.LoadScene("ProjectScene");
                }
            }
            J_Buttons[i].transform.Find("Text").GetComponent<Text>().text = J_RoomName[i];
        }
        J_AreaBtn.interactable = true;
        J_enterbtn.SetActive(true);
        J_IntoBtn.SetActive(true);

        print(AreaTipCon.instance.J_Floor + "----AreaTipCon.instance.J_Floor----");
        StartCoroutine(LoadImage(AreaTipCon.instance.J_FimagenUrl));
        J_Floortext.text = AreaTipCon.instance.J_tierstr;
        J_Floortext.transform.parent.gameObject.SetActive(true);

        Panel.SetActive(true);
    }



    //此方法获得建筑数量、名称、ID以及建筑的楼层数量
    void StartInfo()
    {
        //接收经过解析后的Json数据内容
        J_FloorNum = JsonContent.J_JsonContent.ReturnJsonData_3();//楼层数量
        J_Str = JsonContent.J_JsonContent.ReturnJsonData_1();//建筑名称
        Debug.Log("建筑数量为："+J_Str.Length);
        J_BuildId = JsonContent.J_JsonContent.ReturnJsonData_2();//建筑ID        

        #region  初始化时生成数据库中原有建筑，数量为J_Str数组的长度
        for (int j = 0; j < J_Str.Length; j++)
        {
            print(J_FloorNum[j] + "----楼层数量：J_FloorNum：----");
            buildingChose_Btn[j].name = J_Str[j];
            int t = j + 1;
            J_house1 = new GameObject(J_Str[j]);
            J_house1.tag = "house";
            if (J_index == 2)
            {
                if (J_Last.activeInHierarchy)
                {
                    J_Last.SetActive(false);
                }
                //此处生成新地面
                J_index = 0;
                J_terrainindex++;
                if (J_Terrain != null)
                {
                    J_Terrain.transform.Find("Last").gameObject.SetActive(false);
                }
                J_Terrain = Instantiate(J_changjing, new Vector3(60.5f, -0.1f, 119.83f + 119.67f * J_terrainindex), Quaternion.identity);
            }
            J_index++;
            if (j % 2 == 0)
            {
                if (j == 0)
                {
                    J_house1.transform.position = new Vector3(J_bojx /*+ 43 * j*/, J_bojy, J_bojz);
                }
                else
                {
                    J_house1.transform.position = new Vector3(J_bojx /*+ 43 * j*/, J_bojy, 119.67f * (j / 2) + J_bojz);
                }
            }
            else
            {
                if (j == 1)
                {
                    J_house1.transform.position = new Vector3(J_bojx + 67.44f, J_bojy, J_bojz);
                }
                else
                {
                    J_house1.transform.position = new Vector3(J_bojx + 67.44f, J_bojy, 119.67f * (j - 1) / 2 + J_bojz);
                }
            }

            for (int i = 0; i < int.Parse(J_FloorNum[j]); i++)
            {
                //生成指定高度的楼层，并对每层改名
                if (i == 0)
                {
                    J_floor = Instantiate(J_house, new Vector3(J_house1.transform.position.x, J_house1.transform.position.y + i * 3.1f, J_house1.transform.position.z), Quaternion.identity);
                    GameObject J_pos;
                    if (t % 2 == 1)
                    {
                        J_pos = J_floor.transform.Find("Pos1").gameObject;
                    }
                    else
                    {
                        J_pos = J_floor.transform.Find("Pos2").gameObject;
                    }
                    GameObject UIfirst = Instantiate(J_uifirst, J_pos.transform.position, J_pos.transform.rotation);
                    UIfirst.transform.SetParent(J_pos.transform);
                    UIfirst.transform.Find("Text").GetComponent<Text>().text = "" + t; ;
                }
                else
                {
                    J_floor = Instantiate(J_house2, new Vector3(J_house1.transform.position.x, J_house1.transform.position.y + i * 3.1f, J_house1.transform.position.z), Quaternion.identity);
                }
                J_floor.transform.rotation = new Quaternion(0, 0.7f, 0, 0.7f);
                J_floor.transform.SetParent(J_house1.transform);
                J_floor.name = i + 1 + "";
                if (i == 5)
                {
                    GameObject J_ui = Instantiate(J_uifloor, new Vector3(J_house1.transform.position.x, 3.7f + J_house1.transform.position.y + i * 3.1f, J_house1.transform.position.z), Quaternion.identity);
                    J_ui.transform.Find("Text").GetComponent<Text>().text = "" + t;
                    J_ui.transform.SetParent(J_house1.transform);
                }
                Debug.Log("i:    " + i);
            }
            //if (J_index == 3)
            //{
            //    //此处生成新地面
            //    J_index = 0;
            //}
            //J_areatext.text = AreaTipCon.instance.J_areastr;
            Debug.Log("J:     "+j);
        }
        //J_camera.GetComponent<FollowPlayer>().enabled = false;
        J_renwu.SetActive(true);
        J_camera.GetComponent<FollowPlayer>().enabled = true;
        #endregion

    }

    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            case "SynBtn":
                SceneManager.LoadScene("ProjectScene");
                break;
            case "CIBtn":
                J_GuidePanel.SetActive(false);
                break;
            case "GuideBtn":
                if (J_GuidePanel.activeInHierarchy)
                {
                    J_GuidePanel.SetActive(false);
                }
                else
                {
                    J_GuidePanel.SetActive(true);
                }
                break;

            case "EnterBtn_1":
                if (J_enterpanel.gameObject.activeInHierarchy)
                {
                    J_enterpanel.gameObject.SetActive(false);
                }
                else
                {
                    J_enterpanel.gameObject.SetActive(true);
                }
                break;
            //case "House_1":
            //    //此处切换到一号机房场景
            //    //隐藏楼层平面图
            //    J_changetext.text = btn.transform.Find("Text").GetComponent<Text>().text;
            //    btn.transform.parent.gameObject.SetActive(false);
            //    //关闭切换到机房的面板
            //    AreaTipCon.instance.J_floorstr = btn.transform.Find("Text").GetComponent<Text>().text;
            //    AreaTipCon.instance.J_floorid = J_RoomID[0];
            //    J_Index = 1;
            //    break;
            //case "House_2":
            //    //此处切换到二号机房场景
            //    //隐藏楼层平面图
            //    J_changetext.text = btn.transform.Find("Text").GetComponent<Text>().text;
            //    btn.transform.parent.gameObject.SetActive(false);
            //    AreaTipCon.instance.J_floorstr = btn.transform.Find("Text").GetComponent<Text>().text;
            //    AreaTipCon.instance.J_floorid = J_RoomID[1];
            //    J_Index = 2;
            //    break;
            //case "House_3":
            //    //此处切换到二号机房场景
            //    //隐藏楼层平面图
            //    J_changetext.text = btn.transform.Find("Text").GetComponent<Text>().text;
            //    btn.transform.parent.gameObject.SetActive(false);
            //    AreaTipCon.instance.J_floorstr = btn.transform.Find("Text").GetComponent<Text>().text;
            //    AreaTipCon.instance.J_floorid = J_RoomID[2];
            //    J_Index = 3;
            //    break;
            //case "House_4":
            //    //此处切换到二号机房场景
            //    //隐藏楼层平面图
            //    J_changetext.text = btn.transform.Find("Text").GetComponent<Text>().text;
            //    btn.transform.parent.gameObject.SetActive(false);
            //    AreaTipCon.instance.J_floorstr = btn.transform.Find("Text").GetComponent<Text>().text;
            //    AreaTipCon.instance.J_floorid = J_RoomID[3];
            //    J_Index = 4;
            //    break;
            //case "House_5":
            //    //此处切换到二号机房场景
            //    //隐藏楼层平面图
            //    J_changetext.text = btn.transform.Find("Text").GetComponent<Text>().text;
            //    btn.transform.parent.gameObject.SetActive(false);
            //    AreaTipCon.instance.J_floorstr = btn.transform.Find("Text").GetComponent<Text>().text;
            //    AreaTipCon.instance.J_floorid = J_RoomID[4];
            //    J_Index = 5;
            //    break;
            //case "House_6":
            //    //此处切换到二号机房场景
            //    //隐藏楼层平面图
            //    J_changetext.text = btn.transform.Find("Text").GetComponent<Text>().text;
            //    btn.transform.parent.gameObject.SetActive(false);
            //    AreaTipCon.instance.J_floorstr = btn.transform.Find("Text").GetComponent<Text>().text;
            //    AreaTipCon.instance.J_floorid = J_RoomID[5];
            //    J_Index = 6;
            //    break;
            case "FirstSaidBtn":
                //切换为第一人称视野
                J_Camera = J_camera;
                J_renwu.SetActive(true);
                //J_camera.gameObject.GetComponent<CameraMoveCon>().enabled = false;
                J_player.GetComponent<FollowPlayer>().enabled = false;
                J_player.GetComponent<FollowPlayer>().MinDistance = 0;
                J_player.GetComponent<FollowPlayer>().MaxDistance = 0;
                J_player.GetComponent<FollowPlayer>().enabled = true;
                J_player.GetComponent<FollowPlayer>().J_IsThree = false;
                break;
            case "ThirdSaidBtn":
                //切换为第三人称视野
                J_Camera = J_camera;
                J_renwu.SetActive(true);
                //J_camera.gameObject.GetComponent<CameraMoveCon>().enabled = false;
                J_player.GetComponent<FollowPlayer>().enabled = false;
                J_player.GetComponent<FollowPlayer>().MinDistance = 1.5f;
                J_player.GetComponent<FollowPlayer>().MaxDistance = 10;
                J_player.transform.position = J_cameraPos.transform.position;
                J_player.GetComponent<FollowPlayer>().enabled = true;
                J_player.GetComponent<FollowPlayer>().J_IsThree = true;
                break;
            case "BirdBtn":
                //鸟瞰
                J_Camera = J_camera;
                //J_camera.gameObject.GetComponent<CameraMoveCon>().enabled = true;
                J_camera.transform.position = J_birdcamera.transform.position;
                J_camera.transform.rotation = J_birdcamera.transform.rotation;
                J_renwu.SetActive(false);
                J_player.GetComponent<FollowPlayer>().enabled = false;
                break;

            #region  废弃的模型操作功能
            //case "MoveBtn":
            //    //移动
            //    if (J_modelPanel != null)
            //    {
            //        J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Translation);
            //        J_modelPanel.SetActive(false);
            //    }
            //    break;
            //case "RotateBtn":
            //    //旋转
            //    if (J_modelPanel != null)
            //    {
            //        //J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Rotation);
            //        //J_modelPanel.SetActive(false);
            //        J_rotatepanel.SetActive(true);
            //    }
            //    break;
            //case "SureRot":
            //    if (J_rotatefield.text != "")
            //    {
            //        float rotate = float.Parse(J_rotatefield.text);
            //        J_mouseModel.transform.eulerAngles = new Vector3(0, rotate, 0);
            //    }
            //    J_rotatepanel.SetActive(false);
            //    J_modelPanel.SetActive(false);
            //    break;
            //case "FreeRot":
            //    J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Rotation);
            //    J_rotatepanel.SetActive(false);
            //    J_modelPanel.SetActive(false);
            //    break;
            //case "ScaleBtn":
            //    //缩放
            //    if (J_modelPanel != null)
            //    {
            //        J_editorsystem.ChangeActiveGizmo(RTEditor.GizmoType.Scale);
            //        J_modelPanel.SetActive(false);
            //    }
            //    break;
            //case "DeleBtn":
            //    //删除
            //    if (J_mouseModel != null)
            //    {
            //        //Destroy(J_mouseModel);
            //        J_mouseModel.SetActive(false);
            //        J_mouseModel.transform.SetParent(J_rubbish);
            //        J_mouseModel = null;
            //        J_modelPanel.SetActive(false);
            //    }
            //    break;
            //case "CancelBtn":
            //    J_modelPanel.SetActive(false);
            //    break;


            //case "CreartBtn":
            //    if (J_floorfield.text != "" && J_tierfield.text != "")
            //    {
            //        GameObject[] s = GameObject.FindGameObjectsWithTag("house");
            //        int a = int.Parse(J_floorfield.text);
            //        int b = int.Parse(J_tierfield.text);
            //        a = a + s.Length;
            //        //J_NumText.text = "共" + a + "栋";
            //        J_index = s.Length;

            //        if (s.Length % 2 == 0)
            //        {
            //            J_index = 2;
            //        }

            //        for (int j = s.Length; j < a; j++)
            //        {
            //            if (J_index == 2)
            //            {
            //                //此处生成新地面
            //                J_index = 0;
            //                J_terrainindex++;
            //                Instantiate(J_changjing, new Vector3(60.5f, -0.1f, 119.83f + 119.67f * J_terrainindex), Quaternion.identity);
            //            }
            //            int t = j + 1;
            //            J_house1 = new GameObject("House_" + t);
            //            J_house1.tag = "house";
            //            J_index++;
            //            if (j % 2 == 0)
            //            {
            //                if (j == 0)
            //                {
            //                    J_house1.transform.position = new Vector3(J_bojx /*+ 43 * j*/, J_bojy, J_bojz);
            //                }
            //                else
            //                {
            //                    J_house1.transform.position = new Vector3(J_bojx /*+ 43 * j*/, J_bojy, 119.67f * (j / 2) + J_bojz);
            //                }
            //            }
            //            else
            //            {
            //                if (j == 1)
            //                {
            //                    J_house1.transform.position = new Vector3(J_bojx + 67.44f, J_bojy, J_bojz);
            //                }
            //                else
            //                {
            //                    J_house1.transform.position = new Vector3(J_bojx + 67.44f, J_bojy, 119.67f * (j - 1) / 2 + J_bojz);
            //                }
            //            }
            //            for (int i = 0; i < b; i++)
            //            {
            //                //生成指定高度的楼层，并对每层改名
            //                if (i == 0)
            //                {
            //                    J_floor = Instantiate(J_house, new Vector3(J_house1.transform.position.x, J_house1.transform.position.y + i * 3.1f, J_house1.transform.position.z), Quaternion.identity);

            //                    GameObject J_pos;
            //                    if (t % 2 == 1)
            //                    {
            //                        J_pos = J_floor.transform.Find("Pos1").gameObject;
            //                    }
            //                    else
            //                    {
            //                        J_pos = J_floor.transform.Find("Pos2").gameObject;
            //                    }
            //                    GameObject UIfirst = Instantiate(J_uifirst, J_pos.transform.position, J_pos.transform.rotation);
            //                    UIfirst.transform.SetParent(J_pos.transform);
            //                    UIfirst.transform.Find("Text").GetComponent<Text>().text = "" + t; ;

            //                }
            //                else
            //                {
            //                    J_floor = Instantiate(J_house2, new Vector3(J_house1.transform.position.x, J_house1.transform.position.y + i * 3.1f, J_house1.transform.position.z), Quaternion.identity);
            //                }
            //                J_floor.transform.rotation = new Quaternion(0, 0.7f, 0, 0.7f);
            //                if (i == b - 1)
            //                {
            //                    GameObject J_ui = Instantiate(J_uifloor, new Vector3(J_house1.transform.position.x, 3.7f + J_house1.transform.position.y + i * 3.1f, J_house1.transform.position.z), Quaternion.identity);
            //                    J_ui.transform.Find("Text").GetComponent<Text>().text = "" + t;
            //                    J_ui.transform.SetParent(J_house1.transform);
            //                }
            //                J_floor.transform.SetParent(J_house1.transform);
            //                J_floor.name = i + 1 + "";
            //            }
            //        }
            //        J_floorfield.text = "";
            //        J_tierfield.text = "";
            //    }
            //    break;
            #endregion

            case "BackBtn":
                SceneManager.LoadScene("AreaScene");
                break;
            case "CloseBtn":
                J_Floortext.transform.parent.gameObject.SetActive(false);
                J_plan1.SetActive(false);
                J_plan2.SetActive(false);
                J_enterbtn.SetActive(false);
                J_IntoBtn.SetActive(false);
                Panel.SetActive(false);
                break;
            case "ProBtn":
                SceneManager.LoadScene("AreaScene");
                break;
            case "AreaBtn":
                SceneManager.LoadScene("Computer_1");
                break;
            case "FloorBtn":
                SceneManager.LoadScene("Computer_1");
                //JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/switchrooms/get-switchrooms?id=", "SwitchRoomName", "ID", "RoomType", "", "", "");
                //Invoke("GetRoomInfo", 0.5f);

                break;
            case "IntoBtn":
                Debug.Log("当前RoomType是：  "+AreaTipCon.instance.X_RoomType);
                //for (int i = 0; i < X_RoomType.Length; i++)
                //{
                //    Debug.Log("RoomType:  " + X_RoomType[i]);
                //}
                if (AreaTipCon.instance.X_RoomType=="3")
                {
                    SceneManager.LoadScene("jifang01");
                }
                if (AreaTipCon.instance.X_RoomType == "2")
                {
                    SceneManager.LoadScene("jifang02");
                }
                if (AreaTipCon.instance.X_RoomType == "1")
                {
                    SceneManager.LoadScene("jifang03");
                }

                //if (J_Index == 1 || J_Index == 4 || J_Index == 5 || J_Index == 6)
                //{
                //    SceneManager.LoadScene("jifang01");
                //}
                //if (J_Index == 2)
                //{
                //    SceneManager.LoadScene("jifang02");
                //}
                //if (J_Index == 3)
                //{
                //    SceneManager.LoadScene("jifang03");
                //}

                break;
            case "CloseTipBtn":
                btn.transform.parent.gameObject.SetActive(false);
                break;
            //case "SeaBtn_1":
            //    J_rect.SetActive(true);
            //    J_scrollbar.SetActive(true);
            //    break;
            //case "SeaBtn_2":
            //    J_rect.SetActive(true);
            //    J_scrollbar.SetActive(true);
            //    break;
            //case "SeaBtn_3":
            //    J_rect.SetActive(true);
            //    J_scrollbar.SetActive(true);
            //    break;

            case "ChoseBuilding":

                if (ChoseFloor_Panel.gameObject.active == true)
                {
                    ChoseFloor_Panel.gameObject.SetActive(false);
                }
                CheckFloor_Btn.transform.Find("Text").GetComponent<Text>().text = "选择楼层";
                JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/building/get-building?id=" + AreaTipCon.instance.J_AreaId, "Building", "ID", "", "", "", "");
                
                if (ChoseBuilding_Panel.gameObject.activeInHierarchy)
                {
                    ChoseBuilding_Panel.gameObject.SetActive(false);
                }
                else
                {
                    ChoseBuilding_Panel.gameObject.SetActive(true);
                    Invoke("GetBuilding", 0.5f);
                }
                break;

            case "Turn_Building":
                Button[] Btn = CheckBuilding_Btn.GetComponentsInChildren<Button>(true);
                int index = 0;//所有子物体 除第一个其他是建筑
                int a = 0;//确定所选建筑的序号
                for (int i = 0; i < Btn.Length; i++)
                {
                   index = i ;
                    Debug.Log(Btn[i].name+i);
                    if (Btn[i].name == CheckBuilding_Btn.transform.Find("Text").GetComponent<Text>().text)
                    {
                        Debug.Log(Btn[i].name + "   =   " + CheckBuilding_Btn.transform.Find("Text").GetComponent<Text>().text);
                        AreaTipCon.instance.X_Building = GameObject.Find(Btn[i].name);
                        a = index;
                    }              
                }
                Debug.Log("跳转到建筑：    " + AreaTipCon.instance.X_Building + "  中...");
                if (a%2 == 0)
                {
                    J_renwu.transform.position = new Vector3(AreaTipCon.instance.X_Building.transform.position.x - 22, 0, AreaTipCon.instance.X_Building.transform.position.z - 25);
                    Debug.Log("偶数楼.  "  +AreaTipCon.instance.X_Building.transform.position);
                }
                else if (a%2 == 1)
                {
                    J_renwu.transform.position = new Vector3(AreaTipCon.instance.X_Building.transform.position.x + 37, 0, AreaTipCon.instance.X_Building.transform.position.z - 25);
                    Debug.Log("奇数楼   " + AreaTipCon.instance.X_Building.transform.position);
                }                               
                break;

            case "ChoseFloor":
                if (ChoseBuilding_Panel.gameObject.active == true)
                {
                    ChoseBuilding_Panel.gameObject.SetActive(false);
                }
               
                for (int i = 0; i < floorChose_Btn.Length; i++)
                {
                    floorChose_Btn[i].SetActive(false);
                }

                int temp;
                if (X_buildDic.TryGetValue(CheckBuilding_Btn.transform.Find("Text").GetComponent<Text>().text, out temp))
                {
                    AreaTipCon.instance.X_BuindID = temp;
                    JsonContent.J_JsonContent.ContentJson_1("/APIS3d/device/api/web/floorareas/get-floorareas/?buildingId=" + AreaTipCon.instance.X_BuindID, "FloorAreaName", "ID", "", "", "", "");
                    Invoke("GetFloor", 0.75f);
                    if (ChoseFloor_Panel.gameObject.activeInHierarchy)
                    {
                        ChoseFloor_Panel.gameObject.SetActive(false);
                    }
                    else
                    {
                        ChoseFloor_Panel.gameObject.SetActive(true);
                    }
                }
                else
                {
                    return;
                }
                
                break;

            case "Turn_Floor":

                J_rect.SetActive(false);
                J_scrollbar.SetActive(false);
                

                print("正在点击楼层:   "+AreaTipCon.instance.J_FloorId);
                string url_ = "/APIS3d/device/api/web/switchrooms/get-switchrooms?id=" + AreaTipCon.instance.J_FloorId+"&UserID="+LoginCon.X_UserID+ "&RolesID="+LoginCon.X_RolesID;
                Debug.Log(url_ + "--------66666666--------");
                //PlayerPrefs.SetString("楼层", url);
                AreaTipCon.instance.J_FloorUrl = url_;
                JsonContent.J_JsonContent.ContentJson_1(url_, "SwitchRoomName", "ID", "RoomType", "", "", "");
                Invoke("GetRoomInfo", 0.5f);
                J_Floortext.text = FloorUIName;

                break;

                //暂时还原为最初的刷新效果，等把“跳转回来”时RoomType的问题解决了，再把注释的释放（包括Create里的“FloorBtn”中的
            case "Refresh":
                if (isAllRefresh && CreatModelCon.isReturn ==false)
                {
                    SceneManager.LoadScene("Computer_1");
                }
                else if (isAllRefresh==false &&CreatModelCon.isReturn==false)
                {
                    Invoke("GetRoomInfo", 0.5f);                             
                }
                //else if (isAllRefresh==true&&)
                //{

                //}
                else if(isAllRefresh && CreatModelCon.isReturn == true)
                {
                    //Invoke("GetRoomInfo", 0.5f);//从机房跳转回Computer_1时的刷新会报错，因为跳转回来FloorImageUrl无法获取到。暂时做法：跳转回来楼层页面的话，无刷新按钮
                                        
                }
                else
                {
                    //Invoke("GetRoomInfo", 0.5f);
                    SceneManager.LoadScene("Computer_1");
                }
                break;
               
        }
    }

   

    //
    private Dictionary<string,int> X_buildDic = new Dictionary<string, int>();//记录建筑 Name和ID的字典

    public void GetBuilding()
    {
        X_buildDic.Clear(); 
        J_Str = JsonContent.J_JsonContent.ReturnJsonData_1();//建筑名称
        Debug.Log("建筑名称长度：  "+J_Str.Length);
        J_BuildId = JsonContent.J_JsonContent.ReturnJsonData_2();//建筑ID
        Debug.Log("建筑ID长度：  " + J_BuildId.Length);
        for (int i = 0; i < J_BuildId.Length; i++)
        {
            X_buildDic.Add(J_Str[i], int.Parse(J_BuildId[i]));
            buildingChose_Btn[i].SetActive(true);
            buildingChose_Btn[i].transform.Find("Text").GetComponent<Text>().text = J_Str[i];

        }

    }

    void GetFloor()
    {
        floorName = JsonContent.J_JsonContent.ReturnJsonData_1();//楼层名称
        Debug.Log("楼层名称数量(楼层数)：  " + J_Str.Length+1);
        J_GetFloorID = JsonContent.J_JsonContent.ReturnJsonData_2();//楼层ID
        Debug.Log("楼层ID数量：  " + J_GetFloorID.Length);
        for (int i = 0; i < J_GetFloorID.Length; i++)
        {
            floorChose_Btn[i].SetActive(true);
            //floorChose_Btn[i].name = floorName[i];
            floorChose_Btn[i].name = (i+1).ToString();
            floorChose_Btn[i].transform.Find("Text").GetComponent<Text>().text = floorName[i];
            Debug.Log(floorName[i] + "=========楼层名称");

        }

    }


    //此方法获得楼层平面图名称和ID
    void FimageName()
    {
        J_fimagename = JsonContent.J_JsonContent.ReturnJsonData_1();//楼层平面图
        J_GetFloorID = JsonContent.J_JsonContent.ReturnJsonData_2();//楼层id
        for (int i = 0; i < J_fimagename.Length; i++)
        {
            Debug.LogError(string.Format("============>{0},{1}", J_GetFloorID[i], J_fimagename[i]));
        }
        print(J_FloorIndex + "---------");
        J_W = 0; J_D = 0; J_B = 0;
        string url = J_CUrl + "/APIS3d/device/api/web/switchrooms/get-switchrooms?id=" + J_GetFloorID[J_FloorIndex]+"&UserID="+LoginCon.X_UserID+ "&RolesID="+LoginCon.X_RolesID;
        print(url + "-------------");
        StartCoroutine(WaitForJsonData(url));
    }
    private int J_I;
    IEnumerator WaitForJsonData(string url)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(jo.ToString());
        if (jo["data"].ToString() != "")
        {
            Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
            print(jar + "----jar.Count----");

            J_I = 0;
            //for (int i = 0; i < jar.Count; i++)
            //{
            Newtonsoft.Json.Linq.JArray newJar = Newtonsoft.Json.Linq.JArray.Parse(jar[0]["roomnum"].ToString());
            print(newJar.Count + "+++++++--------++++++++" + newJar.ToString());
            //if (i == 0)
            //{
            //    J_RoomType = new string[newJar.Count];
            //}
            J_RoomType = new string[newJar.Count];
            J_RoomNum = new string[newJar.Count];
            for (int j = 0; j < newJar.Count; j++)
            {
                J_RoomType[J_I] = newJar[j]["roomtype"].ToString();
                J_RoomNum[J_I] = newJar[j]["typenum"].ToString();
                print(J_RoomType[J_I] + "---- J_RoomType[i]----" + J_RoomNum[J_I]);
                J_I++;
            }
            //}

            //J_RoomType = new string[jar.Count];
            //for (int i = 0; i < jar.Count; i++)
            //{
            //    J_RoomType[i] = jar[i]["switchroomname"].ToString();
            //    //print(J_Str_1[i] + node_1 + "---node_1---");
            //}

            print(J_RoomType.Length);
            for (int i = 0; i < J_RoomType.Length; i++)
            {
                if (J_RoomType[i] == "办公室")
                {
                    J_B = int.Parse(J_RoomNum[i]);
                    //J_B++;
                    //J_OfficeText.text = J_RoomNum[i];
                    //Debug.Log(J_OfficeText.text + "-------22222-------" + J_RoomNum[i]);
                }
                if (J_RoomType[i] == "电话配线间")
                {
                    J_D = int.Parse(J_RoomNum[i]);
                    //J_D++;
                    //J_teltext.text = J_RoomNum[i];
                    //Debug.Log(J_teltext.text + "-------33333-------" + J_RoomNum[i]);
                }
                if (J_RoomType[i] == "网络配线间")
                {
                    J_W = int.Parse(J_RoomNum[i]);
                    //J_W++;
                    //J_nettext.text = J_RoomNum[i];
                    //Debug.Log(J_nettext.text + "-------44444-------" + J_RoomNum[i]);
                }
            }
        }


        if (J_firstlightobject!=null)
        {
            J_floortext.text = J_firstlightobject.transform.root.name;
            J_tiertext.text = J_firstlightobject.transform.parent.name;
            J_nettext.text = "" + J_W;
            J_teltext.text = "" + J_D;
            J_OfficeText.text = "" + J_B;
            J_tippanel.SetActive(true);
        }
        
    }


    //此方法获得机房名称和机房ID
    void GetRoomInfo()
    {
        isAllRefresh = false;
        J_RoomName = JsonContent.J_JsonContent.ReturnJsonData_1();
        J_RoomID = JsonContent.J_JsonContent.ReturnJsonData_2();
        X_RoomType = JsonContent.J_JsonContent.ReturnJsonData_3();

        for (int i = 0; i < J_RoomName.Length; i++)
        {
            Debug.Log(J_RoomName[i] + "------" + J_RoomID[i]);
        }

        for (int i = 0; i < J_RoomName.Length; i++)
        {
            print(J_Buttons.Length + "-----681-----" + i + "---" + J_RoomName.Length);
            J_Buttons[i].SetActive(true);
            //确保不出现乱码
            char[] ch = J_RoomName[i].ToCharArray();
            if (J_RoomName[i] != "")
            {
                Debug.Log(ch.Length + "----++++++6+++++" + J_RoomName[i]);
                if (ch[0] >= 0x4E00 && ch[0] <= 0x9FA5)
                {

                }
                else
                {
                    //SceneManager.LoadScene("ProjectScene");
                }
            }
            //给机房选择按钮赋值
            J_Buttons[i].transform.Find("Text").GetComponent<Text>().text = J_RoomName[i];
        }
        J_AreaBtn.interactable = true;
        J_enterbtn.SetActive(true);
        J_IntoBtn.SetActive(true);
        //根据楼层的名字来判断对应的平面图

        //J_fimagename[int.Parse(J_hit.collider.gameObject.transform.parent.name)];//楼层平面图的名称,根据楼层平面图替换面板图片
        //if (int.Parse(J_hit.collider.gameObject.transform.parent.name) % 2 == 0)
        //{
        //    J_plan1.SetActive(true);
        //    J_plan2.SetActive(false);
        //}
        //else
        //{
        //    J_plan1.SetActive(false);
        //    J_plan2.SetActive(true);
        //}
        Debug.LogError(AreaTipCon.instance.J_Floor + "----AreaTipCon.instance.J_Floor----");
        Debug.LogError(J_fimagename[AreaTipCon.instance.J_Floor] + "------J_fimagename[AreaTipCon.instance.J_Floor]-----" + AreaTipCon.instance.J_Floor);
        StartCoroutine(LoadImage(J_fimagename[AreaTipCon.instance.J_Floor]));
       
        AreaTipCon.instance.J_FimagenUrl = J_fimagename[AreaTipCon.instance.J_Floor];

        if (J_firstlightobject!=null)
        {
            J_Floortext.text = "楼层" + J_firstlightobject.transform.parent.name;
        }
        J_Floortext.transform.parent.gameObject.SetActive(true);
        //J_areatext.text = AreaTipCon.instance.J_areastr + "：" + J_firstlightobject.transform.root.name + "-" + J_firstlightobject.transform.parent.name + "层";
        Panel.SetActive(true);
    }

    IEnumerator LoadImage(string path)
    {
        if (AreaTipCon.instance.J_ImageUrl!=null)
        {
            Debug.LogError(path + "----path----");
            www = new WWW(path);
            yield return www;
            Texture2D texture = www.texture;
            Sprite sprites = texture != null ? Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f)) : null;
            J_plan1.GetComponent<Image>().sprite = sprites;
            J_plan1.SetActive(true);
            www = null;
        }
        
    }

    void Update()
    {
        if (isAllRefresh && CreatModelCon.isReturn == true)
        {
            Refresh.gameObject.SetActive(false);
        }

            J_UrlTimer += Time.deltaTime;
        //图片加载进度条
        if (www != null && !www.isDone)
        {
            J_LoadPanel.SetActive(true);
            J_Text.text = "正在进入" + AreaTipCon.instance.J_tierstr;
            J_IsDestroy = true;
            J_LoadSlider.value = www.progress;
            //print(www.progress + "---- www.progress----");
            string J_LoadNum = (www.progress * 100).ToString();
            if (J_LoadNum.Length > 4)
            {
                J_LoadText.text = J_LoadNum.Substring(0, 4) + "%";
            }
            else
            {
                J_LoadText.text = J_LoadNum + "%";
            }
        }
        else if (J_IsDestroy)
        {
            J_IsDestroy = false;
            J_LoadSlider.value = 1;
            J_LoadText.text = "100%";
            J_LoadPanel.SetActive(false);
        }

        J_LoadTime += Time.deltaTime;
        if (J_LoadTime <= 0.8f)
        {
            J_LoadSlider.value = 1.25f * J_LoadTime;
            string J_LoadNum = (125f * J_LoadTime).ToString();
            if (J_LoadNum.Length > 4)
            {
                J_LoadText.text = J_LoadNum.Substring(0, 4) + "%";
            }
            else
            {
                J_LoadText.text = J_LoadNum + "%";
            }
        }
        else if (J_LoadTime > 0.8f && www == null)
        {
            J_LoadPanel.SetActive(false);
        }
        J_ClickTime += Time.deltaTime;

        
        Ray J_ray = J_Camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(J_ray, out J_hit))
        {
            
            J_isclose = true;
            Debug.DrawLine(J_ray.origin, J_hit.point, Color.red);
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //print("鼠标点到UI上了");
                return;
            }
            //if (J_hit.collider.gameObject.tag == "House")
            //{
            //    if (Input.GetMouseButtonUp(1))
            //    {
            //        J_mouseModel = J_hit.collider.gameObject;
            //        J_modelPanel.SetActive(true);
            //    }
            //}

            //Debug.LogError("J_hit.collider.gameObject.tag : " + J_hit.collider.gameObject.tag);
            if (J_hit.collider.gameObject.tag == "House")
            {
                string url = "";
                if (J_hit.collider.gameObject.name != J_LastName)
                {
                    J_IsFloor = true;
                    for (int i = 0; i < J_Str.Length; i++)
                    {
                        print(J_Str[i] + "------741-------" + J_hit.collider.gameObject.name);
                        if (J_Str[i] == J_hit.collider.transform.parent.parent.name && J_BuildId != null && J_BuildId.Length > i && J_IsFloor)
                        {
                            J_IsFloor = false;
                            url = "/APIS3d/device/api/web/floorareas/get-floorareas/?buildingId=" + J_BuildId[i];
                            Debug.Log("++++++++++" + url);
                            print(J_FloorIndex + "---746--" + i);
                            JsonContent.J_JsonContent.ContentJson_1(url, "fileid", "ID", "", "", "", "");
                            Invoke("FimageName", 0.5f);
                            J_LastName = J_hit.collider.gameObject.name;
                            J_ClickTime = 0;

                        }
                    }
                }

                else
                {
                    J_floortext.text = J_firstlightobject.transform.root.name;
                    J_tiertext.text = J_firstlightobject.transform.parent.name;
                    J_nettext.text = "" + J_W;
                    J_teltext.text = "" + J_D;
                    J_OfficeText.text = "" + J_B;
                    J_tippanel.SetActive(true);
                }

                //for (int i = 0; i < J_Str.Length; i++)
                //{
                //    //通过时间限制，避免还未接收到数据库中的数据时就执行
                //    if (J_hit.collider.transform.parent.parent.name == J_Str[i] && J_StartTime >= 1)
                //    {
                //        print(J_Str[i] + "------" + J_BuildId[i]);
                //    }
                //}
                //对楼层单机鼠标时出现对应楼层的平面图

                if (Input.GetMouseButtonUp(0))
                {
                    J_ClickTime = 0;
                }
                Debug.LogError("J_ClickTime : " + J_ClickTime);

                if (Input.GetMouseButtonDown(0) && J_ClickTime <= 1)
                {
                    Debug.LogError("Raycast .. ");
                    J_rect.SetActive(false);
                    J_scrollbar.SetActive(false);
                    //print(J_GetFloorID.Length + "----正在点击楼层----" + int.Parse(J_hit.collider.gameObject.transform.parent.name));
                    if (J_GetFloorID.Length < int.Parse(J_hit.collider.gameObject.transform.parent.name))
                    {
                        return;
                    }

                    print("正在点击楼层");
                    string url_ = "/APIS3d/device/api/web/switchrooms/get-switchrooms?id=" + J_GetFloorID[int.Parse(J_hit.collider.gameObject.transform.parent.name) - 1]+"&UserID="+LoginCon.X_UserID+ "&RolesID="+LoginCon.X_RolesID;
                    Debug.Log(url_ + "--------66666666--------");
                    AreaTipCon.instance.J_FloorID = J_GetFloorID[int.Parse(J_hit.collider.gameObject.transform.parent.name) - 1];
                    AreaTipCon.instance.J_Floor = int.Parse(J_hit.collider.gameObject.transform.parent.name) - 1;

                    Debug.LogError("hit btn : " + J_hit.collider.gameObject.name, J_hit.collider.gameObject);

                    //PlayerPrefs.SetString("楼层", url);
                    AreaTipCon.instance.J_FloorUrl = url_;
                    JsonContent.J_JsonContent.ContentJson_1(url_, "SwitchRoomName", "ID", "RoomType", "", "", "");
                    Invoke("GetRoomInfo", 2.5f);

                    //J_AreaBtn.interactable = true;
                    //J_enterbtn.SetActive(true);
                    //J_IntoBtn.SetActive(true);
                    ////根据楼层的名字来判断对应的平面图

                    ////J_fimagename[int.Parse(J_hit.collider.gameObject.transform.parent.name)];//楼层平面图的名称,根据楼层平面图替换面板图片
                    //if (int.Parse(J_hit.collider.gameObject.transform.parent.name) % 2 == 0)
                    //{
                    //    J_plan1.SetActive(true);
                    //    J_plan2.SetActive(false);
                    //}
                    //else
                    //{
                    //    J_plan1.SetActive(false);
                    //    J_plan2.SetActive(true);
                    //}
                    //J_Floortext.text = "楼层" + J_firstlightobject.transform.parent.name;
                    //J_Floortext.transform.parent.gameObject.SetActive(true);
                    ////J_areatext.text = AreaTipCon.instance.J_areastr + "：" + J_firstlightobject.transform.root.name + "-" + J_firstlightobject.transform.parent.name + "层";
                    //Panel.SetActive(true);
                    AreaTipCon.instance.J_tierstr = "楼层" + J_hit.collider.transform.parent.name;
                    return;
                }
                if (Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2))
                {
                    J_tippanel.SetActive(false);
                    return;
                }
                J_firstlightobject = J_hit.collider.gameObject;


                J_FloorIndex = int.Parse(J_hit.collider.transform.parent.name) - 1;

                //AreaTipCon.instance.J_tierstr = "楼层" + J_firstlightobject.transform.parent.name;
                if (J_firstlightobject != J_nowlightobject)
                {
                    J_lastlightobject = J_nowlightobject;
                    J_nowlightobject = J_firstlightobject;
                    J_firstlightobject.GetComponent<HighlightObject>().SwithHightlight(true);
                }
                if (J_lastlightobject != null && J_firstlightobject.GetInstanceID() != J_lastlightobject.GetInstanceID())
                {
                    J_lastlightobject.GetComponent<HighlightObject>().SwithHightlight(false);
                    J_lastlightobject = null;
                }
            }
            else
            {
                J_tippanel.SetActive(false);
                if (J_firstlightobject == null)
                {
                    return;
                }
                if (J_nowlightobject != null)
                {
                    J_firstlightobject.GetComponent<HighlightObject>().SwithHightlight(false);
                    J_nowlightobject = null;
                }
            }
        }
        else
        {
            if (J_isclose)
            {
                J_isclose = false;
                J_tippanel.SetActive(false);
                if (J_firstlightobject == null)
                {
                    return;
                }
                if (J_nowlightobject != null)
                {
                    J_firstlightobject.GetComponent<HighlightObject>().SwithHightlight(false);
                    J_nowlightobject = null;
                }
            }
        }


    }

    void SaveData()
    {
        //Newtonsoft.Json.Linq.li
        JsonData data = new JsonData();
        data["cabinietid"] = 5;
        data["Manufacturer"] = "666";
        data["AssetsSerial"] = "123456";
        data["buytime"] = "2018-5-17";
        data["StartWorkTime"] = "2018-5-18";
        data["CanWorkTime"] = "3";
        string J_JsonData = data.ToJson();
        //JsonData Js = new JsonData();
        //Js["data"] = new JsonData();
        //Js["data"].Add(data);
        print(J_JsonData.ToString());
        string url = J_CUrl + J_JsonData;
        StartCoroutine(WaitForJsonData_2(url));
    }

    IEnumerator WaitForJsonData_2(string url)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["msg"].ToString());
        print(jar.ToString() + "----jar.ToString----");
    }
        
}
