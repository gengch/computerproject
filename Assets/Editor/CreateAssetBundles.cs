﻿using UnityEditor;
using UnityEngine;

public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.WebGL);
    }

    [MenuItem("Assets/Build AssetBundles ALL")]
    static void CreateAssetBunldesALL()
    {
        Caching.ClearCache();


        string Path = Application.dataPath + "/StreamingAssets/ALL.assetbundle";


        Object[] SelectedAsset = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

        foreach (Object obj in SelectedAsset)
        {
            Debug.Log("Create AssetBunldes name :" + obj);
        }

        //这里注意第二个参数就行  
        //if (BuildPipeline.BuildAssetBundles(null, SelectedAsset, Path, BuildAssetBundleOptions.CollectDependencies))
        //{
        //    AssetDatabase.Refresh();
        //}
        //else
        //{

        //}
    }
}
