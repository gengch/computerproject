﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SBInfoPanelCon : MonoBehaviour
{
    private string J_CUrl = "http://3dmis.zcwjvr.com/";
    private Text J_NameText;
    [HideInInspector]
    public Text J_TypeText;
    private Text J_BLText;
    private Text J_IPText;
    [HideInInspector]
    public Text J_PosText;
    private Text J_NuText;
    private Text J_CJText;
    private Text J_CPText;
    private Text J_BuyText;
    private Text J_SJText;
    private Text J_WorkText;
    private InputField J_NameInput;
    private InputField J_IPInput;
    private InputField J_NuInput;
    private InputField J_CJInput;
    private InputField J_CPInput;
    private InputField J_BuyInput;
    private InputField J_SJInput;
    private InputField J_WorkInput;
    [HideInInspector]
    public GameObject J_RayEquip;
    [HideInInspector]
    public GameObject J_RayCabinet;
    [HideInInspector]
    public string J_EquipID = "1";
    [HideInInspector]
    public bool J_IsNew;
    [HideInInspector]
    public GameObject J_Cabinet;
    [HideInInspector]
    public string J_EquipTypeID;

    private Dictionary<string, string> J_Data;
    public Text J_Tip;

    private string[] J_EquipId;
    private string[] J_EquipName;
    private string[] J_CabinetName;
    private string[] J_CabinetID;

    public static SBInfoPanelCon J_SB;

    private string[] J_ipaddress;//IP地址
    private string[] J_cabinetpos;//机柜位置
    private string[] J_productcode;//序列号
    private string[] J_manufacturer;//生产厂家
    private string[] J_assetsserial;//资产编号
    private string[] J_buytime;//购买时间
    private string[] startworktime;//开始时间
    private string[] canworktime;//工作年限
    private string[] J_equiptypeid;//设备类型id

    private string J_Last1;
    private string J_Last2;
    private string J_Last3;
    private string J_Last4;
    private string J_Last5;
    private string J_Last6;
    private string J_Last7;
    private string J_Last8;
    private string J_Last9;
    private string J_Last10;
    private string J_Last11;

    private bool ISok = true;
    private bool J_IsShow;

    [HideInInspector]
    public string J_A;

    private string J_CabinetiD;
    private string J_EquipiD;

    private Dictionary<string, string> J_D = new Dictionary<string, string>();
    private Vector3 Pos;

    [HideInInspector]
    public bool J_IsChange;
    [HideInInspector]
    public bool J_IsMove;


    private void Awake()
    {
        J_CUrl = GameConst.ServerUrl;// File.ReadAllText("D://Url.txt");

        J_SB = this;
        Button[] J_buttons = GameObject.FindObjectsOfType<Button>();
        foreach (Button J_btn in J_buttons)
        {
            Button J_tempbtn = J_btn;
            J_tempbtn.onClick.AddListener(delegate ()
            {
                OnClickBtn(J_tempbtn);
            });
        }
    }

    void OnEnable()
    {
        J_NameText = transform.Find("NameText").GetComponent<Text>();
        J_TypeText = transform.Find("TypeText").GetComponent<Text>();
        J_BLText = transform.Find("BLText").GetComponent<Text>();
        J_IPText = transform.Find("IPText").GetComponent<Text>();
        J_PosText = transform.Find("PosText").GetComponent<Text>();
        J_NuText = transform.Find("NuText").GetComponent<Text>();
        J_CJText = transform.Find("CJText").GetComponent<Text>();
        J_CPText = transform.Find("CPText").GetComponent<Text>();
        J_BuyText = transform.Find("BuyText").GetComponent<Text>();
        J_SJText = transform.Find("SJText").GetComponent<Text>();
        J_WorkText = transform.Find("WorkText").GetComponent<Text>();

        J_NameInput = transform.Find("NameInput").GetComponent<InputField>();
        J_IPInput = transform.Find("IPInput").GetComponent<InputField>();
        J_NuInput = transform.Find("NuInput").GetComponent<InputField>();
        J_CJInput = transform.Find("CJInput").GetComponent<InputField>();
        J_CPInput = transform.Find("CPInput").GetComponent<InputField>();
        J_BuyInput = transform.Find("BuyInput").GetComponent<InputField>();
        J_SJInput = transform.Find("SJInput").GetComponent<InputField>();
        J_WorkInput = transform.Find("WorkInput").GetComponent<InputField>();

        J_NameInput.gameObject.SetActive(false);
        J_IPInput.gameObject.SetActive(false);
        J_NuInput.gameObject.SetActive(false);
        J_CJInput.gameObject.SetActive(false);
        J_CPInput.gameObject.SetActive(false);
        J_BuyInput.gameObject.SetActive(false);
        J_SJInput.gameObject.SetActive(false);
        J_WorkInput.gameObject.SetActive(false);

        J_IPText.gameObject.SetActive(true);
        J_NameText.gameObject.SetActive(true);
        J_TypeText.gameObject.SetActive(true);
        J_BLText.gameObject.SetActive(true);
        J_PosText.gameObject.SetActive(true);
        J_NuText.gameObject.SetActive(true);
        J_CJText.gameObject.SetActive(true);
        J_CPText.gameObject.SetActive(true);
        J_BuyText.gameObject.SetActive(true);
        J_SJText.gameObject.SetActive(true);
        J_WorkText.gameObject.SetActive(true);

        if (J_IsNew)
        {
            J_NameInput.text = "";
            J_IPInput.text = "";
            J_NuInput.text = "";
            J_CJInput.text = "";
            J_CPInput.text = "";
            J_BuyInput.text = "";
            J_SJInput.text = "";
            J_WorkInput.text = "";

            J_NameText.text = "";
            J_IPText.text = "";
            J_NuText.text = "";
            J_CJText.text = "";
            J_CPText.text = "";
            J_BuyText.text = "";
            J_SJText.text = "";
            J_WorkText.text = "";

        }
        //每次面板打开先查找该机房内所有设备
        if (!J_IsNew)
        {
            string url = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
            StartCoroutine(WaitForJsonData_1(url, "EquipName", "ID", "IPAddress", "CabinetPos", "ProductCode", "Manufacturer", "AssetsSerial", "BuyTime", "StartWorkTime", "CanWorkTime", "EquipTypeID", true));
        }
        //获取房间内的机柜信息

        J_Data = new Dictionary<string, string>();
        J_Data.Add("id", AreaTipCon.instance.J_floorid);
        //J_Data.Add("userName", PlayerPrefs.GetString("账号"));
        J_Data.Add("UserID", LoginCon.X_UserID);
        J_Data.Add("RolesID", LoginCon.X_RolesID);
        string url1 = J_CUrl + "/APIS3d/device/api/web/cabinets/get-cabinets";
        StartCoroutine(WaitForJsonData_2(url1, J_Data, "CabinetName", "ID", "", "", ""));
    }

    IEnumerator WaitForJsonData_1(string url, string node_1, string node_2, string node_3, string node_4, string node_5, string node_6, string node_7, string node_8, string node_9, string node_10, string node_11, bool isContinue)
    {
        WWW www = new WWW(url);
        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print("666666666 " + jar.ToString());
        J_EquipName = new string[jar.Count];

        for (int i = 0; i < jar.Count; i++)
        {
            J_EquipName[i] = jar[i][node_1].ToString();
        }

        if (node_2 != "")
        {
            J_EquipId = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_EquipId[i] = jar[i][node_2].ToString();
            }
        }

        if (node_3 != "")
        {
            J_ipaddress = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_ipaddress[i] = jar[i][node_3].ToString();
            }
        }

        if (node_4 != "")
        {
            J_cabinetpos = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_cabinetpos[i] = jar[i][node_4].ToString();
            }
        }

        if (node_5 != "")
        {
            J_productcode = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_productcode[i] = jar[i][node_5].ToString();
            }
        }
        if (node_6 != "")
        {
            J_manufacturer = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_manufacturer[i] = jar[i][node_6].ToString();
            }
        }

        if (node_7 != "")
        {
            J_assetsserial = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_assetsserial[i] = jar[i][node_7].ToString();
            }
        }

        if (node_8 != "")
        {
            J_buytime = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_buytime[i] = jar[i][node_8].ToString();
            }
        }
        if (node_9 != "")
        {
            startworktime = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                startworktime[i] = jar[i][node_9].ToString();
            }
        }

        if (node_10 != "")
        {
            canworktime = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                canworktime[i] = jar[i][node_10].ToString();
            }
        }
        if (node_11 != "")
        {
            J_equiptypeid = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_equiptypeid[i] = jar[i][node_11].ToString();
            }
        }

        for (int i = 0; i < J_EquipName.Length; i++)
        {
            Debug.Log(J_EquipName[i] + "   ---240---   " + J_EquipId[i] + "   " + J_cabinetpos[i]);
        }

        if (isContinue)
        {

            J_NameText.text = "";
            J_IPText.text = "";
            J_NuText.text = "";
            J_CJText.text = "";
            J_CPText.text = "";
            J_BuyText.text = "";
            J_SJText.text = "";
            J_WorkText.text = "";

            for (int i = 0; i < J_EquipName.Length; i++)
            {
                if (J_RayEquip.name == "SB")
                {
                    J_RayEquip = J_RayEquip.transform.parent.gameObject;
                }

                if (J_EquipName[i] == J_RayEquip.name)
                {
                    J_NameText.text = J_RayEquip.name;
                    J_TypeText.text = PlayerPrefs.GetString(J_equiptypeid[i]);
                    J_IPText.text = J_ipaddress[i];
                    Debug.Log(J_RayEquip.name + "   " + J_A + "   " + J_IsChange);
                    if (J_RayEquip.name == J_A && !J_IsChange)
                    {
                        J_PosText.text = J_cabinetpos[i];
                    }
                    if (J_RayEquip.name != J_A)
                    {
                        J_A = J_RayEquip.name;
                        J_PosText.text = J_cabinetpos[i];
                    }

                    if (J_RayEquip.GetComponent<EqCon>().J_CabinetPos != "")
                    {
                        J_PosText.text = J_RayEquip.GetComponent<EqCon>().J_CabinetPos;
                    }

                    J_NuText.text = J_productcode[i];
                    J_CJText.text = J_manufacturer[i];
                    J_CPText.text = J_assetsserial[i];
                    J_BuyText.text = J_buytime[i];
                    J_SJText.text = startworktime[i];
                    J_WorkText.text = canworktime[i];
                    J_BLText.text = J_RayEquip.transform.parent.name;

                    J_NameInput.text = J_NameText.text;
                    J_IPInput.text = J_IPText.text;
                    J_NuInput.text = J_NuText.text;
                    J_CJInput.text = J_CJText.text;
                    J_CPInput.text = J_CPText.text;
                    J_BuyInput.text = J_BuyText.text;
                    J_SJInput.text = J_SJText.text;
                    J_WorkInput.text = J_WorkText.text;
                }
            }
        }
    }
    IEnumerator WaitForJsonData_2(string url, Dictionary<string, string> post, string node_1, string node_2, string node_3, string node_4, string node_5)
    {
        WWWForm wwwForm = new WWWForm();
        foreach (KeyValuePair<string, string> saveElectric in post)
        {
            wwwForm.AddField(saveElectric.Key, saveElectric.Value.ToString());
        }
        WWW www = new WWW(url, wwwForm);

        yield return www;

        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        print(www.text);
        Newtonsoft.Json.Linq.JArray jar = Newtonsoft.Json.Linq.JArray.Parse(jo["data"].ToString());
        print(jar.ToString());
        J_CabinetName = new string[jar.Count];
        for (int i = 0; i < jar.Count; i++)
        {
            J_CabinetName[i] = jar[i][node_1].ToString();
            print(J_CabinetName[i] + node_1 + "---node_1---");
        }

        if (node_2 != "")
        {
            J_CabinetID = new string[jar.Count];
            for (int i = 0; i < jar.Count; i++)
            {
                J_CabinetID[i] = jar[i][node_2].ToString();
                print(J_CabinetID[i] + node_2 + "---node_2---" + "----url     " + url);
            }
        }
    }

    void OnClickBtn(Button btn)
    {
        switch (btn.name)
        {
            case "SeaveB":

                J_A = J_RayEquip.name;
                //Pos = J_RayEquip.transform.position;
                if (J_NameInput.text != "")
                {
                    J_RayEquip.name = J_NameInput.text;
                    J_NameText.text = J_NameInput.text;
                }
                if (J_IPInput.text != "")
                {
                    J_IPText.text = J_IPInput.text;
                }
                if (J_NuInput.text != "")
                {
                    J_NuText.text = J_NuInput.text;
                }
                if (J_CJInput.text != "")
                {
                    J_CJText.text = J_CJInput.text;
                }
                if (J_CPInput.text != "")
                {
                    J_CPText.text = J_CPInput.text;
                }
                if (J_BuyInput.text != "")
                {
                    J_BuyText.text = J_BuyInput.text;
                }
                if (J_SJInput.text != "")
                {
                    J_SJText.text = J_SJInput.text;
                }
                if (J_WorkInput.text != "")
                {
                    J_WorkText.text = J_WorkInput.text;
                }

                J_NameInput.gameObject.SetActive(false);
                J_IPInput.gameObject.SetActive(false);
                J_NuInput.gameObject.SetActive(false);
                J_CJInput.gameObject.SetActive(false);
                J_CPInput.gameObject.SetActive(false);
                J_BuyInput.gameObject.SetActive(false);
                J_SJInput.gameObject.SetActive(false);
                J_WorkInput.gameObject.SetActive(false);

                J_IPText.gameObject.SetActive(true);
                J_NameText.gameObject.SetActive(true);
                J_TypeText.gameObject.SetActive(true);
                J_BLText.gameObject.SetActive(true);
                J_PosText.gameObject.SetActive(true);
                J_NuText.gameObject.SetActive(true);
                J_CJText.gameObject.SetActive(true);
                J_CPText.gameObject.SetActive(true);
                J_BuyText.gameObject.SetActive(true);
                J_SJText.gameObject.SetActive(true);
                J_WorkText.gameObject.SetActive(true);

                J_Data = new Dictionary<string, string>();
                if (!J_IsNew)
                {
                    for (int i = 0; i < J_EquipName.Length; i++)
                    {
                        if (J_EquipName[i] == J_A)
                        {
                            J_Data.Add("id", J_EquipId[i]);
                            J_EquipiD = J_EquipId[i];
                            Debug.Log(J_EquipiD + "   ---398---   ");
                        }
                    }
                }

                J_Data.Add("EquipName", J_NameText.text);

                if (J_IsNew)
                {
                    J_Data.Add("EquipTypeID", J_EquipTypeID);
                    PlayerPrefs.SetString(J_TypeText.text, J_EquipTypeID);
                }
                else
                {
                    J_Data.Add("EquipTypeID", PlayerPrefs.GetString(J_TypeText.text));
                }
                J_Data.Add("SwitchRoom", AreaTipCon.instance.J_floorid);
                for (int i = 0; i < J_CabinetName.Length; i++)
                {
                    if (J_CabinetName[i] == J_RayEquip.transform.parent.name)
                    {
                        J_Data.Add("CabinetID", J_CabinetID[i]);
                        J_CabinetiD = J_CabinetID[i];
                        print("----359---" + J_CabinetID[i]);
                    }
                }
                //http://cams.zcwjvr.com:8080/equipment/equipSave?EquipName=111&EquipTypeID=1&SwitchRoom=3&CabinetID=854&CabinetPos=40&IPAddress=111&AssetsSerial=111&Manufacturer=1&Remark=1&ProductCode=1&StartWorkTime=1&BuyTime=1&CanWorkTime=1&PortNumPerRow=24
                //http://cams.zcwjvr.com:8080/equipment/equipSave?EquipName=152&EquipTypeID=1&SwitchRoom=3&CabinetID=854&CabinetPos=40&IPAddress=111&AssetsSerial=111&Manufacturer=1&Remark=1&ProductCode=1&StartWorkTime=1&BuyTime=1&CanWorkTime=1&PortNumPerRow=24

                J_Data.Add("CabinetPos", J_PosText.text);
                J_Data.Add("IPAddress", J_IPText.text);
                J_Data.Add("AssetsSerial", J_CPText.text);
                J_Data.Add("Manufacturer", J_CJText.text);
                J_Data.Add("Remark", "11");
                J_Data.Add("ProductCode", J_NuText.text);
                J_Data.Add("StartWorkTime", J_SJText.text);
                J_Data.Add("BuyTime", J_BuyText.text);
                J_Data.Add("CanWorkTime", J_WorkText.text);
                J_Data.Add("PortNumPerRow", "24");

                Debug.Log(J_PosText.text + "--------501--------");

                if (J_IsNew)
                {
                    J_IsNew = false;
                    J_IsShow = true;
                    JsonContent.J_JsonContent.SaveDataToServer("/APIS3d/device/api/web/equipments/save-equipments", J_Data);
                    foreach (KeyValuePair<string, string> kvp in J_Data)
                    {
                        Debug.Log("键" + kvp.Key + "    值" + kvp.Value);
                    }
                    Invoke("ShowSavaInfo", 0.5f);
                    Invoke("WaitUpdateDate", 0.5f);
                    print("设备新建成功");
                }
                else
                {
                    if (J_Last1 != J_NameText.text || J_Last2 != J_TypeText.text || J_Last3 != J_BLText.text || J_Last4 != J_IPText.text || J_Last5 != J_PosText.text || J_Last6 != J_NuText.text || J_Last7 != J_CJText.text || J_Last8 != J_CPText.text || J_Last9 != J_BuyText.text || J_Last10 != J_SJText.text || J_Last11 != J_WorkText.text)
                    {
                        J_Last1 = J_NameText.text;
                        J_Last2 = J_TypeText.text;
                        J_Last3 = J_BLText.text;
                        J_Last4 = J_IPText.text;
                        J_Last5 = J_PosText.text;
                        J_Last6 = J_NuText.text;
                        J_Last7 = J_CJText.text;
                        J_Last8 = J_CPText.text;
                        J_Last9 = J_BuyText.text;
                        J_Last10 = J_SJText.text;
                        J_Last11 = J_WorkText.text;
                        print(J_NameText.text + "////" + J_TypeText.text + "////" + J_BLText.text + "////" + J_IPText.text + "////" + J_PosText.text);
                        J_D = J_Data;


                        if (J_IsMove)
                        {
                            J_IsMove = false;
                            string endurl = J_CabinetiD + "&EquipTypeID=" + PlayerPrefs.GetString(J_TypeText.text) + "&CabinetPos=" + J_PosText.text + "&EquipID=" + J_EquipiD;

                            string uurl = J_CUrl + "/APIS3d/device/api/web/equipments/cabinet-pos?CabinetID=" + endurl;
                            print(uurl + "-------------" + J_A);
                            StartCoroutine(WaitForJsonData_1(uurl));
                        }

                        else
                        {
                            J_RayEquip.GetComponent<EqCon>().J_EquipPos = J_RayEquip.transform.localPosition;
                            JsonContent.J_JsonContent.SaveDataToServer("/APIS3d/device/api/web/equipments/save-equipments", J_Data);
                            string UpdateUrl = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
                            StartCoroutine(WaitForJsonData_1(UpdateUrl, "EquipName", "ID", "IPAddress", "CabinetPos", "ProductCode", "Manufacturer", "AssetsSerial", "BuyTime", "StartWorktTime", "CanWorkTime", "EquipTypeID", false));
                            Invoke("ShowSavaInfo", 0.5f);
                            print("设备信息更新成功");
                        }

                    }
                }

                J_NameInput.text = "";
                J_IPInput.text = "";
                J_NuInput.text = "";
                J_CJInput.text = "";
                J_CPInput.text = "";
                J_BuyInput.text = "";
                J_SJInput.text = "";
                J_WorkInput.text = "";

                break;
            case "CloseBtn":
                J_IsChange = false;
                if (J_IsNew)
                {
                    J_IsNew = false;
                    Destroy(J_RayEquip);
                }
                if (btn.transform.parent.gameObject.name == "SBInfoPanel")
                {
                    J_RayEquip.transform.localPosition = J_RayEquip.GetComponent<EqCon>().J_EquipPos;
                }
                btn.transform.parent.gameObject.SetActive(false);
                break;
            case "NameB":
                J_NameText.gameObject.SetActive(false);
                J_NameInput.text = J_NameText.text;
                J_NameInput.gameObject.SetActive(true);
                break;
            case "IPB":
                J_IPText.gameObject.SetActive(false);
                J_IPInput.text = J_IPText.text;
                J_IPInput.gameObject.SetActive(true);
                break;
            case "NuB":
                J_NuText.gameObject.SetActive(false);
                J_NuInput.text = J_NuText.text;
                J_NuInput.gameObject.SetActive(true);
                break;
            case "CJB":
                J_CJText.gameObject.SetActive(false);
                J_CJInput.text = J_CJText.text;
                J_CJInput.gameObject.SetActive(true);
                break;
            case "CPB":
                J_CPText.gameObject.SetActive(false);
                J_CPInput.text = J_CPText.text;
                J_CPInput.gameObject.SetActive(true);
                break;
            case "BuyB":
                J_BuyText.gameObject.SetActive(false);
                J_BuyInput.text = J_BuyText.text;
                J_BuyInput.gameObject.SetActive(true);
                break;
            case "SJB":
                J_SJText.gameObject.SetActive(false);
                J_SJInput.text = J_SJText.text;
                J_SJInput.gameObject.SetActive(true);
                break;
            case "WorkB":
                J_WorkText.gameObject.SetActive(false);
                J_WorkInput.text = J_WorkText.text;
                J_WorkInput.gameObject.SetActive(true);
                break;
        }
    }

    IEnumerator WaitForJsonData_1(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        Newtonsoft.Json.Linq.JObject jo = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(www.text);
        string J_Str;
        print(jo.ToString() + "------更新设备位置------");
        J_Str = jo["code"].ToString();
        J_IsChange = false;
        if (J_Str == "0")
        {
            //http://3dmis.zcwjvr.com/equipment/equipUpdate?id=923&EquipName=152&EquipTypeID=1&SwitchRoom=3&CabinetID=862&CabinetPos=40&IPAddress=111&AssetsSerial=111&Manufacturer=1&Remark=1&ProductCode=1&StartWorkTime=1&BuyTime=1&CanWorkTime=1&PortNumPerRow=24
            J_RayEquip.GetComponent<EqCon>().J_EquipPos = J_RayEquip.transform.localPosition;
            JsonContent.J_JsonContent.SaveDataToServer("/APIS3d/device/api/web/equipments/save-equipments", J_Data);
            string UpdateUrl = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
            StartCoroutine(WaitForJsonData_1(UpdateUrl, "EquipName", "ID", "IPAddress", "CabinetPos", "ProductCode", "Manufacturer", "AssetsSerial", "BuyTime", "StartWorkTime", "CanWorkTime", "EquipTypeID", false));
            Invoke("ShowSavaInfo", 0.5f);
            print("设备信息更新成功");
        }
        else
        {
            Debug.Log(J_RayEquip.name + "---" + J_RayEquip.transform.position + "   " + J_RayEquip.GetComponent<EqCon>().J_EquipPos);
            J_RayEquip.transform.localPosition = J_RayEquip.GetComponent<EqCon>().J_EquipPos;
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && J_IsNew)
        {
            GameObject clickedBtn = EventSystem.current.currentSelectedGameObject;

            if (clickedBtn != null && (clickedBtn.name == "SBInfoPanel" || clickedBtn.transform.parent.name == "SBInfoPanel"))
            {
                print("这个是正确的");
            }
            else
            {
                print("点错东西了");
                J_Tip.text = "请先保存或关闭信息面板";
                J_Tip.gameObject.SetActive(true);
                transform.root.GetComponent<CreatModelCon>().WaitClose();
            }
            return;
        }

        if (J_IsNew)
        {
            return;
        }

        if (J_RayEquip != null)
        {
            J_NameText.text = J_RayEquip.name;
        }
    }

    public void UpdateEquipInfo()
    {
        J_Data = new Dictionary<string, string>();

        for (int i = 0; i < J_EquipName.Length; i++)
        {
            print(J_EquipName[i] + "-------505-------" + J_A);
            if (J_EquipName[i] == J_A)
            {
                print("-------507------");
                J_Data.Add("id", J_EquipId[i]);
            }
        }

        J_Data.Add("EquipName", J_NameText.text);
        J_Data.Add("EquipTypeID", PlayerPrefs.GetString(J_TypeText.text));
        J_Data.Add("SwitchRoom", AreaTipCon.instance.J_floorid);

        for (int i = 0; i < J_CabinetName.Length; i++)
        {
            print(J_CabinetName[i] + "----J_CabinetName[i]----");
            if (J_CabinetName[i] == J_Cabinet.name)
            {
                J_Data.Add("CabinetID", J_CabinetID[i]);
                print("----359---" + J_CabinetID[i]);
            }
        }

        J_Data.Add("CabinetPos", J_PosText.text);
        J_Data.Add("IPAddress", J_IPText.text);
        J_Data.Add("AssetsSerial", J_CPText.text);
        J_Data.Add("Manufacturer", J_CJText.text);
        J_Data.Add("Remark", "11");
        J_Data.Add("ProductCode", J_NuText.text);
        J_Data.Add("StartWorkTime", J_SJText.text);
        J_Data.Add("BuyTime", J_BuyText.text);
        J_Data.Add("CanWorkTime", J_WorkText.text);
        J_Data.Add("PortNumPerRow", "24");

        JsonContent.J_JsonContent.SaveDataToServer("/APIS3d/device/api/web/equipments/save-equipments", J_Data);

        string url = J_CUrl + "/APIS3d/device/api/web/equipments/get-roomid?SwitchRoomID=" + AreaTipCon.instance.J_floorid;
        StartCoroutine(WaitForJsonData_1(url, "EquipName", "ID", "iIPAddress", "CabinetPos", "ProductCode", "Manufacturer", "AssetsSerial", "BuyTime", "StartWorkTime", "CanWorkTime", "EquipTypeID", false));
        print("设备信息更新成功");
    }

    void WaitUpdateDate()
    {
        transform.root.GetComponent<CreatModelCon>().UpdateData();
    }

    void ShowSavaInfo()
    {
        string SaveData = JsonContent.J_JsonContent.ReturnSaveData();
        Debug.Log("保存返回的信息是===" + SaveData);
        if (SaveData == "")
        {
            if (J_IsShow)
            {
                J_IsShow = false;
                J_Tip.text = "保存成功";
                J_Tip.gameObject.SetActive(true);
                this.transform.root.gameObject.GetComponent<CreatModelCon>().WaitClose();
                gameObject.SetActive(false);
            }
        }
        else
        {
            J_Tip.text = "名称重复，请重新操作";
            J_Tip.gameObject.SetActive(true);
            this.transform.root.gameObject.GetComponent<CreatModelCon>().WaitClose();
            if (J_IsShow)
            {
                J_IsShow = false;
                J_IsNew = true;
            }
        }
    }
}
