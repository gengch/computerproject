﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZText : MonoBehaviour
{
    public Camera J_camera;
    private RaycastHit J_hit;

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray J_ray = J_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(J_ray, out J_hit))
            {
                print(J_hit.collider.gameObject.name);
            }
        }
    }
}
